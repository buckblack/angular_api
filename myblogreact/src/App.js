import React from 'react';
import './App.css';
import Header from './client/Header/Header';
import MobileMenu from './client/Header/MobileMenu';
import Footer from './client/Footer/Footer';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from './client/Home/Home';
import CategoryListSubCategory from './client/Category/CategoryListSubCategory';
import CategoryListPost from './client/Category/CategoryListPost';

function App() {
  return (
    <Router>
        <Switch>
          <Route exact path="/">
            <Header />
            <MobileMenu />
            <Home/>
            <Footer/>
          </Route>
          <Route path="/category">
            <Header />
            <MobileMenu />
            <CategoryListSubCategory/>
            <CategoryListPost/>
            <Footer/>
          </Route>
        </Switch>
    </Router>
  );
}

export default App;
