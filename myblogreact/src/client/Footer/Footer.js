import React, { Component } from 'react'
import $ from 'jquery';

const topFunction=()=> {
    $("html, body").animate({ scrollTop: "0" });
    document.documentElement.scrollTop = 0;
}

export default class Footer extends Component {
    render() {
        return (
            <div>
                <div className="site-footer">
                    <div className="container">
                        <div className="row mb-5">
                            <div className="col-md-4">
                                <h3 className="footer-heading mb-4">About Us</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat reprehenderit magnam deleniti quasi saepe, consequatur atque sequi delectus dolore veritatis obcaecati quae, repellat eveniet omnis, voluptatem in. Soluta, eligendi, architecto.</p>
                            </div>
                            <div className="col-md-3 ml-auto">
                                <ul className="list-unstyled float-left mr-5">
                                    <li><a href="/" onClick={e=>e.preventDefault()}>About Us</a></li>
                                    <li><a href="/" onClick={e=>e.preventDefault()}>Advertise</a></li>
                                    <li><a href="/" onClick={e=>e.preventDefault()}>Careers</a></li>
                                    <li><a href="/" onClick={e=>e.preventDefault()}>Subscribes</a></li>
                                </ul>
                                <ul className="list-unstyled float-left">
                                    <li><a href="/" onClick={e=>e.preventDefault()}>Travel</a></li>
                                    <li><a href="/" onClick={e=>e.preventDefault()}>Lifestyle</a></li>
                                    <li><a href="/" onClick={e=>e.preventDefault()}>Sports</a></li>
                                    <li><a href="/" onClick={e=>e.preventDefault()}>Nature</a></li>
                                </ul>
                            </div>
                            <div className="col-md-4">
                                <div>
                                    <h3 className="footer-heading mb-4">Connect With Us</h3>
                                    <p>
                                        <a href="/" onClick={e=>e.preventDefault()}><span className="fa fa-facebook-square pt-2 pr-2 pb-2 pl-0" /></a>
                                        <a href="/" onClick={e=>e.preventDefault()}><span className="fa fa-twitter-square p-2" /></a>
                                        <a href="/" onClick={e=>e.preventDefault()}><span className="fa fa-instagram p-2" /></a>
                                        <a href="/" onClick={e=>e.preventDefault()}><span className="fa fa-rss-square p-2" /></a>
                                        <a href="/" onClick={e=>e.preventDefault()}><span className="fa fa-envelope-square p-2" /></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 text-center">
                                <p>
                                    Copyright ©
                                    All rights reserved | This template is made with <i className="icon-heart text-danger" aria-hidden="true" /> by <a rel="noopener noreferrer" target="_blank" href={'https://colorlib.com'}>Colorlib</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <button onClick={topFunction} id="movetop" className="bg-primary" title="Go to top">
                        <i className="fa fa-angle-up" />
                    </button>
                </div>
            </div>
        )
    }
}
