import React, { Component } from 'react'

export default class MobileMenu extends Component {
    render() {
        return (
            <div className="site-mobile-menu">
                <div className="site-mobile-menu-header">
                    <div className="site-mobile-menu-close mt-3">
                        <span className="icon-close2 js-menu-toggle" />
                    </div>
                    </div>
                    <div className="site-mobile-menu-body text-toggle-mobile text-center">
                    <p><a href="/" onClick={e=>e.preventDefault()}>Home</a></p>
                    <p><a href="/" onClick={e=>e.preventDefault()}>row.cateName</a></p>
                    <div className="input-group">
                        <input type="text" className="form-control input-mobile" placeholder="Search" />
                        <div className="input-group-append">
                        <button className="btn btn-outline-secondary btn-search-mobile" type="button"><i className="fa fa-search" /></button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
