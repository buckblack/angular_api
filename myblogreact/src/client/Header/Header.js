import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import HeaderSearch from './HeaderSearch'
import axios from 'axios';

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.handleSearchOpenClick = this.handleSearchOpenClick.bind(this);
        this.handleSearchCloseClick = this.handleSearchCloseClick.bind(this);
        this.state = { isShow: false };
    }
    handleSearchOpenClick() {
        this.setState({ isShow: true });
    }
    handleSearchCloseClick() {
        this.setState({ isShow: false });
    }
    // componentDidMount() {
    //     axios.get("http://localhost:63168/api/category/getall")
    //         .then(res => {
    //             console.log(res);

    //         })
    //         .catch(error => console.log(error));
    // }
    // componentDidMount() {
    //     fetch("http://localhost:63168/api/category/getall")
    //         .then(res => res.json())
    //         .then(
    //             (result) => {
    //                 console.log(result);

    //             }
    //         )
    // }
    render() {
        return (
            <header className="site-navbar" role="banner">
                <div className="container-fluid">
                    <div className="row align-items-center">
                        {this.state.isShow && <HeaderSearch handleSearchCloseClick={this.handleSearchCloseClick} />}
                        <div className="col-4 site-logo">
                            <a href="/" className="text-black h2 mb-0">Mini Blog</a>
                        </div>
                        <div className="col-8 text-right">
                            <nav className="site-navigation" role="navigation">
                                <ul className="site-menu js-clone-nav mr-auto d-none d-lg-block mb-0">
                                    <li><Link to="/">Home</Link></li>
                                    <li><Link to="/category">row.cateName</Link></li>
                                    <li className="d-none d-lg-inline-block" onClick={this.handleSearchOpenClick}><div className="js-search-toggle text-black pointer"><i className="fa fa-search"></i></div></li>
                                </ul>
                            </nav>
                            <a href="/" className="site-menu-toggle js-menu-toggle text-black d-inline-block d-lg-none"><i className="fa fa-bars" /></a>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}
