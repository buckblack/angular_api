import React, { Component } from 'react'

export default class HeaderSearch extends Component {
    render() {
        return (
            <div className="col-12 search-form-wrap js-search-form">
                    <input type="text" id="s" className="form-control" placeholder="Search..." />
                    <button className="search-btn pointer" type="button">
                        <i title="Search" className="fa fa-search"></i>&nbsp;&nbsp;
                        <i title="Close" className="fa fa-remove" onClick={this.props.handleSearchCloseClick}></i></button>
            </div>
        )
    }
}
