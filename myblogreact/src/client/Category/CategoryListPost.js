import React, { Component } from 'react'

export default class CategoryListPost extends Component {
    render() {
        return (
            <div className="py-5 bg-light">
                <div className="container">
                    <div className="row mb-5">
                        <div className="col-12">
                            <h2 id="category">Sub-Category</h2>
                        </div>
                    </div>
                    <div className="row align-items-stretch retro-layout-2">
                        <div className="col-md-4 col-lg-3">
                            <a className="h-entry mb-30 v-height gradient">
                                <div className="text h-100 w-100 d-flex justify-content-center align-items-center">
                                    <h2 className="categorie-text">name</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
