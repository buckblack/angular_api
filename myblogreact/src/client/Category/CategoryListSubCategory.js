import React, { Component } from 'react'

export default class CategoryListSubCategory extends Component {
    render() {
        return (
            <div className="site-section bg-white">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 col-md-6 col-sm-6 mb-4">
                            <div className="entry2">
                                <a ><img src="{{row.pathThumbnail}}" alt="{{row.postTitle}}" className="img-fluid rounded img-category" /></a>
                                <div className="excerpt">
                                    <span className="post-category text-white bg-success mb-3">subcatename</span>
                                    <h2><a>post tile</a></h2>
                                    <div className="post-meta align-items-center text-left clearfix">
                                        <span className="d-inline-block mt-1">By <a>username</a></span>
                                        <span>date create</span>
                                    </div>
                                    <p>summary</p>
                                    <p><a>Read More</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row text-center pt-5 border-top">
                        <div className="col-md-12">
                            <ul className="pagination justify-content-center">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
