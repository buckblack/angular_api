# angular_api

**Client**: localhost:4200

**Admin**: localhost:4200/admin

**UserAdmin**:
    <br/>username: tienhm4@gmail.com
    <br/>password: 123456
    
**Migrations**: yes

**Seed**: yes
    <br/>Role: Admin
    <br/>userAdmin: tienhm4@gmail.com - 123456

**Scripts**:
    <br/>Full(Data and Schema): yes
    <br/>Data only: yes
    <br/>Schema only: yes

**Unit Testing**: nUnit (please comment OnConfiguring in BlogContext.cs before run tests)
    
# DEMO
**Client**: https://desd3892blog.web.app/

**Admin**: https://desd3892blog.web.app/admin

**UserAdmin**:
    <br/>username: tienhm4@gmail.com
    <br/>password: 123456

**Fond-end**: firebase

**Database**: https://www.smarterasp.net/

**Back-end**: https://somee.com/
    
**Image**:

**Client**:
<br/>

![Screenshot](https://gitlab.com/buckblack/angular_api/-/raw/master/MyBlog/src/assets/images/client.PNG)
<br/>

**Admin**
![Screenshot](https://gitlab.com/buckblack/angular_api/-/raw/master/MyBlog/src/assets/images/admin.PNG)