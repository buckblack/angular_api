import { AdminstatisuserComponent } from './admin/adminStatistical/adminstatisuser/adminstatisuser.component';
import { ForbiddenComponent } from './shared/forbidden/forbidden.component';
import { UnauthorizeComponent } from './shared/unauthorize/unauthorize.component';
import { ResetpasswordComponent } from './shared/resetpassword/resetpassword.component';
import { UserComponent } from './admin/adminUser/user/user.component';
import { RoleComponent } from './admin/adminUser/role/role.component';
import { AdminmessageComponent } from './admin/adminNotifications/adminmessage/adminmessage.component';
import { AdminnotifyComponent } from './admin/adminNotifications/adminnotify/adminnotify.component';
import { AdminstatiscommentComponent } from './admin/adminStatistical/adminstatiscomment/adminstatiscomment.component';
import { AdminstatispostComponent } from './admin/adminStatistical/adminstatispost/adminstatispost.component';
import { AdmincommentComponent } from './admin/adminContent/admincomment/admincomment.component';
import { AdmincategoryComponent } from './admin/adminContent/admincategory/admincategory.component';
import { PostComponent } from './admin/adminContent/post/post.component';
import { PostpagingComponent } from './client/home/postpaging/postpaging.component';
import { NotfoundComponent } from './shared/notfound/notfound.component';
import { DetailComponent } from './client/detail/detail.component';
import { SearchComponent } from './client/search/search.component';
import { AboutComponent } from './client/about/about.component';
import { ContactComponent } from './client/contact/contact.component';
import { SubcategoryComponent } from './client/subcategory/subcategory.component';
import { CategoryComponent } from './client/category/category.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { AdminComponent } from './admin/admin.component';
import { ClientComponent } from './client/client.component';
import { HomeComponent } from './client/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './shared/login/login.component';
import { GuestComponent } from './admin/adminUser/guest/guest.component';


const routes: Routes = [
  {
    path: '',
    component: ClientComponent,
    children: [
      {
        path: '',
        component: HomeComponent,
      },
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'category/:cateid',
        component: CategoryComponent,
      },
      {
        path:'Detail/:postid',
        component: DetailComponent,
      },
      {
        path:'subcategory/:subcateid',
        component: SubcategoryComponent,
      },
      {
        path:'search/:key',
        component: SearchComponent,
      },
      {
        path:'about',
        component: AboutComponent,
      },
      {
        path:'contact',
        component: ContactComponent,
      },
    ]
  },
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      {
        path: '',
        component: DashboardComponent,
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: 'post',
        component: PostComponent,
      },
      {
        path: 'category',
        component: AdmincategoryComponent,
      },
      {
        path: 'comment',
        component: AdmincommentComponent,
      },
      {
        path: 'statispost',
        component: AdminstatispostComponent,
      },
      {
        path: 'statiscomment',
        component: AdminstatiscommentComponent,
      },
      {
        path: 'statisuser',
        component: AdminstatisuserComponent,
      },
      {
        path: 'notify',
        component: AdminnotifyComponent,
      },
      {
        path: 'message',
        component: AdminmessageComponent,
      },
      {
        path: 'role',
        component: RoleComponent,
      },
      {
        path: 'user',
        component: UserComponent,
      },
      {
        path: 'guest',
        component: GuestComponent,
      },
    ]
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'resetpassword',
    component: ResetpasswordComponent
  },
  {
    path: '401',
    component: UnauthorizeComponent
  },
  {
    path: '403',
    component: ForbiddenComponent
  },
  {
    path: '404',
    component: NotfoundComponent
  },
  {
    path:'**',
    redirectTo: '/404'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
