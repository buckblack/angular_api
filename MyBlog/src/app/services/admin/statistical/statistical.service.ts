import { Observable } from 'rxjs';
import { ApiService } from '../../api/api.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StatisticalSchemas } from "src/app/schema/statistical";

@Injectable({
  providedIn: 'root'
})
export class StatisticalService {

  constructor(private http:HttpClient, private api:ApiService) { }

  public LoadPostChartAllTime():Observable<StatisticalSchemas.StatisticalPostResponse>{
    return this.http.get<StatisticalSchemas.StatisticalPostResponse>(this.api.apiUrl.adminStatisticalPostAllTime);
  }

  public LoadPostChartByTime(param):Observable<StatisticalSchemas.StatisticalPostResponse>{
    return this.http.post<StatisticalSchemas.StatisticalPostResponse>(this.api.apiUrl.adminStatisticalPostByTime,param);
  }

  public LoadCommentChartAllTime():Observable<StatisticalSchemas.StatisticalCommentResponse>{
    return this.http.get<StatisticalSchemas.StatisticalCommentResponse>(this.api.apiUrl.adminStatisticalCommentAllTime);
  }

  public LoadCommentChartByTime(param):Observable<StatisticalSchemas.StatisticalCommentResponse>{
    return this.http.post<StatisticalSchemas.StatisticalCommentResponse>(this.api.apiUrl.adminStatisticalCommentByTime,param);
  }

  public LoadUserChartAllTime():Observable<StatisticalSchemas.StatisticalUserResponse>{
    return this.http.get<StatisticalSchemas.StatisticalUserResponse>(this.api.apiUrl.adminStatisticalUserAllTime);
  }

  public LoadUserChartByTime(param):Observable<StatisticalSchemas.StatisticalUserResponse>{
    return this.http.post<StatisticalSchemas.StatisticalUserResponse>(this.api.apiUrl.adminStatisticalUserByTime,param);
  }
}
