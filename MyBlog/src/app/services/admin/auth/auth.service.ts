import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Observable, from } from 'rxjs';
import { ApiService } from '../../api/api.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {LoginSchemas} from '../../../schema/login';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public timeCookie:number=(0.5/24);//30 min
  constructor(private api:ApiService,private http:HttpClient,private cookieService:CookieService,private route:Router) { }

  public login(param):Observable<LoginSchemas.LoginResponse>{
    return this.http.post<LoginSchemas.LoginResponse>(this.api.apiUrl.authLogin,param);
  }

  public ChangePassword(param):Observable<LoginSchemas.LoginResponse>{
    return this.http.post<LoginSchemas.LoginResponse>(this.api.apiUrl.authChangePW,param);
  }

  public ResetPassword(param):Observable<LoginSchemas.LoginResponse>{
    return this.http.post<LoginSchemas.LoginResponse>(this.api.apiUrl.authResetPW,param);
  }

  public IsLogin():boolean
  {
    if(this.cookieService.get("token") == undefined || this.cookieService.get("token")=='')
    {
      return false;
    }
    return true;
  }

  public Logout()
  {
    this.cookieService.deleteAll();
    this.route.navigate(['/login']);
  }

}
