import { SubCategorySchemas } from "src/app/schema/subcategory";
import { PostSchemas } from "src/app/schema/post";
import { Observable } from 'rxjs';
import { ApiService } from '../../api/api.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http:HttpClient, private apiService:ApiService) { }

  public LoadDashboard():Observable<PostSchemas.DashboardResponse>{
    return this.http.get<PostSchemas.DashboardResponse>(this.apiService.apiUrl.dashBoard);
  }

  public LoadDashboardChart():Observable<SubCategorySchemas.SubCategoryDataResponse>{
    return this.http.get<SubCategorySchemas.SubCategoryDataResponse>(this.apiService.apiUrl.dashBoardChart);
  }

}
