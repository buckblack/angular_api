import { PostSchemas } from "src/app/schema/post";
import { CategorySchemas } from "src/app/schema/category";
import { SubCategorySchemas } from "src/app/schema/subcategory";
import { CommentSchemas } from "src/app/schema/comment";
import { Observable } from 'rxjs';
import { ApiService } from '../../api/api.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  constructor(private api: ApiService, private http:HttpClient) { }

  //***********************************//
  //start category content
  public getCategorySort():Observable<CategorySchemas.CategoriesResponse>
  {
    return this.http.get<CategorySchemas.CategoriesResponse>(this.api.apiUrl.contentCategorySort);
  }

  public getSubcategorySort(cateId):Observable<SubCategorySchemas.SubCategoryDataResponse>{
    return this.http.get<SubCategorySchemas.SubCategoryDataResponse>(this.api.apiUrl.contentSubCategorySort+'/'+cateId);
  }

  public getTableCategory():Observable<CategorySchemas.CategoriesResponse>{
    return this.http.get<CategorySchemas.CategoriesResponse>(this.api.apiUrl.contentTableSubCategory+'/'+null);
  }

  public getTableSubCategory(cateId):Observable<SubCategorySchemas.SubCategoryDataResponse>{
    return this.http.get<SubCategorySchemas.SubCategoryDataResponse>(this.api.apiUrl.contentTableSubCategory+'/'+cateId);
  }

  public createCategory(param):Observable<CategorySchemas.CategoriesResponse>{
    return this.http.post<CategorySchemas.CategoriesResponse>(this.api.apiUrl.contentCreateCategory,param);
  }

  public updateCategory(param):Observable<CategorySchemas.CategoriesResponse>{
    return this.http.post<CategorySchemas.CategoriesResponse>(this.api.apiUrl.contentUpdateCategory,param);
  }

  public deleteCategory(cateId):Observable<CategorySchemas.CategoriesResponse>{
    return this.http.delete<CategorySchemas.CategoriesResponse>(this.api.apiUrl.contentdeleteCategory+'/'+cateId);
  }

  public createSubCategory(param):Observable<SubCategorySchemas.SubCategoryResponse>{
    return this.http.post<SubCategorySchemas.SubCategoryResponse>(this.api.apiUrl.contentCreateSubCategory,param);
  }

  public updateSubCategory(param):Observable<SubCategorySchemas.SubCategoryResponse>{
    return this.http.post<SubCategorySchemas.SubCategoryResponse>(this.api.apiUrl.contentUpdateSubCategory,param);
  }

  public deleteSubCategory(subCateId):Observable<SubCategorySchemas.SubCategoryResponse>{
    return this.http.delete<SubCategorySchemas.SubCategoryResponse>(this.api.apiUrl.contentdeleteSubCategory+'/'+subCateId);
  }

  //end category content
  //*******************************//

  //***************************************//
  //start comment content
  public searchPostsComment(param):Observable<PostSchemas.PostsRespone>{
    return this.http.post<PostSchemas.PostsRespone>(this.api.apiUrl.contentSearchPostComment,param);
  }

  public getComments(param):Observable<CommentSchemas.CommentResponse>{
    return this.http.post<CommentSchemas.CommentResponse>(this.api.apiUrl.contentGetComments,param);
  }

  public submitRemoveComment(param):Observable<CommentSchemas.CommentResponse>{
    return this.http.post<CommentSchemas.CommentResponse>(this.api.apiUrl.contentRemoveComments,param);
  }

  //end comment content
  //*******************************//

  //***************************************//
  //start post content
  public getAllCategoryForPost():Observable<CategorySchemas.CategoriesResponse>
  {
    return this.http.get<CategorySchemas.CategoriesResponse>(this.api.apiUrl.contentPostGetCategory);
  }

  public getSubcategoryForPost(cateId):Observable<SubCategorySchemas.SubCategoryDataResponse>{
    return this.http.get<SubCategorySchemas.SubCategoryDataResponse>(this.api.apiUrl.contentPostGetSubCategory+'/'+cateId);
  }

  public getPostBySubCateId(cateId):Observable<PostSchemas.PostsRespone>{
    return this.http.get<PostSchemas.PostsRespone>(this.api.apiUrl.contentPostGetBySubCateId+'/'+cateId);
  }

  public addNewPost(param):Observable<PostSchemas.PostsRespone>{
    return this.http.post<PostSchemas.PostsRespone>(this.api.apiUrl.contentAddNewPost,param);
  }

  public addImageMoreUpdate(param):Observable<PostSchemas.PostDetailRespone>{
    return this.http.post<PostSchemas.PostDetailRespone>(this.api.apiUrl.contentAddImgMoreUpdate,param);
  }

  public searchPostById(param):Observable<PostSchemas.PostsRespone>{
    return this.http.post<PostSchemas.PostsRespone>(this.api.apiUrl.contentPostSearch,param);
  }

  public getPostDetailUpdate(param):Observable<PostSchemas.PostDetailRespone>{
    return this.http.post<PostSchemas.PostDetailRespone>(this.api.apiUrl.contentPostDetail,param);
  }

  public UpdatePost(param):Observable<PostSchemas.PostsRespone>{
    return this.http.post<PostSchemas.PostsRespone>(this.api.apiUrl.contentUpdatePost,param);
  }

  public UpdateStatusPost(param):Observable<PostSchemas.PostsRespone>{
    return this.http.post<PostSchemas.PostsRespone>(this.api.apiUrl.contentUpdateStatusPost,param);
  }

  public RemoveTrashImageContent(param):Observable<PostSchemas.PostsRespone>{
    return this.http.get<PostSchemas.PostsRespone>(this.api.apiUrl.contentRemoveTrashImagePost+'/'+param);
  }
    //end post content
  //*******************************//
}
