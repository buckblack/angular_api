import { NotifySchemas } from "src/app/schema/notify";
import { Observable } from 'rxjs';
import { ApiService } from '../../api/api.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class NotifyService {

  constructor(private http:HttpClient, private api:ApiService) { }

  public SubmitNotify(param):Observable<NotifySchemas.NotifyResponse>
  {
    return this.http.post<NotifySchemas.NotifyResponse>(this.api.apiUrl.adminNotify,param);
  }

  public GetNotifyNav():Observable<NotifySchemas.NotifyResponse>
  {
    return this.http.get<NotifySchemas.NotifyResponse>(this.api.apiUrl.adminGetNotifyNav);
  }

  public GetNotify():Observable<NotifySchemas.NotifyResponse>
  {
    return this.http.get<NotifySchemas.NotifyResponse>(this.api.apiUrl.adminGetNotify);
  }

  public GetNotifyDetail(notifyId):Observable<NotifySchemas.NotifyDetailResponse>
  {
    return this.http.get<NotifySchemas.NotifyDetailResponse>(this.api.apiUrl.adminGetNotifyDetail + '/' +notifyId);
  }

  public DeleteNotify(param):Observable<NotifySchemas.NotifyResponse>
  {
    return this.http.delete<NotifySchemas.NotifyResponse>(this.api.apiUrl.adminDeleteNotify+'/'+param);
  }
}
