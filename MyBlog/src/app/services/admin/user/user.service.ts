import { RoleSchemas } from "src/app/schema/role";
import { GuestSchemas } from "src/app/schema/guest";
import { UserSchemas } from "src/app/schema/user";
import { Observable } from 'rxjs';
import { ApiService } from '../../api/api.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private api:ApiService,private http:HttpClient) { }

  public getAllRole():Observable<RoleSchemas.RoleResponse>
  {
    return this.http.get<RoleSchemas.RoleResponse>(this.api.apiUrl.roleGetAll)
  }

  public AddNewRole(param):Observable<RoleSchemas.RoleResponse>
  {
    return this.http.post<RoleSchemas.RoleResponse>(this.api.apiUrl.roleAddNew,param)
  }

  public EditRole(param):Observable<RoleSchemas.RoleResponse>
  {
    return this.http.post<RoleSchemas.RoleResponse>(this.api.apiUrl.roleEdit,param)
  }

  public RemoveRole(param):Observable<RoleSchemas.RoleResponse>
  {
    return this.http.delete<RoleSchemas.RoleResponse>(this.api.apiUrl.roleRemove+'/'+param)
  }

  public getAllGuest():Observable<GuestSchemas.guestResponse>
  {
    return this.http.get<GuestSchemas.guestResponse>(this.api.apiUrl.guestGetAll)
  }

  public getAllUser():Observable<UserSchemas.userResponse>
  {
    return this.http.get<UserSchemas.userResponse>(this.api.apiUrl.userGetAll)
  }

  public AddNewUser(param):Observable<UserSchemas.userResponse>
  {
    return this.http.post<UserSchemas.userResponse>(this.api.apiUrl.userAddNew,param)
  }

  public UpdateUser(param):Observable<UserSchemas.userResponse>
  {
    return this.http.post<UserSchemas.userResponse>(this.api.apiUrl.userUpdate,param)
  }

  public UpdateStatusUser(param):Observable<UserSchemas.userResponse>
  {
    return this.http.post<UserSchemas.userResponse>(this.api.apiUrl.userUpdateStatus,param)
  }
}
