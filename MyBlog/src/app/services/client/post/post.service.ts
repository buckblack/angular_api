import { PostSchemas } from "src/app/schema/post";
import { CommentSchemas } from "src/app/schema/comment";
import { ApiService } from '../../api/api.service';
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class PostService {

  constructor(private api: ApiService, private http: HttpClient) { }
  public getPostsHot(): Observable<PostSchemas.PostsRespone> {
    return this.http.get<PostSchemas.PostsRespone>(this.api.apiUrl.postHot);
  }

  public getPostsRelated(param): Observable<PostSchemas.PostsRespone> {
    return this.http.get<PostSchemas.PostsRespone>(this.api.apiUrl.postRelated,{params:param});
  }

  public getPostsSearch(param): Observable<PostSchemas.PostsRespone> {
    return this.http.get<PostSchemas.PostsRespone>(this.api.apiUrl.postSearch,{params:param});
  }

  public getSortSearch(param): Observable<PostSchemas.PostsRespone> {
    return this.http.post<PostSchemas.PostsRespone>(this.api.apiUrl.postSortSearch,param);
  }

  public submitComment(data):Observable<CommentSchemas.SubmitCommentRespone>{
    return this.http.post<CommentSchemas.SubmitCommentRespone>(this.api.apiUrl.submitComment,data);
  }

  public submitSubscriber(data):Observable<CommentSchemas.SubmitCommentRespone>{
    return this.http.post<CommentSchemas.SubmitCommentRespone>(this.api.apiUrl.submitSubscriber,data);
  }
}
