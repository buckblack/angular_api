import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../api/api.service';
import { Injectable } from '@angular/core';
import { PostSchemas } from "src/app/schema/post";



@Injectable({
  providedIn: 'root'
})


export class HomeService {

  constructor(private api:ApiService,private http:HttpClient) { }
  public getPostsHot():Observable<PostSchemas.HomePostsHotResponse>{
    return this.http.get<PostSchemas.HomePostsHotResponse>(this.api.apiUrl.postHot);
  }

  public getPostsRecent(param):Observable<PostSchemas.HomePostsRecentResponse>{
    return this.http.get<PostSchemas.HomePostsRecentResponse>(this.api.apiUrl.postRecent,{params:param});
  }

  public getPostsRecentComment():Observable<PostSchemas.HomePostsRecentCommentResponse>{
    return this.http.get<PostSchemas.HomePostsRecentCommentResponse>(this.api.apiUrl.postRecentComment);
  }
}
