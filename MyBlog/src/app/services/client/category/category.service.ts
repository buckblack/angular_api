import { ApiService } from '../../api/api.service';
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { CategorySchemas } from "src/app/schema/category";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private api: ApiService, private http: HttpClient) { }

  public getAll(): Observable<CategorySchemas.CategoriesResponse> {
    return this.http.get<CategorySchemas.CategoriesResponse>(this.api.apiUrl.category)
  }

  public getAllSubcategory(param): Observable<CategorySchemas.CategoryPostResponse> {
    return this.http.get<CategorySchemas.CategoryPostResponse>(this.api.apiUrl.categorySub,{params:param})
  }
  public getAllPosts(param):Observable<CategorySchemas.CategoryPostResponse>{
    return this.http.get<CategorySchemas.CategoryPostResponse>(this.api.apiUrl.categoryPost,{params:param})
  }
}
