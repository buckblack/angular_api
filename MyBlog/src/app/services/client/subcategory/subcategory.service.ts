import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../api/api.service';
import { Injectable } from '@angular/core';
import { SubCategorySchemas } from "src/app/schema/subcategory";

@Injectable({
  providedIn: 'root'
})
export class SubcategoryService {

  constructor(private ApiService: ApiService,private http:HttpClient) { }
  
  public getAllPostBySubCateId(param):Observable<SubCategorySchemas.SubCategoryResponse>{
    return this.http.get<SubCategorySchemas.SubCategoryResponse>(this.ApiService.apiUrl.subcategory,{params:param});
  }

  public getAllSubCategory():Observable<SubCategorySchemas.SubCategoryDataResponse>{
    return this.http.get<SubCategorySchemas.SubCategoryDataResponse>(this.ApiService.apiUrl.getallsubcategory);
  }

}
