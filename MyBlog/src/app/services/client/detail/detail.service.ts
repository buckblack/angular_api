import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../api/api.service';
import { Injectable } from '@angular/core';
import { PostSchemas } from "src/app/schema/post";

@Injectable({
  providedIn: 'root'
})
export class DetailService {

  constructor(private ApiService: ApiService,private http:HttpClient) { }
  
  public getPostDetail(param):Observable<PostSchemas.PostDetailRespone>{
    return this.http.get<PostSchemas.PostDetailRespone>(this.ApiService.apiUrl.postDetail,{params:param});
  }

  public getPostDetailComment(param):Observable<PostSchemas.PostDetailCommentRespone>{
    return this.http.get<PostSchemas.PostDetailCommentRespone>(this.ApiService.apiUrl.postComment,{params:param});
  }
}
