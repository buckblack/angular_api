import { PostSchemas } from '../schema/post'
import { SubCategorySchemas } from '../schema/subcategory'
export namespace CategorySchemas {

    export interface CategoryDataOnly {
        Id: string;
        cateName: string;
    }
    export interface CategoryData {
        Id: string;
        cateName: string;
        subCategories: SubCategorySchemas.SubCategoryData[];
    }

    export interface CategoriesResponse {
        errorCode: number;
        message: string;
        data: CategoryData[];
    }

    export interface CategoryPostResponse {
        errorCode: number;
        message: string;
        data: PostSchemas.PostData[];
        totalPage: number;
        currentPage: number;
        totalRecords: number;
    }
}