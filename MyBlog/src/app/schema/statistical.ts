export namespace StatisticalSchemas {

    export interface StatisticalComment {
        Id: number;
        postTitle: string;
        sumComment: number;
    }

    export interface StatisticalCommentResponse {
        errorCode: number;
        message: string;
        data: StatisticalComment[];
    }

    export interface StatisticalPost {
        Id: number;
        subCateName: string;
        pathThumbnail: string;
        categoryId: number;
        sumPost: number;
    }

    export interface StatisticalPostResponse {
        errorCode: number;
        message: string;
        data: StatisticalPost[];
    }

    export interface StatisticalUser {
        Id: number;
        pathAvatar: string;
        fullName: string;
        sumPost: number;
    }

    export interface StatisticalUserResponse {
        errorCode: number;
        message: string;
        data: StatisticalUser[];
    }
}