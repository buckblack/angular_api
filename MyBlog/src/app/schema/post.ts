import { UserSchemas } from "../schema/user";
import { CommentSchemas } from "../schema/comment";
import { SubCategorySchemas } from "../schema/subcategory"
import { CategorySchemas } from "../schema/category"

export namespace PostSchemas {

    export interface PostData {
        Id: string;
        postTitle: string;
        summaryContent: string;
        detailContent: string;
        dateCreate: Date;
        pathThumbnail: string;
        status: number;
        subCategoriesId: number;
        userId: number;
        fullName: string;
        subCateName: string;
        pathAvatar: string;
        countPost: number;
        description: string;
        subCategories: SubCategorySchemas.SubCategoryDataOnly;
        user: UserSchemas.userData;
        comments: CommentSchemas.CommentData[];
    }

    export interface PostDataUpdate {
        IdUd: string;
        postTitleUd: string;
        summaryContentUd: string;
        detailContentUd: string;
        pathThumbnailUd: string;
        statusUd: number;
        subCategoriesIdUd: number;
    }


    export interface PostsRespone {
        errorCode: number;
        message: string;
        data: PostData[]
        totalPage: number;
        currentPage: number;
        totalRecords: number;
    }

    export interface PostDetailRespone {
        errorCode: number;
        message: string;
        data: PostData
        userPathImg: string;
        category: CategorySchemas.CategoryDataOnly;
    }

    export interface PostDetailCommentRespone {
        errorCode: number;
        message: string;
        data: CommentSchemas.CommentData[]
    }

    export interface HomePostsHotResponse {
        errorCode: number;
        message: string;
        data: PostData[];
    }

    export interface HomePostsRecentResponse {
        errorCode: number;
        message: string;
        data: PostData[];
        totalPage: number;
        currentPage: number;
        totalRecords: number;
    }

    export interface HomePostsRecentCommentResponse {
        errorCode: number;
        message: string;
        data: PostData[];
    }

    export interface DashboardResponse {
        countPost: string;
        countCategory: string;
        countGuest: string;
        countMember: string;
        lstPostNew: PostData[];
        lstPostHot: PostData[];
        lstCommentNew: CommentSchemas.CommentData[];
        message: string;
        errorCode: number;
    }

    export interface arrDataChartPost {
        name: string;
        count: number;
    }
}