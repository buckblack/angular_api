export namespace LoginSchemas {
        export interface LoginRequest {
            email: string;
            password: string;
        }

        export interface LoginResponse {
            token: string;
            errorCode: number;
            pathAvatar: string;
            fullName: string;
            role: string;
            message: string;
        }
    
}