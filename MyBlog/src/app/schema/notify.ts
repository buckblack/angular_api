import { UserSchemas } from "../schema/user";
export namespace NotifySchemas {

    export interface NotifyDetailResponse {
        data: NotifyData;
        errorCode: number;
        message: string;
    }

    export interface NotifyData {
        Id: number,
        contentNotify: string;
        dateCreate: Date;
        user: UserSchemas.userData;
        errorCode: number;
        message: string;
    }

    export interface NotifyResponse {
        errorCode: number;
        message: string;
        data: NotifyData[];
    }
}