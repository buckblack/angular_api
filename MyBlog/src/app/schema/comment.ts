import { GuestSchemas } from '../schema/guest'
export namespace CommentSchemas {

    export interface CommentData {
        Id: string;
        contentComment: string;
        dateCreate: Date;
        guest: GuestSchemas.guestData;
    }

    export interface CommentResponse {
        errorCode: number;
        message: string;
        data: CommentData[]
    }

    export interface SubmitCommentRespone {
        errorCode: number;
        message: string;
    }

    export interface arrDataChartComment {
        name: string;
        title: string;
        count: number;
    }

}