export namespace GuestSchemas {

    export interface guestData {
        Id: string;
        email: string;
        fullName: string;
    }

    export interface guestResponse {
        errorCode: number;
        message: string;
        data: guestData[];
    }
}