import { PostSchemas } from '../schema/post'
export namespace SubCategorySchemas {

    export interface SubCategoryData {
        Id: string;
        fullName: string;
        pathAvatar: string;
        description: string;
        subCateName: string;
        pathThumbnail: string;
        countPost: number;
    }

    export interface SubCategoryDataOnly {
        Id: string;
        subCateName: string;
        pathThumbnail: string;
    }

    export interface CategorySubResponse {
        errorCode: number;
        message: string;
        data: SubCategoryData[];
    }

    export interface CategoryPostResponse {
        errorCode: number;
        message: string;
        data: PostSchemas.PostData[];
        totalPage: number;
        currentPage: number;
        totalRecords: number;
    }

    export interface SubCategoryResponse {
        errorCode: number;
        message: string;
        data: PostSchemas.PostData[];
        totalPage: number;
        currentPage: number;
        totalRecords: number;
        subCateName: string;
        cateName: string;
    }


    export interface SubCategoryDataResponse {
        errorCode: number;
        message: string;
        data: SubCategoryData[];
    }
}