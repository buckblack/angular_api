import { UserSchemas } from "../schema/user";
export namespace RoleSchemas {
    export interface RoleData {
        Id: number;
        roleName: string;
        users: UserSchemas.userData[];
    }

    export interface RoleResponse {
        errorCode: number;
        message: string;
        data: RoleData[];
    }
}