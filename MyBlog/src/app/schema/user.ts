import { CommentSchemas } from '../schema/comment'
import { RoleSchemas } from '../schema/role'
export namespace UserSchemas {

    export interface userResponse {
        errorCode: number;
        message: string;
        data: userData[];
    }

    export interface guestData {
        Id: string;
        email: string;
        fullName: string;
        comment: CommentSchemas.CommentData[]
    }

    export interface userData {
        Id: string;
        fullName: string;
        pathAvatar: string;
        description: string;
        phoneNumber: string;
        email: string;
        address: string;
        roleId: string;
        password: string;
        status: number;
        role: RoleSchemas.RoleData[]
    }

    export interface arrDataChartUser {
        name: string;
        avatar: string;
        count: number;
    }
}