import { Router } from '@angular/router';
import { PostSchemas } from "src/app/schema/post";
import { PostService } from '../../services/client/post/post.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit {

  lstPostsSearch: PostSchemas.PostData[] = [] as PostSchemas.PostData[];
  currentPage: number = 1;
  totalPage: number;
  key: string = '';
  sortKey: string;
  sortAuthor:boolean=false;
  sortTitle:boolean=false;
  sortContent:boolean=false;
  sl_sort:number=0;
  checkSort:boolean;//true: click button sort
  @ViewChild("modalLoading") modalLoading:ModalDirective;
  constructor(private title: Title, 
    private route: ActivatedRoute, 
    private router: Router, 
    private postService: PostService,
    private toast:ToastrService) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit(): void {
    this.title.setTitle("Search | Mini Blog");
    this.key = this.route.snapshot.paramMap.get('key');
    this.loadPostsSearch(this.currentPage);
  }

  //key must more 3 charater
  checkKey() {
    if (this.key.trim().length < 3) {
      return;
    }
  }

  loadPostsSearch(page) {
    this.checkKey();
    this.checkSort=false;
    //create parameters
    let param = new HttpParams();
    param = param.append('searchKey', this.route.snapshot.paramMap.get('key'));
    param = param.append('page', page);
    //call api and set data
    this.postService.getPostsSearch(param).subscribe(result => {
      if (result.errorCode === 0) {
        this.lstPostsSearch = result.data;
        this.totalPage = result.totalPage;
        this.currentPage = result.currentPage;
      }
      else {
        console.log(result.message);
      }
    })
  }

  loadNewPage(newPage) {
    if (typeof (newPage) === typeof (1)) {
      //load data new page
      this.loadPostsSearch(newPage);
      //scroll to href
      this.scrollToTop();
    }
  }

  scrollToTop()
  {
    let x = document.querySelector("#search");
    if (x) {
      x.scrollIntoView();
    }
  }

  Sort(page)
  {
    //check key
    if (this.sortKey == undefined || this.sortKey.trim().length < 3) {
      this.toast.info("Please enter more 3 character!");
      return;
    }
    this.modalLoading.show();
    //create parameters
    const param:FormData=new FormData();
    param.append("key",this.sortKey.trim());
    param.append("author",String(this.sortAuthor));
    param.append("title",String(this.sortTitle));
    param.append("content",String(this.sortContent));
    param.append("sort",this.sl_sort.toString());
    param.append("page",page);
    this.checkSort=true;
    this.key=this.sortKey;
    //call api and set data
    this.postService.getSortSearch(param).subscribe(result => {
      if (result.errorCode === 0) {
        this.lstPostsSearch = result.data;
        this.totalPage = result.totalPage;
        this.currentPage = result.currentPage;
        //scroll to href
        this.scrollToTop();
        this.modalLoading.hide();
      }
      else {
        console.log(result.message);
      }
    })
    
  }

}
