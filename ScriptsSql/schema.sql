USE [master]
GO
/****** Object:  Database [API_Blog]    Script Date: 4/17/2020 5:47:24 PM ******/
CREATE DATABASE [API_Blog]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'API_Blog', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\API_Blog.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'API_Blog_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\API_Blog_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [API_Blog] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [API_Blog].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [API_Blog] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [API_Blog] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [API_Blog] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [API_Blog] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [API_Blog] SET ARITHABORT OFF 
GO
ALTER DATABASE [API_Blog] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [API_Blog] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [API_Blog] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [API_Blog] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [API_Blog] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [API_Blog] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [API_Blog] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [API_Blog] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [API_Blog] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [API_Blog] SET  ENABLE_BROKER 
GO
ALTER DATABASE [API_Blog] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [API_Blog] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [API_Blog] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [API_Blog] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [API_Blog] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [API_Blog] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [API_Blog] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [API_Blog] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [API_Blog] SET  MULTI_USER 
GO
ALTER DATABASE [API_Blog] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [API_Blog] SET DB_CHAINING OFF 
GO
ALTER DATABASE [API_Blog] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [API_Blog] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [API_Blog] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [API_Blog] SET QUERY_STORE = OFF
GO
USE [API_Blog]
GO
/****** Object:  Table [dbo].[SubCategories]    Script Date: 4/17/2020 5:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCategories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[subCateName] [nvarchar](max) NULL,
	[pathThumbnail] [nvarchar](max) NULL,
	[categoryId] [int] NOT NULL,
 CONSTRAINT [PK_SubCategories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 4/17/2020 5:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[fullName] [nvarchar](max) NULL,
	[phoneNumber] [nvarchar](max) NULL,
	[email] [nvarchar](max) NULL,
	[pathAvatar] [nvarchar](max) NULL,
	[description] [nvarchar](max) NULL,
	[address] [nvarchar](max) NULL,
	[password] [nvarchar](max) NULL,
	[status] [int] NOT NULL,
	[roleId] [int] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Posts]    Script Date: 4/17/2020 5:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Posts](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[postTitle] [nvarchar](max) NULL,
	[summaryContent] [nvarchar](max) NULL,
	[detailContent] [nvarchar](max) NULL,
	[dateCreate] [datetime2](7) NOT NULL,
	[pathThumbnail] [nvarchar](max) NULL,
	[status] [int] NOT NULL,
	[subCategoriesId] [int] NOT NULL,
	[userId] [int] NOT NULL,
 CONSTRAINT [PK_Posts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comments]    Script Date: 4/17/2020 5:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comments](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[contentComment] [nvarchar](max) NULL,
	[dateCreate] [datetime2](7) NOT NULL,
	[postId] [bigint] NOT NULL,
	[guestId] [int] NOT NULL,
 CONSTRAINT [PK_Comments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[PostsHotViews]    Script Date: 4/17/2020 5:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[PostsHotViews] as
                select hp.Id, hp.postTitle,hp.summaryContent,hp.dateCreate,hp.status,hp.pathThumbnail,u.fullName,s.subCateName from Posts hp
                inner join
                (select top 20 p.Id,count(c.postId) as sumcomments from dbo.Posts p
                left join dbo.Comments c
                on c.postId = p.Id
                where p.status=0
                group by c.postId,p.Id
                order by sumcomments desc) v1
                on v1.Id=hp.Id
                inner join dbo.SubCategories s
                on s.Id = hp.subCategoriesId
                inner join dbo.Users u
                on u.id = hp.userId
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 4/17/2020 5:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 4/17/2020 5:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[cateName] [nvarchar](max) NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Guests]    Script Date: 4/17/2020 5:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guests](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](max) NULL,
	[fullName] [nvarchar](max) NULL,
 CONSTRAINT [PK_Guests] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notifies]    Script Date: 4/17/2020 5:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notifies](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[notifyTitle] [nvarchar](max) NULL,
	[contentNotify] [nvarchar](max) NULL,
	[dateCreate] [datetime2](7) NOT NULL,
	[userId] [int] NOT NULL,
 CONSTRAINT [PK_Notifies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 4/17/2020 5:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[roleName] [nvarchar](max) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_Comments_guestId]    Script Date: 4/17/2020 5:47:24 PM ******/
CREATE NONCLUSTERED INDEX [IX_Comments_guestId] ON [dbo].[Comments]
(
	[guestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Comments_postId]    Script Date: 4/17/2020 5:47:24 PM ******/
CREATE NONCLUSTERED INDEX [IX_Comments_postId] ON [dbo].[Comments]
(
	[postId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Notifies_userId]    Script Date: 4/17/2020 5:47:24 PM ******/
CREATE NONCLUSTERED INDEX [IX_Notifies_userId] ON [dbo].[Notifies]
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Posts_subCategoriesId]    Script Date: 4/17/2020 5:47:24 PM ******/
CREATE NONCLUSTERED INDEX [IX_Posts_subCategoriesId] ON [dbo].[Posts]
(
	[subCategoriesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Posts_userId]    Script Date: 4/17/2020 5:47:24 PM ******/
CREATE NONCLUSTERED INDEX [IX_Posts_userId] ON [dbo].[Posts]
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_SubCategories_categoryId]    Script Date: 4/17/2020 5:47:24 PM ******/
CREATE NONCLUSTERED INDEX [IX_SubCategories_categoryId] ON [dbo].[SubCategories]
(
	[categoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Users_roleId]    Script Date: 4/17/2020 5:47:24 PM ******/
CREATE NONCLUSTERED INDEX [IX_Users_roleId] ON [dbo].[Users]
(
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD  CONSTRAINT [FK_Comments_Guests_guestId] FOREIGN KEY([guestId])
REFERENCES [dbo].[Guests] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Comments] CHECK CONSTRAINT [FK_Comments_Guests_guestId]
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD  CONSTRAINT [FK_Comments_Posts_postId] FOREIGN KEY([postId])
REFERENCES [dbo].[Posts] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Comments] CHECK CONSTRAINT [FK_Comments_Posts_postId]
GO
ALTER TABLE [dbo].[Notifies]  WITH CHECK ADD  CONSTRAINT [FK_Notifies_Users_userId] FOREIGN KEY([userId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Notifies] CHECK CONSTRAINT [FK_Notifies_Users_userId]
GO
ALTER TABLE [dbo].[Posts]  WITH CHECK ADD  CONSTRAINT [FK_Posts_SubCategories_subCategoriesId] FOREIGN KEY([subCategoriesId])
REFERENCES [dbo].[SubCategories] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Posts] CHECK CONSTRAINT [FK_Posts_SubCategories_subCategoriesId]
GO
ALTER TABLE [dbo].[Posts]  WITH CHECK ADD  CONSTRAINT [FK_Posts_Users_userId] FOREIGN KEY([userId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Posts] CHECK CONSTRAINT [FK_Posts_Users_userId]
GO
ALTER TABLE [dbo].[SubCategories]  WITH CHECK ADD  CONSTRAINT [FK_SubCategories_Categories_categoryId] FOREIGN KEY([categoryId])
REFERENCES [dbo].[Categories] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SubCategories] CHECK CONSTRAINT [FK_SubCategories_Categories_categoryId]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Roles_roleId] FOREIGN KEY([roleId])
REFERENCES [dbo].[Roles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Roles_roleId]
GO
/****** Object:  StoredProcedure [dbo].[StatisticalComment]    Script Date: 4/17/2020 5:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StatisticalComment] @dateFrom nvarchar(10), @dateTo nvarchar(10)
as
select hp.Id, hp.postTitle, sumComment from Posts hp
inner join
(select top 20 p.Id,count(c.postId) as sumcomment from dbo.Posts p
left join dbo.Comments c
on c.postId = p.Id
where p.status=0
AND c.dateCreate>=CONVERT(datetime,@dateFrom)
AND c.dateCreate<CONVERT(datetime,@dateTo)
group by c.postId,p.Id
order by sumComment desc) v1
on v1.Id=hp.Id
GO
/****** Object:  StoredProcedure [dbo].[StatisticalPost]    Script Date: 4/17/2020 5:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StatisticalPost] @dateFrom nvarchar(10), @dateTo nvarchar(10)
                as
                select * from SubCategories s,
                (select p.subCategoriesId,count(*) as sumPost from Posts p
                where p.dateCreate>=CONVERT(datetime,@dateFrom)
                AND p.dateCreate<CONVERT(datetime,@dateTo)
                group by p.subCategoriesId) as tmp
                where tmp.subCategoriesId=s.Id
GO
/****** Object:  StoredProcedure [dbo].[StatisticalUser]    Script Date: 4/17/2020 5:47:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StatisticalUser] @dateFrom nvarchar(10), @dateTo nvarchar(10)
                as
                select top 20 u.Id,u.pathAvatar,u.fullName,
	                (case 
		                when tmp.sumPost is NULL then 0
		                else tmp.sumPost
	                end) as sumPost
                from Users u
                left join
                (select top 20 p.userId, count(*) as sumPost from Posts p
                where p.status=0
                AND p.dateCreate>=CONVERT(datetime,@dateFrom)
                AND p.dateCreate<CONVERT(datetime,@dateTo)
                group by p.userId
                order by sumPost desc) as tmp
                on u.Id = tmp.userId
                order by tmp.sumPost desc
GO
USE [master]
GO
ALTER DATABASE [API_Blog] SET  READ_WRITE 
GO
