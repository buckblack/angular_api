﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_BlogCore.Requests;
using API_BlogCore.Ultils;
using API_BlogServices.Blo;
using API_BlogServices.Models;
using API_BlogServices.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API_Blog.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminNotifyController : ControllerBase
    {
        private readonly INotifyBlo _notifyBlo;
        private readonly IUserBlo _userBlo;
        private IMyLogger _logger;

        public AdminNotifyController(INotifyBlo notify, IMyLogger logger,IUserBlo user)
        {
            _userBlo = user;
            _notifyBlo = notify;
            _logger = logger;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("submitnotify")]
        public async Task<BaseResponse> SubmitNotify([FromForm]NotifyRequest request)
        {
            try
            {
                //check empty
                if(string.IsNullOrEmpty(request.title) || request.title.Trim()=="")
                {
                    return new BaseResponse() { errorCode = 2, message = BlogConst.msgNotifyTitleEmpty };
                }
                if (string.IsNullOrEmpty(request.content) || request.content.Trim() == "")
                {
                    return new BaseResponse() { errorCode = 2, message = BlogConst.msgNotifyDetailEmpty };
                }
                //add notify
                NotifyModel model = new NotifyModel();
                model.contentNotify = request.content;
                model.notifyTitle = request.title;
                model.dateCreate = DateTime.Now;
                model.userId= int.Parse(HttpContext.User.Claims.Where(x => x.Type == "UserId").FirstOrDefault().Value);
                await _notifyBlo.CreateNotify(model);
                return new BaseResponse()
                {
                    message = BlogConst.msgNotifySuccess
                };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpGet("getnotifynav")]
        public async Task<BaseResponse> GetNotifyNav()
        {
            try
            {
                //get notify for navbar
                int records = 4;
                List<NotifyModel> result = await _notifyBlo.GetNotify(records);
                return new BaseResponse()
                {
                    data = result
                };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpGet("getnotify")]
        public async Task<BaseResponse> GetNotify()
        {
            try
            {
                //get all notify
                List<NotifyModel> result = await _notifyBlo.GetNotify();
                return new BaseResponse()
                {
                    data = result
                };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpGet("getnotifydetail/{id}")]
        public async Task<BaseResponse> GetNotifyDetail(string id)
        {
            try
            {
                //check id notify
                long idCheck = 0;
                long.TryParse(id, out idCheck);
                if(idCheck == 0)
                {
                    return new BaseResponse() { errorCode = 2, message = BlogConst.msgNotifyDetailNotFound };
                }
                //get detail notify
                NotifyModel result = await _notifyBlo.FindNotifyByID(long.Parse(id));
                return new BaseResponse()
                {
                    data = result
                };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("deletenotify/{id}")]
        public async Task<BaseResponse> DeleteNotify(string id)
        {
            try
            {
                //check id notify
                long idCheck = 0;
                long.TryParse(id, out idCheck);
                int userId = int.Parse(HttpContext.User.Claims.Where(x => x.Type == "UserId").FirstOrDefault().Value);
                //find user
                UserModel user = _userBlo.FindUserById(userId);
                //check user exists
                if(user == null)
                {
                    return new BaseResponse() { errorCode = 3, message = BlogConst.msgCheckUserNotfound };
                }
                //check role of user
                if(user.role.roleName != "Admin")
                {
                    return new BaseResponse() { errorCode = 3, message = BlogConst.msgCheckUserRoleError };
                }
                //id not true
                if (idCheck == 0)
                {
                    return new BaseResponse() { errorCode = 2, message = BlogConst.msgNotifyDetailNotFound };
                }
                //find notify
                NotifyModel result = await _notifyBlo.FindNotifyByID(long.Parse(id));
                if (result == null)
                {
                    return new BaseResponse() { errorCode = 2, message = BlogConst.msgNotifyDetailNotFound };
                }
                //remove notify
                await _notifyBlo.DeleteNotify(result);
                return new BaseResponse()
                {
                    message = BlogConst.msgNotifyDeleteSuccess
                };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }
    }
}