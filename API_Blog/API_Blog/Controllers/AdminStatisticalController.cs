﻿using System;
using System.Collections.Generic;
using System.Linq;
using API_BlogCore.Requests;
using API_BlogCore.Ultils;
using API_BlogServices.Blo;
using API_BlogServices.Models.View;
using API_BlogServices.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace API_Blog.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminStatisticalController : ControllerBase
    {
        private readonly IStatisticalBlo _statisticalBlo;
        private IMyLogger _logger;
        public AdminStatisticalController(IStatisticalBlo statistical, IMyLogger logger)
        {
            _statisticalBlo = statistical;
            _logger = logger;
        }

        [HttpGet("getpostchartalltime")]
        public BaseResponse GetPostChartAllTime()
        {
            try
            {
                List<StatisticalPostView> result = _statisticalBlo.GetPostChartAllTime();
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpPost("getpostchartbytime")]
        public BaseResponse GetPostChartByTime(StatisticalRequest request)
        {
            try
            {
                List<StatisticalPostView> result = new List<StatisticalPostView>();
                //format datetime to string
                string dateFrom = request.dateFrom.ToString("yyyy-MM-dd");
                string dateTo = request.dateTo.AddDays(1).ToString("yyyy-MM-dd");
                //get data from procedure
                result = _statisticalBlo.GetPostChartByTime(dateFrom, dateTo);
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpGet("getcommentchartalltime")]
        public BaseResponse GetCommentChartAllTime()
        {
            try
            {
                int records = 20;
                List<StatisticalCommentView> result = _statisticalBlo.GetCommentChartAllTime(records);
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpPost("getcommentchartbytime")]
        public BaseResponse GetCommentChartByTime(StatisticalRequest request)
        {
            try
            {
                List<StatisticalCommentView> result = new List<StatisticalCommentView>();
                //format datetime to string
                string dateFrom = request.dateFrom.ToString("yyyy-MM-dd");
                string dateTo = request.dateTo.AddDays(1).ToString("yyyy-MM-dd");
                //get data from procedure
                result = _statisticalBlo.GetCommentChartByTime(dateFrom, dateTo);
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpGet("getuserchartalltime")]
        public BaseResponse GetUserChartAllTime()
        {
            try
            {
                int records = 20;
                List<StatisticalUserView> result = _statisticalBlo.GetUserChartAllTime(records);
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpPost("getuserchartbytime")]
        public BaseResponse GetUserChartByTime(StatisticalRequest request)
        {
            try
            {
                List<StatisticalUserView> result = new List<StatisticalUserView>();
                //format datetime to string
                string dateFrom = request.dateFrom.ToString("yyyy-MM-dd");
                string dateTo = request.dateTo.AddDays(1).ToString("yyyy-MM-dd");
                //get data from procedure
                result = _statisticalBlo.GetUserChartByTime(dateFrom, dateTo);
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }
    }
}