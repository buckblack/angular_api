﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API_BlogCore.Requests;
using API_BlogCore.Ultils;
using API_BlogDao.Dao;
using API_BlogServices.Blo;
using API_BlogServices.Models;
using API_BlogServices.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API_Blog.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminContentController : ControllerBase
    {
        private readonly ICategoryBlo _categoryBlo;
        private readonly IPostBlo _postBlo;
        private readonly ISubCategoryBlo _subCategoryBlo;
        private readonly ICommentBlo _commentBlo;
        private IMyLogger _logger;
        public AdminContentController(ICategoryBlo category, IPostBlo post, ISubCategoryBlo subCategory, ICommentBlo comment, IMyLogger logger)
        {
            _categoryBlo = category;
            _postBlo = post;
            _subCategoryBlo = subCategory;
            _commentBlo = comment;
            _logger = logger;
        }

        //************************************//
        //**start**  admin/content - post

        [HttpGet("getallcategoryforpost")]
        public BaseResponse GetAllCategoryForPost()
        {
            try
            {
                return new BaseResponse() { data = _categoryBlo.getAllCategory() };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpGet("getsubcategoryforpost/{cateid}")]
        public BaseResponse GetSubCategoryForPost(string cateid)
        {
            try
            {
                //check id category
                int id = 1;
                int.TryParse(cateid, out id);
                //get data
                List<SubCategoryModel> result = _subCategoryBlo.GetSubCategoryByCateId(id);
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpGet("getpostbysubcateid/{subCateid}")]
        public async Task<BaseResponse> GetPostBySubCateId(string subCateid)
        {
            try
            {
                //check id sub-category
                int id = 1;
                int.TryParse(subCateid, out id);
                //get data
                string pathImg = Helper.getUrl(Request);
                List<PostModel> result = await _subCategoryBlo.GetPostBySubCateId(id);
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpPost("searchpost")]
        public async Task<BaseResponse> SearchPostById([FromForm]PostRequest post)
        {
            try
            {
                string pathImg = Helper.getUrl(Request);
                List<PostModel> result = await _postBlo.SearchPostById(long.Parse(post.postId));
                string message = string.Empty;
                if(result==null || result.Count==0)
                {
                    message = BlogConst.msgUpdatePostNotFound;
                }
                return new BaseResponse() { data = result,message=message };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        private string checkPostInfo(PostRequest post)
        {
            if (_subCategoryBlo.CheckExistsSubCategory(int.Parse(post.subCateId)))
            {
                return BlogConst.msgPostSubCateNotFound;
            }
            if (string.IsNullOrEmpty(post.postTitle) || post.postTitle.Trim() == "")
            {
                return BlogConst.msgPostTitleEmpty;
            }
            if (string.IsNullOrEmpty(post.summaryContent) || post.summaryContent.Trim() == "")
            {
                return BlogConst.msgPostSummaryEmpty;
            }
            if (string.IsNullOrEmpty(post.detailContent) || post.detailContent.Trim() == "")
            {
                return BlogConst.msgPostDetailEmpty;
            }
            return string.Empty;
        }

        [HttpPost("detail")]
        public BaseResponse getDetail([FromForm]string postId)
        {
            try
            {
                //check id post
                int id = 1;
                int.TryParse(postId, out id);
                //get data
                string pathImg = Helper.getUrl(Request);
                PostModel result = _postBlo.GetPostDetail(id);
                return new PostDetailResponse() { data = result, category =_categoryBlo.FindCategoryById(result.subCategories.categoryId) };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpPost("addnewpost")]
        public async Task<BaseResponse> addNewPost([FromForm]PostRequest post)
        {
            try
            {
                string message = checkPostInfo(post);
                //check info
                if (!string.IsNullOrEmpty(message))
                {
                    return new BaseResponse() { message = message, errorCode = 2 };
                }
                //check image thumbnail exists or not
                if (post.pathThumbnail == null)
                {
                    return new BaseResponse() { message = BlogConst.msgPostImgEmpty, errorCode = 3 };
                }
                //add new post
                PostModel model = new PostModel();
                model.postTitle = post.postTitle;
                model.summaryContent = post.summaryContent;
                model.detailContent = post.detailContent;
                model.dateCreate = DateTime.Now;
                model.subCategoriesId = int.Parse(post.subCateId);
                model.status = 0;
                model.userId = int.Parse(HttpContext.User.Claims.Where(x => x.Type == "UserId").FirstOrDefault().Value);
                //upload image
                string pathImage = "wwwroot\\images";
                var uploadFilesPath = Path.Combine(pathImage, "posts");

                if (!Directory.Exists(uploadFilesPath))
                {
                    Directory.CreateDirectory(uploadFilesPath);
                }

                string newFileName = DateTime.Now.ToString("ddMMyyyyhhssmm") + "_" + post.pathThumbnail.FileName;

                //get new path
                string path = pathImage + "\\posts\\" + newFileName;

                //save new file
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    post.pathThumbnail.CopyTo(stream);
                    model.pathThumbnail = "/posts/" + newFileName;
                }
                //save
                await _postBlo.CreatePost(model);
                return new BaseResponse() { message = BlogConst.msgNewPostSuccess };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpPost("addmoreimageupdate")]
        public async Task<BaseResponse> AddMoreImageUpdate([FromForm]PostRequest post)
        {
            try
            {
                //check post exists or not
                PostModel model = _postBlo.FindPostById(long.Parse(post.postId));
                if (model == null)
                {
                    return new BaseResponse() { message = BlogConst.msgUpdatePostNotFound, errorCode = 3 };
                }
                //update content
                //model.userId = int.Parse(HttpContext.User.Claims.Where(x => x.Type == "UserId").FirstOrDefault().Value);
                //upload image thubmail
                if (post.pathThumbnailMore == null)
                {
                    return new BaseResponse() { message = BlogConst.contentAddMoreImgEmpty, errorCode = 4 };
                }
                string pathNewImg = string.Empty;
                string pathImage = "wwwroot\\images";
                var uploadFilesPath = Path.Combine(pathImage, "posts\\content\\" + post.postId);

                if (!Directory.Exists(uploadFilesPath))
                {
                    Directory.CreateDirectory(uploadFilesPath);
                }

                string newFileName = DateTime.Now.ToString("ddMMyyyyhhssmm") + "_" + post.pathThumbnailMore.FileName;
                //get new path
                string path = uploadFilesPath + "\\" + newFileName;
                //save new file
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    post.pathThumbnailMore.CopyTo(stream);
                    pathNewImg = "/posts/content/" + post.postId + "/" + newFileName;
                }
                model.detailContent = model.detailContent + string.Format(BlogConst.contentAddMoreImg, new string[] { Helper.getUrl(Request), pathNewImg });

                //save
                await _postBlo.UpdatePost(model);
                return new BaseResponse() { message = BlogConst.msgUpdatePostSuccess, data = model };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpPost("updatepost")]
        public async Task<BaseResponse> UpdatePost([FromForm]PostRequest post)
        {
            try
            {
                string message = checkPostInfo(post);
                //check info
                if (!string.IsNullOrEmpty(message))
                {
                    return new BaseResponse() { message = message, errorCode = 2 };
                }
                //check post exists or not
                PostModel model = _postBlo.FindPostById(long.Parse(post.postId));
                if (model == null)
                {
                    return new BaseResponse() { message = BlogConst.msgUpdatePostNotFound, errorCode = 3 };
                }
                //update
                model.postTitle = post.postTitle;
                model.summaryContent = post.summaryContent;
                model.detailContent = post.detailContent;
                model.subCategoriesId = int.Parse(post.subCateId);
                //model.userId = int.Parse(HttpContext.User.Claims.Where(x => x.Type == "UserId").FirstOrDefault().Value);
                //upload image thubmail
                if (post.pathThumbnail != null)
                {
                    string pathImage = "wwwroot\\images";
                    string oldImageName = pathImage + Path.Combine(model.pathThumbnail);
                    var uploadFilesPath = Path.Combine(pathImage, "posts");

                    if (!Directory.Exists(uploadFilesPath))
                    {
                        Directory.CreateDirectory(uploadFilesPath);
                    }

                    string newFileName = DateTime.Now.ToString("ddMMyyyyhhssmm") + "_" + post.pathThumbnail.FileName;

                    //get new path
                    string path = pathImage + "\\posts\\" + newFileName;

                    if (model.pathThumbnail != "")
                    {
                        if (System.IO.File.Exists(oldImageName))
                        {
                            System.IO.File.Delete(oldImageName);
                        }
                    }
                    //save new file
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        post.pathThumbnail.CopyTo(stream);
                        model.pathThumbnail = "/posts/" + newFileName;
                    }
                }
                //save
                await _postBlo.UpdatePost(model);
                return new BaseResponse() { message = BlogConst.msgUpdatePostSuccess };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpPost("updatestatuspost")]
        public async Task<BaseResponse> UpdateStatusPost([FromForm]PostRequest post)
        {
            try
            {
                //check post exists or not
                PostModel model = _postBlo.FindPostById(long.Parse(post.postId));
                if (model == null)
                {
                    return new BaseResponse() { message = BlogConst.msgUpdatePostNotFound, errorCode = 2 };
                }
                //update
                model.status = int.Parse(post.status);
                //save
                await _postBlo.UpdatePost(model);
                return new BaseResponse() { message = BlogConst.msgUpdateStatusPostSuccess };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpGet("removetrashimagecontent/{postId}")]
        public BaseResponse RemoveTrashImageContent(string postId)
        {
            try
            {
                //check post exists or not
                PostModel model = _postBlo.FindPostById(long.Parse(postId));
                if (model == null)
                {
                    return new BaseResponse() { message = BlogConst.msgUpdatePostNotFound, errorCode = 2 };
                }
                //remove image
                string contentPost = model.detailContent;
                string pathImage = "wwwroot\\images\\posts\\content\\"+postId;
                if(!Directory.Exists(pathImage))
                {
                    return new BaseResponse() { message = BlogConst.msgRemoveTrashImageNull, errorCode = 3 };
                }
                //get all path file in folder content post
                string[] file = Directory.GetFiles(pathImage);
                //list path will be removed
                List<string> lstFileWillRemote = new List<string>();
                foreach (string s in Directory.GetFiles(pathImage))
                {
                    if (!contentPost.Contains(Path.GetFileName(s)))
                    {
                        lstFileWillRemote.Add(s);
                    }
                }
                //delete file
                foreach(string path in lstFileWillRemote)
                {
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                }
                return new BaseResponse() { message = BlogConst.msgRemoveTrashImageSuccess };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        //**end** admin/content - post
        //************************************//

        //**start**  admin/content - category
        [HttpGet("getcategoryoptionsort")]
        public BaseResponse GetCategoryOptionSort()
        {
            try
            {
                //add option All
                List<CategoryModel> model = new List<CategoryModel>();
                model.Add(new CategoryModel() { Id = 0, cateName = "All" });
                model.AddRange(_categoryBlo.getAllCategory());
                return new BaseResponse() { data = model };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpGet("getsubcategoryoptionsort/{cateid}")]
        public async Task<BaseResponse> GetSubCategoryOptionSort(string cateid)
        {
            try
            {
                //check id category
                int id = 1;
                int.TryParse(cateid, out id);
                //get data
                string pathImg = Helper.getUrl(Request);
                List<SubCategoryModel> result = await _subCategoryBlo.GetSubCategoriesByCateIdSync(id);
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpGet("getsortresultcategorybycateid/{cateid}")]
        public async Task<BaseResponse> GetSortResultCategoryByCateId(string cateid)
        {
            try
            {
                //check id
                int id = 0;
                int.TryParse(cateid, out id);
                //get data
                object result;
                if (id == 0)
                {
                    result = await _categoryBlo.GetAllCategorySync();
                }
                else
                {
                    string pathImg = Helper.getUrl(Request);
                    result = await _subCategoryBlo.GetSubCategoriesByCateIdSync(id);
                }
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpPost("createcategory")]
        public async Task<BaseResponse> CreateCategory([FromForm]CategoryRequest request)
        {
            string messsage = string.Empty;
            int code = 0;
            try
            {
                //check empty
                if (string.IsNullOrEmpty(request.cateName) || request.cateName.Trim() == "")
                {
                    messsage = BlogConst.msgCreateCategoryNullName;
                    code = 2;
                }
                else
                {
                    //check exists
                    if (_categoryBlo.CheckExistsCategoryByCateName(request.cateName.Trim()))
                    {
                        messsage = BlogConst.msgCreateCategoryNameExists;
                        code = 3;
                    }
                    else
                    {
                        //add new category
                        await _categoryBlo.CreateCategory(new CategoryModel() { cateName = request.cateName.Trim() });
                        messsage = BlogConst.msgCreateCategorySuccess;
                    }
                }
                return new BaseResponse() { message = messsage, errorCode = code };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpPost("updatecategory")]
        public async Task<BaseResponse> UpdateCategory([FromForm]CategoryRequest request)
        {
            string messsage = string.Empty;
            int code = 0;
            try
            {
                //check empty
                if (string.IsNullOrEmpty(request.cateName) || request.cateName.Trim() == "")
                {
                    messsage = BlogConst.msgUpdateCategoryNullName;
                    code = 2;
                }
                else
                {
                    //check exists name
                    if (_categoryBlo.CheckExistsCategoryByCateName(request.cateName.Trim()))
                    {
                        messsage = BlogConst.msgUpdateCategoryNameExists;
                        code = 3;
                    }
                    else
                    {
                        //check and find this category
                        CategoryModel model = _categoryBlo.FindCategoryById(int.Parse(request.cateId));
                        if (model != null)
                        {
                            model.cateName = request.cateName.Trim();
                            await _categoryBlo.UpdateCategory(model);
                            messsage = BlogConst.msgUpdateCategorySuccess;
                        }
                        else
                        {
                            messsage = BlogConst.msgUpdateCategoryNullId;
                        }
                    }
                }
                return new BaseResponse() { message = messsage, errorCode = code };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpDelete("deletecategory/{cateId}")]
        public async Task<BaseResponse> DeleteCategory(string cateId)
        {
            string messsage = string.Empty;
            int code = 0;
            try
            {
                //find category
                CategoryModel model = _categoryBlo.FindCategoryById(int.Parse(cateId));
                if (model != null)
                {
                    //connot remove if category have subcategory
                    if (model.subCategories != null && model.subCategories.Count > 0)
                    {
                        messsage = BlogConst.msgDeleteCategoryIsUsed;
                        code = 3;
                    }
                    else
                    {
                        //delete category
                        await _categoryBlo.DeleteCategory(model);
                        messsage = BlogConst.msgDeleteCategorySuccess;
                    }
                }
                else
                {
                    messsage = BlogConst.msgDeleteCategoryNotFound;
                    code = 2;
                }
                return new BaseResponse() { message = messsage, errorCode = code };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpPost("createsubcategory")]
        public async Task<BaseResponse> CreateSubCategory([FromForm]SubCategoryRequest request)
        {
            string messsage = string.Empty;
            int code = 0;
            try
            {
                //check empty
                if (string.IsNullOrEmpty(request.subCateName) || request.subCateName.Trim() == "")
                {
                    messsage = BlogConst.msgCreateSubCategoryNullName;
                    code = 2;
                }
                else
                {
                    //check exists name sub-category in category
                    if (_subCategoryBlo.CheckExistsSubCateNameInCategory(request.subCateName.Trim(), int.Parse(request.cateId)))
                    {
                        messsage = BlogConst.msgCreateSubCategoryNameExists;
                        code = 3;
                    }
                    else
                    {
                        //check image
                        if (request.thumbnailSubCate != null)
                        {
                            SubCategoryModel model = new SubCategoryModel();
                            model.subCateName = request.subCateName;
                            model.categoryId = int.Parse(request.cateId);
                            string pathImage = "wwwroot\\images";
                            var uploadFilesPath = Path.Combine(pathImage, "subcategory");
                            //check and create folder
                            if (!Directory.Exists(uploadFilesPath))
                            {
                                Directory.CreateDirectory(uploadFilesPath);
                            }
                            //file name
                            string newFileName = DateTime.Now.ToString("ddMMyyyyhhssmm") + "_" + request.thumbnailSubCate.FileName;
                            //get new path
                            string path = pathImage + "\\subcategory\\" + newFileName;
                            //save new file
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                request.thumbnailSubCate.CopyTo(stream);
                                model.pathThumbnail = "/subcategory/" + newFileName;
                            }
                            //save database
                            await _subCategoryBlo.CreateSubCategory(model);
                            messsage = BlogConst.msgCreateSubCategorySuccess;
                        }
                        else
                        {
                            messsage = BlogConst.msgCreateSubCategoryNullImg;
                            code = 4;
                        }
                    }
                }
                return new BaseResponse() { message = messsage, errorCode = code };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode=1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpPost("updatesubcategory")]
        public async Task<BaseResponse> UpdateSubCategory([FromForm]SubCategoryRequest request)
        {
            string messsage = string.Empty;
            int code = 0;
            try
            {
                //check empty
                if (string.IsNullOrEmpty(request.subCateName) || request.subCateName.Trim() == "")
                {
                    messsage = BlogConst.msgUpdateSubCategoryNullName;
                    code = 2;
                }
                else
                {
                    //checked exists name sub-category in category
                    if (_subCategoryBlo.CheckExistsSubCateNameInCategoryUpdate(request.subCateName.Trim(), int.Parse(request.cateId)))
                    {
                        messsage = BlogConst.msgUpdateSubCategoryNameExists;
                        code = 3;
                    }
                    else
                    {
                        //find and update sub-category
                        SubCategoryModel model = _subCategoryBlo.FindSubCategoryById(int.Parse(request.subCateId));
                        if (model != null)
                        {
                            model.subCateName = request.subCateName.Trim();
                            model.categoryId = int.Parse(request.cateId);
                            //check image
                            if (request.thumbnailSubCate != null)
                            {
                                
                                string pathImage = "wwwroot\\images";
                                var uploadFilesPath = Path.Combine(pathImage, "subcategory");
                                //check and create path
                                if (!Directory.Exists(uploadFilesPath))
                                {
                                    Directory.CreateDirectory(uploadFilesPath);
                                }

                                //get old path
                                string oldImageName = pathImage + Path.Combine(model.pathThumbnail);
                                //remove old file
                                if (model.pathThumbnail != "")
                                {
                                    if (System.IO.File.Exists(oldImageName))
                                    {
                                        System.IO.File.Delete(oldImageName);
                                    }
                                }
                                //file name
                                string newFileName = DateTime.Now.ToString("ddMMyyyyhhssmm") + "_" + request.thumbnailSubCate.FileName;
                                //get new path
                                string path = pathImage + "\\subcategory\\" + newFileName;
                                //save new file
                                using (var stream = new FileStream(path, FileMode.Create))
                                {
                                    request.thumbnailSubCate.CopyTo(stream);
                                    model.pathThumbnail = "/subcategory/" + newFileName;
                                }
                            }
                            //save database
                            await _subCategoryBlo.UpdateSubCategory(model);
                            messsage = BlogConst.msgUpdateSubCategorySuccess;
                        }
                        else
                        {
                            messsage = BlogConst.msgUpdateSubCategoryNullId;
                        }
                    }
                }
                return new BaseResponse() { message = messsage, errorCode = code };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpDelete("deletesubcategory/{subCateId}")]
        public async Task<BaseResponse> DeleteSubCategory(string subCateId)
        {
            string messsage = string.Empty;
            int code = 0;
            try
            {
                //find subcategory
                SubCategoryModel model = _subCategoryBlo.FindSubCategoryById(int.Parse(subCateId));
                if (model != null)
                {
                    //check sub-category have post or not
                    if (model.posts != null && model.posts.Count > 0)
                    {
                        messsage = BlogConst.msgDeleteSubCategoryIsUsed;
                        code = 3;
                    }
                    else
                    {
                        string pathImage = "wwwroot\\images";
                        //get old path
                        string oldImageName = pathImage + Path.Combine(model.pathThumbnail);
                        //remove old file
                        if (model.pathThumbnail != "")
                        {
                            if (System.IO.File.Exists(oldImageName))
                            {
                                System.IO.File.Delete(oldImageName);
                            }
                        }
                        //save database
                        await _subCategoryBlo.DeleteSubCategory(model);
                        messsage = BlogConst.msgDeleteSubCategorySuccess;
                    }
                }
                else
                {
                    messsage = BlogConst.msgDeleteSubCategoryNotFound;
                    code = 2;
                }
                return new BaseResponse() { message = messsage, errorCode = code };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }
        //**end**  admin/content - category

        //*****************************//
        //start admin/content - comment

        [HttpPost("searchpostscomment")]
        public async Task<BaseResponse> SearchPostsComment([FromForm]string key)
        {
            try
            {
                List<PostModel> result = await _postBlo.SearchPostsComment(key);
                return new BaseResponse() { data = result };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpPost("getcomments")]
        public async Task<BaseResponse> getComments([FromForm]string postId)
        {
            try
            {
                List<CommentModel> result = await _commentBlo.FindCommentByPostId(long.Parse(postId));
                return new BaseResponse() { data = result };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpPost("removecomment")]
        public async Task<BaseResponse> RemoveComment([FromForm]string commentId)
        {
            try
            {
                int code = 0;
                string message = string.Empty;
                //find comment
                CommentModel model = _commentBlo.FindCommentById(long.Parse(commentId));
                //check exists or not
                if(model != null)
                {
                    //remove comment
                    await _commentBlo.DeleteComment(model);
                    message = BlogConst.msgDeleteCommentSuccess;
                }
                else
                {
                    code = 2;
                    message = BlogConst.msgDeleteCommentNotFound;
                }
                return new BaseResponse() { message = message, errorCode = code };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        //**end** admin/content - comment
        //************************************//
    }
}