﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_BlogCore.Ultils;
using API_BlogServices.Blo;
using API_BlogServices.Models;
using API_BlogServices.Models.View;
using API_BlogServices.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API_Blog.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminDashboardController : ControllerBase
    {
        private readonly ISubCategoryBlo _subCategoryBlo;
        private readonly IPostBlo _postBlo;
        private readonly ICommentBlo _commentBlo;
        private readonly IUserBlo _userBlo;
        private readonly IGuestBlo _guestBlo;
        private IMyLogger _logger;
        public AdminDashboardController(ISubCategoryBlo subCategory, IMyLogger logger, IPostBlo post, ICommentBlo comment,IUserBlo user,IGuestBlo guest)
        {
            _guestBlo = guest;
            _userBlo = user;
            _commentBlo = comment;
            _postBlo = post;
            _subCategoryBlo = subCategory;
            _logger = logger;
        }

        [HttpGet("dashboardchart")]
        public BaseResponse DashBoardChart()
        {
            try
            {
                //get data for chart
                List<SubCategoryModel> lstChartDashboard = _subCategoryBlo.GetAllSubCategory();
                return new BaseResponse()
                {
                    data=lstChartDashboard,
                };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new DashBoardResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpGet("dashboard")]
        public async Task<DashBoardResponse> DashBoard()
        {
            try
            {
                //get list new post
                List<PostModel> lstPostNew = await _postBlo.GetPostsNewDashBoard(20);

                //get list new comment
                List<CommentModel> lstCommentNew = await _commentBlo.GetNewCommentDashboard(40);
                //get list hot post
                List<PostsHotView> lstPostHot = await _postBlo.GetPostsHotDashBoard(20);
                //count category(subcategory)
                string countCategory = _subCategoryBlo.GetAllSubCategory().Count.ToString();
                //count guest
                string countGuest = _guestBlo.GetAllGuest().Result.Count().ToString();
                //count user
                string countMember = _userBlo.GetAllUser().Result.Count().ToString();
                //count post
                string countPost = _postBlo.CountAllPost().ToString();
                return new DashBoardResponse()
                {
                    errorCode = 0,
                    countCategory = countCategory,
                    countGuest = countGuest,
                    countMember = countMember,
                    countPost = countPost,
                    lstPostNew = lstPostNew,
                    lstCommentNew = lstCommentNew,
                    lstPostHot = lstPostHot,
                };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new DashBoardResponse() { errorCode = 1, message = ex.ToString() };
            }
        }
    }
}