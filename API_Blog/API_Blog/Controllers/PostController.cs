﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_BlogCore.Common;
using API_BlogCore.Requests;
using API_BlogCore.Ultils;
using API_BlogServices.Blo;
using API_BlogServices.Models;
using API_BlogServices.Models.View;
using API_BlogServices.Responses;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace API_Blog.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IPostBlo _postBlo;
        private IMyLogger _logger;
        private readonly ICommon _common;
        private readonly ICommentBlo _commentBlo;
        private readonly IGuestBlo _guestBlo;
        public PostController(IPostBlo post, IMyLogger logger, ICommon common,ICommentBlo comment,IGuestBlo guest)
        {
            _common = common;
            _postBlo = post;
            _logger = logger;
            _commentBlo = comment;
            _guestBlo = guest;
        }

        [HttpGet("getpostshot")]
        public BaseResponse getPostsHot()
        {
            try
            {
                string pathImg = Helper.getUrl(Request);
                List<PostsHotView> result = new List<PostsHotView>();
                int recordDisplay = 9;
                result = _postBlo.GetPostsHot(recordDisplay);
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpGet("getpostsrelated")]
        public BaseResponse getPostsRelated(string postId, string page)
        {
            try
            {
                //check post id
                long Id = 1;//postId
                long.TryParse(postId, out Id);
                //find post
                PostModel model = _postBlo.FindPostById(Id);
                //check exists post
                if (model != null)
                {
                    //set paging
                    int currentPage = 1;
                    int sumDisplay = 12;
                    long totalRecodrs = _postBlo.CountPostsRelatedRecord(model.subCategoriesId, Id);
                    int.TryParse(page, out currentPage);
                    if (currentPage < 1)
                    {
                        currentPage = 1;
                    }
                    //return data and paging
                    string pathImg = Helper.getUrl(Request);
                    return new PostPagingResponse()
                    {
                        currentPage = currentPage,
                        totalRecords = totalRecodrs,
                        totalPage = (int)Math.Ceiling((double)totalRecodrs / sumDisplay),
                        data = _postBlo.GetPostsRelatedPaging(model.subCategoriesId, Id, currentPage, sumDisplay),
                    };
                }
                return new BaseResponse() { errorCode = 2, message = "Data not found" };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpGet("getpostsrecent")]
        public BaseResponse getPostsRecent(string page)
        {
            try
            {
                //set paging
                int currentPage = 1;
                int sumDisplay = 9;
                long totalRecodrs = _postBlo.CountAllPostEnable();
                int.TryParse(page, out currentPage);
                if (currentPage < 1)
                {
                    currentPage = 1;
                }
                //return data and paging
                string pathImg = Helper.getUrl(Request);
                return new PostPagingResponse()
                {
                    currentPage = currentPage,
                    totalRecords = totalRecodrs,
                    totalPage = (int)Math.Ceiling((double)totalRecodrs / sumDisplay),
                    data = _postBlo.GetPostsRecentPaging(currentPage, sumDisplay)
                };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpGet("getpostsrecentcomment")]
        public BaseResponse getPostsRecentComment()
        {
            try
            {
                string pathImg = Helper.getUrl(Request);
                List<PostModel> result = new List<PostModel>();
                int recordDisplay = 12;
                result = _postBlo.GetPostsRecentComment(recordDisplay);
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpGet("detail")]
        public BaseResponse getDetail(string postId)
        {
            try
            {
                //check post id
                int id = 1;
                int.TryParse(postId, out id);
                //get data
                string pathImg = Helper.getUrl(Request);
                PostModel result = _postBlo.GetPostDetail(id);
                return new PostDetailResponse() { data = result};
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpGet("comment")]
        public BaseResponse getComment(string postId)
        {
            try
            {
                //check post id
                int id = 1;
                int.TryParse(postId, out id);
                //get data
                List<CommentModel> result = _commentBlo.GetCommentByPostId(id);
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpGet("search")]
        public BaseResponse search(string searchKey, string page)
        {
            try
            {
                //set paging
                int currentPage = 1;
                int sumDisplay = 15;
                long totalRecodrs = _postBlo.CountSearch(searchKey);
                int.TryParse(page, out currentPage);
                if (currentPage < 1)
                {
                    currentPage = 1;
                }
                //return data and paging
                string pathImg = Helper.getUrl(Request);
                return new PostPagingResponse()
                {
                    currentPage = currentPage,
                    totalRecords = totalRecodrs,
                    totalPage = (int)Math.Ceiling((double)totalRecodrs / sumDisplay),
                    data = _postBlo.Search(searchKey, currentPage, sumDisplay),
                };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpPost("sortsearch")]
        public BaseResponse SortSearch([FromForm]SearchRequest request)
        {
            try
            {
                //get data request
                bool author = bool.Parse(request.author);
                bool title = bool.Parse(request.title);
                bool content = bool.Parse(request.content);
                int sort = int.Parse(request.sort);
                //sort by key
                List<PostModel> result = _postBlo.SortSearch(request.key, author, title, content, sort);
                //set paging
                int currentPage = 1;
                int sumDisplay = 15;
                long totalRecodrs = result.Count();
                int.TryParse(request.page, out currentPage);
                if (currentPage < 1)
                {
                    currentPage = 1;
                }
                //paging
                result = _postBlo.SortSearchPaging(result, currentPage, sumDisplay);
                //return data and paging
                return new PostPagingResponse()
                {
                    currentPage = currentPage,
                    totalRecords = totalRecodrs,
                    totalPage = (int)Math.Ceiling((double)totalRecodrs / sumDisplay),
                    data = result,
                };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpPost("submitcomment")]
        public BaseResponse submitComment(CommentRequest request)
        {
            try
            {
                //check valid
                if (!Helper.IsValidEmail(request.guestEmail))
                {
                    return new BaseResponse() { message = BlogConst.msgCheckUserEmail, errorCode = 1 };
                }
                if (string.IsNullOrEmpty(request.guestName) || string.IsNullOrEmpty(request.guestName.Trim()))
                {
                    return new BaseResponse() { message = BlogConst.msgGuestNameEmpty, errorCode = 1 };
                }
                if (string.IsNullOrEmpty(request.contentComment) || request.contentComment.Trim().Length < 50)
                {
                    return new BaseResponse() { message = BlogConst.msgGuestContent, errorCode = 1 };
                }
                //check guest exists or not
                GuestModel model = _guestBlo.FindGuestByEmail(request.guestEmail.Trim());
                if (string.IsNullOrEmpty(model.email))
                {
                    //add new if not exists
                    _guestBlo.CreateGuest(request);
                }
                //get guest info
                GuestModel guest = _guestBlo.FindGuestByEmail(request.guestEmail.Trim());
                if (!string.IsNullOrEmpty(guest.email))
                {
                    //update name
                    //guest.fullName = request.guestName;
                    _guestBlo.UpdateGuest(request, guest.Id);
                }
                //add comment
                _commentBlo.CreateComment(request, guest.Id);
                return new BaseResponse() {  message = BlogConst.msgSubmitSuccess };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [HttpPost("addnewsubscriber")]
        public BaseResponse AddNewSubscriber([FromForm]CommentRequest request)
        {
            try
            {
                //check valid email
                if(!Helper.IsValidEmail(request.guestEmail))
                {
                    return new BaseResponse() { message = BlogConst.msgCheckUserEmail, errorCode = 1 };
                }
                //check guest exists or not
                GuestModel model = _guestBlo.FindGuestByEmail(request.guestEmail.Trim());
                if (model == null)
                {
                    //add new if not exists
                    _guestBlo.CreateGuest(request);
                }
                //add comment
                return new BaseResponse() { message = BlogConst.msgSubmitSuccess };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }
    }
}