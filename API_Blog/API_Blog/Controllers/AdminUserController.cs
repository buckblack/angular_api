﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API_BlogCore.Requests;
using API_BlogCore.Ultils;
using API_BlogServices.Blo;
using API_BlogServices.Models;
using API_BlogServices.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API_Blog.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminUserController : ControllerBase
    {
        private readonly IUserBlo _userBlo;
        private readonly IRoleBlo _roleBlo;
        private readonly IGuestBlo _guestBlo;
        private IMyLogger _logger;

        public AdminUserController(IUserBlo user,IMyLogger logger,IRoleBlo role,IGuestBlo guest)
        {
            _guestBlo = guest;
            _roleBlo = role;
            _userBlo = user;
            _logger = logger;
        }

        //********************************************//
        //**start** admin/users - role
        [Authorize(Roles = "Admin")]
        [HttpGet("getallrole")]
        public async Task<BaseResponse> GetAllRole()
        {
            try
            {
                return new BaseResponse()
                {
                    data = await _roleBlo.GetAllRole(),
                };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("addnewrole")]
        public async Task<BaseResponse> AddNewRole(RoleRequest role)
        {
            try
            {
                int code = 0;
                string message = string.Empty;
                //check empty
                if (string.IsNullOrEmpty(role.roleName) || role.roleName.Trim() == "")
                {
                    code = 2;
                    message = BlogConst.msgCreateRoleNullName;
                }
                else
                {
                    //check role name exists
                    if (_roleBlo.CheckRoleExists(role.roleName))
                    {
                        code = 3;
                        message = BlogConst.msgCreateRoleExists;
                    }
                    else
                    {
                        //add role
                        await _roleBlo.CreateRole(new RoleModel() { roleName = role.roleName.Trim() });
                        message = BlogConst.msgCreateRoleSuccess;
                    }
                }
                return new BaseResponse()
                {
                    errorCode = code,
                    message = message,
                };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("editrole")]
        public async Task<BaseResponse> EditRole(RoleRequest role)
        {
            try
            {
                int code = 0;
                string message = string.Empty;
                //check empty
                if (string.IsNullOrEmpty(role.roleName) || role.roleName.Trim() == "")
                {
                    code = 2;
                    message = BlogConst.msgCreateRoleNullName;
                }
                else
                {
                    //find role
                    RoleModel model = _roleBlo.FindRoleById(int.Parse(role.roleId));
                    //check null
                    if (model == null)
                    {
                        code = 3;
                        message = BlogConst.msgEditRoleNotFound;
                    }
                    //cannot rename role admin
                    else if (model.roleName == "Admin")
                    {
                        code = 4;
                        message = BlogConst.msgEditRoleAdmin;
                    }
                    else
                    {
                        //update role
                        model.roleName = role.roleName.Trim();
                        await _roleBlo.UpdataRole(model);
                        message = BlogConst.msgEditRoleSuccess;
                    }
                }
                return new BaseResponse()
                {
                    errorCode = code,
                    message = message,
                };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("removerole/{roleId}")]
        public async Task<BaseResponse> RemoveRole(string roleId)
        {
            try
            {
                int code = 0;
                string message = string.Empty;
                //check empty
                if (string.IsNullOrEmpty(roleId))
                {
                    code = 2;
                    message = BlogConst.msgDeleteRoleNotFound;
                }
                else
                {
                    //find role
                    RoleModel model = _roleBlo.FindRoleById(int.Parse(roleId));
                    //check null
                    if (model == null)
                    {
                        code = 3;
                        message = BlogConst.msgDeleteRoleNotFound;
                    }
                    //cannot remove role admin
                    else if (model.roleName == "Admin")
                    {
                        code = 4;
                        message = BlogConst.msgDeleteRoleAdmin;
                    }
                    else
                    {
                        //remove role
                        await _roleBlo.RemoveRole(model);
                        message = BlogConst.msgDeleteRoleSuccess;
                    }
                }
                return new BaseResponse()
                {
                    errorCode = code,
                    message = message,
                };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        //**end** admin/users - role
        //********************************************//

        //********************************************//
        //**start** admin/users - user
        [Authorize(Roles = "Admin")]
        [HttpGet("getalluser")]
        public async Task<BaseResponse> getAllUser()
        {
            try
            {
                string pathImg = Helper.getUrl(Request);
                return new BaseResponse()
                {
                    data = await _userBlo.GetAllUser()
                };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        private string checkUserInfo(UserRequest user)
        {
            string message = string.Empty;
            if(!Helper.IsValidEmail(user.userEmail))
            {
                message += BlogConst.msgCheckUserEmail;
            }
            if(string.IsNullOrEmpty(user.userAddress))
            {
                message += BlogConst.msgCheckUserAdress;
            }
            if (string.IsNullOrEmpty(user.userDescription))
            {
                message += BlogConst.msgCheckUserDescription;
            }
            if (string.IsNullOrEmpty(user.userFullName))
            {
                message += BlogConst.msgCheckUserName;
            }
            if (string.IsNullOrEmpty(user.userPhone))
            {
                message += BlogConst.msgCheckUserName;
            }
            return message;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("addnewuser")]
        public async Task<BaseResponse> AddNewUser([FromForm]UserRequest user)
        {
            try
            {
                //check user info
                string checkInfo = checkUserInfo(user);
                if (!string.IsNullOrEmpty(checkInfo))
                {
                    return new BaseResponse() { errorCode = 2, message = checkInfo };
                }
                else
                {
                    //check user exists
                    UserModel userModel = _userBlo.FindUserByEmail(user.userEmail.Trim(), false);
                    if (userModel != null)
                    {
                        return new BaseResponse() { errorCode = 5, message = BlogConst.msgCheckUserEmailIsUsed };
                    }
                    //check role empty
                    RoleModel role = _roleBlo.FindRoleById(int.Parse(user.userRoleId));
                    if (role == null)
                    {
                        return new BaseResponse() { errorCode = 4, message = BlogConst.msgCheckUserRole };
                    }
                    //check avatar empty
                    if (user.userPathAvatar == null)
                    {
                        return new BaseResponse() { errorCode = 3, message = BlogConst.msgCheckUserAvatar };
                    }
                    //add user
                    UserModel model = new UserModel();
                    model.password = new PasswordHasher<UserModel>().HashPassword(model, user.userPassword);
                    model.address = user.userAddress;
                    model.description = user.userDescription;
                    model.email = user.userEmail.Trim();
                    model.fullName = user.userFullName;
                    model.phoneNumber = user.userPhone;
                    model.roleId = role.Id;
                    model.status = 0;
                    string pathImage = "wwwroot\\images";
                    var uploadFilesPath = Path.Combine(pathImage, "users");
                    //check and create folder
                    if (!Directory.Exists(uploadFilesPath))
                    {
                        Directory.CreateDirectory(uploadFilesPath);
                    }
                    //file name
                    string newFileName = DateTime.Now.ToString("ddMMyyyyhhssmm") + "_" + user.userPathAvatar.FileName;
                    //get new path
                    string path = pathImage + "\\users\\" + newFileName;
                    //save new file
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        user.userPathAvatar.CopyTo(stream);
                        model.pathAvatar = "/users/" + newFileName;
                    }
                    //save database
                    await _userBlo.CreateUser(model);
                    return new BaseResponse() { message = BlogConst.msgCheckUserSuccess };
                }
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        private bool CheckLastAdmin(UserRequest user)
        {
            try
            {
                //find role of user
                RoleModel role = _roleBlo.FindRoleById(int.Parse(user.userRoleId));
                //count user role admin
                int countAdmin = _userBlo.CountUserRoleAdmin();
                //canot update or delete if this user is last admin
                if (role.roleName != "Admin" && countAdmin <= 1)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        private bool CheckLastAdminStatus(UserRequest user)
        {
            try
            {
                //find role of user
                RoleModel role = _roleBlo.FindRoleById(int.Parse(user.userRoleId));
                //count user role admin
                int countAdmin = _userBlo.CountUserRoleAdmin();
                //canot update status if this user is last admin
                if (role.roleName == "Admin" && countAdmin <= 1 && int.Parse(user.status) != 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("updateuser")]
        public async Task<BaseResponse> UpdateUser([FromForm]UserRequest user)
        {
            try
            {
                //check user info
                string checkInfo = checkUserInfo(user);
                if (!string.IsNullOrEmpty(checkInfo))
                {
                    return new BaseResponse() { errorCode = 2, message = checkInfo };
                }
                else
                {
                    //check last user admin
                    if(CheckLastAdmin(user))
                    {
                        return new BaseResponse() { errorCode = 6, message = BlogConst.msgCheckUserLastAdmin };
                    }
                    //check user exists
                    UserModel model = _userBlo.FindUserById(int.Parse(user.userId));
                    if (model == null)
                    {
                        return new BaseResponse() { errorCode = 5, message = BlogConst.msgCheckUserNotfound };
                    }
                    //check role empty
                    RoleModel role = _roleBlo.FindRoleById(int.Parse(user.userRoleId));
                    if (role == null)
                    {
                        return new BaseResponse() { errorCode = 4, message = BlogConst.msgCheckUserRole };
                    }
                    //update user
                    model.address = user.userAddress;
                    model.description = user.userDescription;
                    model.fullName = user.userFullName;
                    model.phoneNumber = user.userPhone;
                    model.roleId = role.Id;
                    //check image avatar
                    if (user.userPathAvatar != null)
                    {
                        string pathImage = "wwwroot\\images";
                        var uploadFilesPath = Path.Combine(pathImage, "users");
                        //check and create folder
                        if (!Directory.Exists(uploadFilesPath))
                        {
                            Directory.CreateDirectory(uploadFilesPath);
                        }
                        //get old path
                        string oldImageName = pathImage + Path.Combine(model.pathAvatar);
                        //remove old file
                        if (model.pathAvatar != "")
                        {
                            if (System.IO.File.Exists(oldImageName))
                            {
                                System.IO.File.Delete(oldImageName);
                            }
                        }
                        //file name
                        string newFileName = DateTime.Now.ToString("ddMMyyyyhhssmm") + "_" + user.userPathAvatar.FileName;
                        //get new path
                        string path = pathImage + "\\users\\" + newFileName;
                        //save new file
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            user.userPathAvatar.CopyTo(stream);
                            model.pathAvatar = "/users/" + newFileName;
                        }
                    }
                    //save database
                    await _userBlo.UpdateUserSync(model, false);
                    return new BaseResponse() { message = BlogConst.msgCheckUserUpdateSuccess };
                }
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("updatestatususer")]
        public async Task<BaseResponse> UpdateStatusUser([FromForm]UserRequest user)
        {
            try
            {
                //check last user admin
                if (CheckLastAdminStatus(user))
                {
                    return new BaseResponse() { errorCode = 6, message = BlogConst.msgCheckUserLastAdmin };
                }
                //check user exists
                UserModel model = _userBlo.FindUserById(int.Parse(user.userId));
                if (model == null)
                {
                    return new BaseResponse() { errorCode = 5, message = BlogConst.msgCheckUserNotfound };
                }
                //save database
                model.status = int.Parse(user.status);
                await _userBlo.UpdateUserSync(model, false);
                return new BaseResponse() { message = BlogConst.msgCheckUserUpdateSuccess };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        //**end** admin/users - user
        //********************************************//

        //********************************************//
        //**start** admin/users - guest

        [HttpGet("getallguest")]
        public async Task<BaseResponse> getAllGuest()
        {
            try
            {
                return new BaseResponse() { data = await _guestBlo.GetAllGuest() };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        //**end** admin/users - guest
        //********************************************//

    }
}