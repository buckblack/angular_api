﻿using System;
using System.Collections.Generic;
using API_BlogCore.Ultils;
using API_BlogServices.Blo;
using API_BlogServices.Models;
using API_BlogServices.Responses;
using Microsoft.AspNetCore.Mvc;

namespace API_Blog.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryBlo _categoryBlo;
        private readonly ISubCategoryBlo _subCategoryBlo;
        private IMyLogger _logger;
        public CategoryController(ICategoryBlo category, ISubCategoryBlo subcategory, IMyLogger logger)
        {
            _categoryBlo = category;
            _subCategoryBlo = subcategory;
            _logger = logger;
        }

        [HttpGet("getall")]
        public BaseResponse getAll()
        {
            try
            {
                List<CategoryModel> result = _categoryBlo.getAllCategory();
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpGet("getallsubcategory")]
        public BaseResponse getAllSubCategory(string cateId)
        {
            try
            {
                //check id category
                int categoryId = 1;
                int.TryParse(cateId, out categoryId);
                //get data
                List<SubCategoryModel> result = _subCategoryBlo.GetSubCategoryByCateId(categoryId);
                return new BaseResponse() { data = result };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpGet("getpostsbycateid")]
        public BaseResponse getPostsByCateId(string cateId, string page)
        {
            try
            {
                //check id category
                int categoryId = 1;
                int.TryParse(cateId, out categoryId);
                //set paging
                int currentPage = 1;
                int sumDisplay = 12;
                long totalRecodrs = _subCategoryBlo.CountTotalPostBySubCateId(categoryId);
                int.TryParse(page, out currentPage);
                if (currentPage < 1)
                {
                    currentPage = 1;
                }
                //get data ang paging
                List<PostModel> result = new List<PostModel>();
                result = _subCategoryBlo.GetPostBySubCateIdPaging(categoryId, sumDisplay, currentPage);
                return new PostPagingResponse()
                {
                    data = result,
                    currentPage = currentPage,
                    totalRecords = totalRecodrs,
                    totalPage = (int)Math.Ceiling((double)totalRecodrs / sumDisplay),
                };

            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }
    }
}