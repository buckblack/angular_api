﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_BlogCore.Ultils;
using API_BlogDao.Dao;
using API_BlogServices.Blo;
using API_BlogServices.Models;
using API_BlogServices.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API_Blog.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubCategoryController : ControllerBase
    {
        private readonly ISubCategoryBlo _subCategory;
        private readonly IPostBlo _postBlo;
        private IMyLogger _logger;
        public SubCategoryController(ISubCategoryBlo subCategory, IMyLogger logger,IPostBlo post)
        {
            _postBlo = post;
            _subCategory = subCategory;
            _logger = logger;
        }

        [HttpGet("getallsubcategory")]
        public BaseResponse getallSubCategory()
        {
            try
            {
                List<SubCategoryModel> result = _subCategory.GetAllSubCategory();
                return new BaseResponse() {
                    data = result,
                };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }

        [HttpGet("getallpostsbysubcateid")]
        public BaseResponse getAllPostsBySubCateid(string subCateId,string page)
        {
            try
            {
                //check subcategor id
                int subCategoryId = 1;
                int.TryParse(subCateId, out subCategoryId);
                //set paging
                int currentPage = 1;
                int sumDisplay = 18;
                long totalRecodrs = _postBlo.CoutPostBySubCateID(subCategoryId);
                int.TryParse(page, out currentPage);
                if (currentPage < 1)
                {
                    currentPage = 1;
                }
                //get data subcategory
                SubCategoryModel subCategoryModel = _subCategory.FindSubCategoryById(subCategoryId);
                if(subCategoryModel == null)
                {
                    return new PostPagingResponse()
                    {
                        errorCode = 2,
                        message = BlogConst.msgDeleteSubCategoryNotFound,
                    };
                }
                //return data and paging
                List<PostModel> result = _postBlo.GetPostBySubCategoryIdPaging(subCategoryId, currentPage, sumDisplay);
                return new PostPagingResponse()
                {
                    data = result,
                    currentPage = currentPage,
                    totalRecords = totalRecodrs,
                    totalPage = (int)Math.Ceiling((double)totalRecodrs / sumDisplay),
                    subCateName = subCategoryModel.subCateName,
                    cateName = subCategoryModel.category.cateName,
                };
            }
            catch (Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = ex.ToString() };
            }
        }
    }
}