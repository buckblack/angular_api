﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using API_BlogCore.Common;
using API_BlogCore.Requests;
using API_BlogCore.Ultils;
using API_BlogServices.Blo;
using API_BlogServices.Models;
using API_BlogServices.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace API_Blog.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminAuthController : ControllerBase
    {
        private readonly IUserBlo _userBlo;
        private readonly ICommon _common;
        private IMyLogger _logger;

        public AdminAuthController(IUserBlo user, IMyLogger logger, ICommon common)
        {
            _userBlo = user;
            _logger = logger;
            _common = common;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public BaseResponse login([FromForm]LoginRequest request)
        {
            try
            {
                //check empty
                if (string.IsNullOrEmpty(request.email) || string.IsNullOrEmpty(request.password.Trim()))
                {
                    return new BaseResponse() { errorCode = 2, message = BlogConst.msgLoginEmpty };
                }
                //check email true or false
                if (!Helper.IsValidEmail(request.email))
                {
                    return new BaseResponse() { errorCode = 2, message = BlogConst.msgCheckUserEmail };
                }
                UserModel user = new UserModel();
                PasswordHasher<UserModel> hash = new PasswordHasher<UserModel>();
                //get and check user
                user = _userBlo.FindUserByEmail(request.email, true);
                if (user == null)
                {
                    return new BaseResponse() { errorCode = 3, message = BlogConst.msgLoginIncorrect };
                }
                //compare password
                if (hash.VerifyHashedPassword(user, user.password, request.password) != PasswordVerificationResult.Success)
                {
                    return new BaseResponse() { errorCode = 3, message = BlogConst.msgLoginIncorrect };
                }
                //gen token
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(Helper.key);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.Role, user.role.roleName),
                    new Claim("UserId", user.Id.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddMinutes(30),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                var tokenString = tokenHandler.WriteToken(token);
                return new LoginResponse() { token = tokenString, fullName = user.fullName, pathAvatar = user.pathAvatar, role = user.role.roleName };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        private string checkPassword(LoginRequest request)
        {
            string message = string.Empty;
            if(string.IsNullOrEmpty(request.password) || string.IsNullOrEmpty(request.password.Trim()))
            {
                return BlogConst.msgChangePWEmpty;
            }
            if (string.IsNullOrEmpty(request.newPassword) || string.IsNullOrEmpty(request.newPassword.Trim()))
            {
                return BlogConst.msgChangePWEmpty;
            }
            if (string.IsNullOrEmpty(request.confirmPassword) || string.IsNullOrEmpty(request.confirmPassword.Trim()))
            {
                return BlogConst.msgChangePWEmpty;
            }
            if(request.confirmPassword != request.newPassword)
            {
                return BlogConst.msgChangePWNotMatch;
            }
            return message;
        }

        [HttpPost("changepassword")]
        public BaseResponse ChangePassword([FromForm]LoginRequest request)
        {
            try
            {
                //check password
                string message = checkPassword(request);
                if (!string.IsNullOrEmpty(message))
                {
                    return new BaseResponse() { errorCode = 2, message = message };
                }
                UserModel user = new UserModel();
                PasswordHasher<UserModel> hash = new PasswordHasher<UserModel>();
                //get user
                int userId= int.Parse(HttpContext.User.Claims.Where(x => x.Type == "UserId").FirstOrDefault().Value);
                user = _userBlo.FindUserById(userId);
                if (user == null)
                {
                    return new BaseResponse() { errorCode = 3, message = BlogConst.msgChangePWNotFound };
                }
                //compare password
                if (hash.VerifyHashedPassword(user, user.password, request.password) != PasswordVerificationResult.Success)
                {
                    return new BaseResponse() { errorCode = 3, message = BlogConst.msgChangePWIncorrect };
                }
                //update password
                user.password = new PasswordHasher<UserModel>().HashPassword(user, request.newPassword);
                _userBlo.UpdateUser(user,true);
                return new LoginResponse() { message=BlogConst.msgChangePWSuccess };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

        private string RandomPW()
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            char[] stringChars = new char[10];
            Random random = new Random();
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < 10; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
                result.Append(stringChars[i].ToString());
            }

            return result.ToString();
        }

        [AllowAnonymous]
        [HttpPost("resetpassword")]
        public BaseResponse ResetPassword([FromForm]LoginRequest request)
        {
            try
            {
                //check password
                if (!Helper.IsValidEmail(request.email))
                {
                    return new BaseResponse() { errorCode = 2, message = BlogConst.msgCheckUserEmail };
                }
                UserModel user = new UserModel();
                PasswordHasher<UserModel> hash = new PasswordHasher<UserModel>();
                //get user
                user = _userBlo.FindUserByEmail(request.email, false);
                if (user == null)
                {
                    return new BaseResponse() { errorCode = 3, message = BlogConst.msgChangePWNotFound };
                }
                //update password
                string ramdomPW = RandomPW();
                user.password = new PasswordHasher<UserModel>().HashPassword(user, ramdomPW);
                _userBlo.UpdateUser(user,true);
                //send mail
                string address = "long4581994@gmail.com";
                string password = "khoa1234";
                SmtpClient client = new SmtpClient("smtp.gmail.com", 25);
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(address, password);
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(address);
                mailMessage.To.Add(request.email);
                mailMessage.Body = "Your new password: " + ramdomPW;
                mailMessage.Subject = "Your password has been changed";
                client.Send(mailMessage);
                return new LoginResponse() { message = BlogConst.msgChangePWSuccess };
            }
            catch(Exception ex)
            {
                _logger.WriteLogError(ex.ToString());
                return new BaseResponse() { errorCode = 1, message = BlogConst.msgTryAgain };
            }
        }

    }
}