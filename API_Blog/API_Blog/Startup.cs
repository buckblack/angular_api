using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API_BlogCore.Common;
using API_BlogCore.Ultils;
using API_BlogDao.Dao;
using API_BlogDao.Models;
using API_BlogServices.Blo;
using API_BlogServices.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace API_Blog
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<BlogContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddSingleton<IMyLogger, MyLogger>();
            services.AddSingleton<ICommon, Common>();
            services.AddScoped<ICategoryBlo, CategoryBlo>();
            services.AddScoped<ICategoryDao, CategoryDao>();
            services.AddScoped<ISubCategoryBlo, SubcategoryBlo>();
            services.AddScoped<ISubcategoryDao, SubCategoryDao>();
            services.AddScoped<IGuestBlo, GuestBlo>();
            services.AddScoped<IGuestDao, GuestDao>();
            services.AddScoped<IPostBlo, PostBlo>();
            services.AddScoped<IPostDao, PostDao>();
            services.AddScoped<IUserBlo, UserBlo>();
            services.AddScoped<IUserDao, UserDao>();
            services.AddScoped<IRoleBlo, RoleBlo>();
            services.AddScoped<IRoleDao, RoleDao>();
            services.AddScoped<IStatisticalBlo, StatisticalBlo>();
            services.AddScoped<IStatisticalDao, StatisticalDao>();
            services.AddScoped<INotifyBlo, NotifyBlo>();
            services.AddScoped<INotifyDao, NotifyDao>();
            services.AddScoped<ICommentBlo, CommentBlo>();
            services.AddScoped<ICommentDao, CommentDao>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IAppRepository, AppRepository>();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(x =>
                {
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Helper.key)),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });
            services.AddControllers().AddNewtonsoftJson(opt =>
            {
                opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });
            services.AddCors(opt =>
            {
                opt.AddPolicy("AllowSpecificOrigin", builder => builder.WithOrigins("*").AllowAnyMethod().AllowAnyOrigin().AllowAnyHeader());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            loggerFactory.AddFile("Logs/Info/LogInfo-{Date}.txt", minimumLevel: LogLevel.Information);
            loggerFactory.AddFile("Logs/Error/LogError-{Date}.txt", minimumLevel: LogLevel.Error);
            app.UseStaticFiles();
            app.UseRouting();
            app.UseCors("AllowSpecificOrigin");
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
