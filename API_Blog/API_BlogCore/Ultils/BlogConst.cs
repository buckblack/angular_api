﻿namespace API_BlogCore.Ultils
{
    public class BlogConst
    {
        public const string msgSubmitSuccess = "Submit successful";

        public const string msgTryAgain = "Please try again";
        public const string msgCreateCategoryNullName = "Category name cannot empty";
        public const string msgCreateCategoryNameExists = "Category name has been used";
        public const string msgCreateCategorySuccess = "Successfull";

        public const string msgCreateSubCategoryNullName = "Sub-Category name cannot empty";
        public const string msgCreateSubCategoryNullImg = "Sub-Category image is empty";
        public const string msgCreateSubCategoryNameExists = "Sub-Category name has been used";
        public const string msgCreateSubCategorySuccess = "Successfull";

        public const string msgUpdateCategoryNullId = "Category not found";
        public const string msgUpdateCategoryNullName = "Category name cannot empty";
        public const string msgUpdateCategoryNameExists = "Category name has been used";
        public const string msgUpdateCategorySuccess = "Successfull";

        public const string msgUpdateSubCategoryNullId = "Sub-Category not found";
        public const string msgUpdateSubCategoryNullName = "Sub-Category name cannot empty";
        public const string msgUpdateSubCategoryNameExists = "Sub-Category name has been used";
        public const string msgUpdateSubCategorySuccess = "Successfull";

        public const string msgDeleteCategorySuccess = "Successfull";
        public const string msgDeleteCategoryNotFound = "Category not found";
        public const string msgDeleteCategoryIsUsed = "Category has been used, cannot remove";

        public const string msgDeleteSubCategorySuccess = "Successfull";
        public const string msgDeleteSubCategoryNotFound = "Sub-Category not found";
        public const string msgDeleteSubCategoryIsUsed = "Sub-Category has been used, cannot remove";

        public const string msgDeleteCommentSuccess = "Remove comment successful";
        public const string msgDeleteCommentNotFound = "Comment not found";

        public const string msgCreateRoleNullName = "Role name cannot empty";
        public const string msgCreateRoleExists = "Role name has been used";
        public const string msgCreateRoleSuccess = "Create role success";

        public const string msgEditRoleNotFound = "Role not found";
        public const string msgEditRoleNullName = "Role name cannot empty";
        public const string msgEditRoleAdmin = "Role 'Admin' cannot edit";
        public const string msgEditRoleSuccess = "Edit role success";

        public const string msgDeleteRoleNotFound = "Role not found";
        public const string msgDeleteRoleAdmin = "Role 'Admin' cannot remove";
        public const string msgDeleteRoleSuccess = "Remove role success";

        public const string msgCheckUserLastAdmin = "System need 1 admin";
        public const string msgCheckUserEmail = "Email not true";
        public const string msgCheckUserNotfound = "User Not Found";
        public const string msgCheckUserRoleError = "User Not Permission";
        public const string msgCheckUserAdress = "Address cannot empty";
        public const string msgCheckUserDescription = "Description cannot empty";
        public const string msgCheckUserName = "Name cannot empty</br>";
        public const string msgCheckUserPhone = "Phone cannot empty</br>";
        public const string msgCheckUserRole = "Please select role";
        public const string msgCheckUserAvatar = "Please select image";
        public const string msgCheckUserSuccess = "Create user success";
        public const string msgCheckUserUpdateSuccess = "Update user success";
        public const string msgCheckUserEmailIsUsed = "This email has been used";

        public const string msgLoginIncorrect = "Email or password incorrect";
        public const string msgLoginEmpty = "Email and password cannot empty";

        public const string msgPostDetailEmpty = "Detail cannot empty";
        public const string msgPostTitleEmpty = "Title cannot empty";
        public const string msgPostSummaryEmpty = "Summary cannot empty";
        public const string msgPostImgEmpty = "Thumbnail cannot empty";
        public const string msgPostSubCateNotFound = "Sub-Category not found";
        public const string msgNewPostSuccess = "Create new post successful";

        public const string msgUpdatePostSuccess = "Update post successful";
        public const string msgUpdateStatusPostSuccess = "Update status successful";
        public const string msgUpdatePostNotFound = "Post not found";
        public const string msgRemoveTrashImageNull = "This post don't have trash image in content";
        public const string msgRemoveTrashImageSuccess = "Remove trash success";

        public const string msgNotifyDetailEmpty = "Detail cannot empty";
        public const string msgNotifyTitleEmpty = "Title cannot empty";
        public const string msgNotifySuccess = "Submit notify successful";
        public const string msgNotifyDetailNotFound = "Notify not found";
        public const string msgNotifyDeleteSuccess = "Delete notify successful";

        public const string msgChangePWEmpty = "Password cannot empty";
        public const string msgChangePWNotMatch = "Password not match";
        public const string msgChangePWNotFound = "User not found";
        public const string msgChangePWIncorrect = "Current password incorrect";
        public const string msgChangePWSuccess = "Password has been change";

        public const string contentAddMoreImg = "<img src='{0}{1}' />";
        public const string contentAddMoreImgEmpty = "This image is empty";
        public const string contentAddMoreImgSuccess = "Upload Success";

        public const string msgGuestNameEmpty = "Name cannot empty";
        public const string msgGuestContent = "Content must more 50 character";

    }
}
