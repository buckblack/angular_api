﻿using Microsoft.Extensions.Logging;

namespace API_BlogCore.Ultils
{
    public interface IMyLogger
    {
        void WriteLogInfo(string message);
        void WriteLogError(string message);
    }
    public class MyLogger:IMyLogger
    {
        public ILogger _logger;
        public MyLogger(ILoggerFactory logFactory)
        {
            _logger = logFactory.CreateLogger<MyLogger>();
        }
        public void WriteLogInfo(string message)
        {
            _logger.LogInformation(message);
        }

        public void WriteLogError(string message)
        {
            _logger.LogError(message);
        }
    }
}
