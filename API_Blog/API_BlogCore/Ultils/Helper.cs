﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace API_BlogCore.Ultils
{
    public class Helper
    {
        public static string getUrl(HttpRequest req)
        {
            try
            {
                string result = string.Empty;
                result = "http" + (req.IsHttps ? "s" : "") + "://" + req.Host.ToString() + "/images";
                return result;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static bool IsValidEmail(string email)
        {
            return new EmailAddressAttribute().IsValid(email);
        }

        public const string key = "iusdgfiaDSFHGsas23f1g564564asasdg65aKHJGSDYFG";
    }
}
