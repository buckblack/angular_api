﻿using Microsoft.AspNetCore.Http;

namespace API_BlogCore.Requests
{
    public class SubCategoryRequest
    {
        public string subCateId { get; set; }
        public string cateId { get; set; }
        public string subCateName { get; set; }
        public string cateName { get; set; }
        public IFormFile thumbnailSubCate { get; set; }
    }
}
