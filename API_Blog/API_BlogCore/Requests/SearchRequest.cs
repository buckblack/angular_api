﻿
namespace API_BlogCore.Requests
{
    public class SearchRequest
    {
        public string key { get; set; }
        public string author { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public string sort { get; set; }
        public string page { get; set; }
    }
}
