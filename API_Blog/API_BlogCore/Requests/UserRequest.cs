﻿using Microsoft.AspNetCore.Http;

namespace API_BlogCore.Requests
{
    public class UserRequest
    {
        public string userId { get; set; }
        public string userRoleId { get; set; }
        public string userFullName { get; set; }
        public string userPhone { get; set; }
        public string userEmail { get; set; }
        public string userPassword { get; set; }
        public string userAddress { get; set; }
        public string userDescription { get; set; }
        public string status { get; set; }
        public IFormFile userPathAvatar { get; set; }
    }
}
