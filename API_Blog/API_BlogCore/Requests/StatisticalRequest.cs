﻿using System;

namespace API_BlogCore.Requests
{
    public class StatisticalRequest
    {
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
    }
}
