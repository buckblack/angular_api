﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace API_BlogCore.Requests
{
    public class PostRequest
    {
        public string postId { get; set; }
        public string postTitle { get; set; }
        public string summaryContent { get; set; }
        public string detailContent { get; set; }
        public IFormFile pathThumbnail { get; set; }
        public IFormFile pathThumbnailMore { get; set; }
        public List<IFormFile> lstImgContent { get; set; }
        public string subCateId { get; set; }
        public string status { get; set; }
    }
}
