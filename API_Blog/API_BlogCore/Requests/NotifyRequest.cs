﻿
namespace API_BlogCore.Requests
{
    public class NotifyRequest
    {
        public string content { get; set; }
        public string title { get; set; }
    }
}
