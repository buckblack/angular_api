﻿
namespace API_BlogCore.Requests
{
    public class RoleRequest
    {
        public string roleId { get; set; }
        public string roleName { get; set; }
    }
}
