﻿
namespace API_BlogCore.Requests
{
    public class LoginRequest
    {
        public string email { get; set; }
        public string password { get; set; }
        public string newPassword { get; set; }
        public string confirmPassword { get; set; }
    }
}
