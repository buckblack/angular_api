﻿
namespace API_BlogCore.Requests
{
    public class CommentRequest
    {
        public string guestName { get; set; }
        public string guestEmail { get; set; }
        public string contentComment { get; set; }
        public long postId { get; set; }
    }
}
