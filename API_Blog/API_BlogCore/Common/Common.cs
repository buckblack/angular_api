﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace API_BlogCore.Common
{
    public class Common:ICommon
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public Common(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetPathImg()
        {
            return "http" + (_httpContextAccessor.HttpContext.Request.IsHttps ? "s" : "") + "://" + _httpContextAccessor.HttpContext.Request.Host.ToString() + "/images";
        }
    }
}
