﻿
namespace API_BlogServices.Responses
{
    public class BaseResponse
    {
        public int errorCode { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }
}
