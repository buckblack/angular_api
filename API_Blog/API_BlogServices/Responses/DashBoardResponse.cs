﻿using API_BlogServices.Models;
using API_BlogServices.Models.View;
using System.Collections.Generic;

namespace API_BlogServices.Responses
{
    public class DashBoardResponse:BaseResponse
    {
        public string countPost { get; set; }
        public string countCategory { get; set; }
        public string countGuest { get; set; }
        public string countMember { get; set; }
        public List<PostModel> lstPostNew { get; set; }
        public List<CommentModel> lstCommentNew { get; set; }
        public List<PostsHotView> lstPostHot { get; set; }
    }
}
