﻿
using API_BlogServices.Models;

namespace API_BlogServices.Responses
{
    public class PostDetailResponse:BaseResponse
    {
        public string userPathImg { get; set; }
        public CategoryModel category { get; set; }
    }
}
