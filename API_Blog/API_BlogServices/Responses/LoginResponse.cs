﻿
namespace API_BlogServices.Responses
{
    public class LoginResponse:BaseResponse
    {
        public string token { get; set; }
        public string fullName { get; set; }
        public string pathAvatar { get; set; }
        public string role { get; set; }
    }
}
