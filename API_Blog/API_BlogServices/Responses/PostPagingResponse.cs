﻿
namespace API_BlogServices.Responses
{
    public class PostPagingResponse: BaseResponse
    {
        public int totalPage { get; set; }
        public int currentPage { get; set; }
        public long totalRecords { get; set; }
        public string subCateName { get; set; }
        public string cateName { get; set; }
    }
}
