﻿using API_BlogDao.Dao;
using API_BlogDao.Models;
using API_BlogServices.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using API_BlogCore.Common;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public class CategoryBlo: ICategoryBlo
    {
        private readonly IAppRepository IAppRepository;
        private readonly ICommon _common;
        public CategoryBlo(IAppRepository appRepository, ICommon common)
        {
            this.IAppRepository = appRepository;
            _common = common;
        }

        public List<CategoryModel> getAllCategory()
        {
            List<CategoryEntity> model = IAppRepository.CategoryDao.getAllCategory();
            List<CategoryModel> result = new List<CategoryModel>();
            result = model.Select(x => new CategoryModel
            {
                cateName = x.cateName,
                Id = x.Id,
            }).ToList();
            return result;
        }

        public async Task<List<CategoryModel>> GetAllCategorySync()
        {
            List<CategoryEntity> model = await IAppRepository.CategoryDao.getAllCategorySync();
            List<CategoryModel> result = new List<CategoryModel>();
            string pathImg = _common.GetPathImg();
            result = model.Select(x => new CategoryModel
            {
                cateName = x.cateName,
                Id = x.Id,
                subCategories = x.subCategories.Select(s => new SubCategoryModel
                {
                    categoryId = s.categoryId,
                    Id = s.Id,
                    pathThumbnail = pathImg + s.pathThumbnail,
                    subCateName = s.subCateName,
                }).ToList(),
            }).ToList();
            return result;
        }

        public CategoryModel FindCategoryById(int cateId)
        {
            CategoryEntity entity = IAppRepository.CategoryDao.FindCategoryById(cateId);
            CategoryModel model = new CategoryModel();
            if (entity != null)
            {
                model.cateName = entity.cateName;
                model.Id = entity.Id;
                string pathImg = _common.GetPathImg();
                model.subCategories = entity.subCategories == null ? null : entity.subCategories.Select(s => new SubCategoryModel
                {
                    categoryId = s.categoryId,
                    Id = s.Id,
                    pathThumbnail = pathImg + s.pathThumbnail,
                    subCateName = s.subCateName
                }).ToList();
            }
            return model;
        }
        
        public async Task<int> UpdateCategory(CategoryModel model)
        {
            CategoryEntity entity = new CategoryEntity();
            entity.cateName = model.cateName;
            entity.Id = model.Id;
            return await IAppRepository.CategoryDao.UpdateCategory(entity);
        }
        public async Task<int> CreateCategory(CategoryModel model)
        {
            CategoryEntity entity = new CategoryEntity();
            entity.cateName = model.cateName;
            return await IAppRepository.CategoryDao.CreateCategory(entity);
        }
        public async Task<int> DeleteCategory(CategoryModel model)
        {
            CategoryEntity entity = new CategoryEntity();
            entity.cateName = model.cateName;
            entity.Id = model.Id;
            return await IAppRepository.CategoryDao.DeleteCategory(entity);
        }
        
        public bool CheckExistsCategoryByCateName(string cateName)
        {
            return IAppRepository.CategoryDao.CheckExistsCategoryByCateName(cateName);
        }
    }
}
