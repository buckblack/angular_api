﻿using API_BlogCore.Common;
using API_BlogDao.Dao;
using API_BlogDao.Models;
using API_BlogServices.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public class RoleBlo:IRoleBlo
    {
        private readonly IAppRepository IAppRepository;
        private readonly ICommon _common;
        public RoleBlo(IAppRepository appRepository, ICommon common)
        {
            this.IAppRepository = appRepository;
            _common = common;
        }

        public bool CheckRoleExists(string roleName)
        {
            return IAppRepository.RoleDao.CheckRoleExists(roleName);
        }

        public async Task<List<RoleModel>> GetAllRole()
        {
            List<RoleModel> models = new List<RoleModel>();
            List<RoleEntity> entities = await IAppRepository.RoleDao.GetAllRole();
            if(entities != null && entities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                models = entities.Select(x => new RoleModel
                {
                    Id = x.Id,
                    roleName = x.roleName,
                    users = x.users == null ? null : new Collection<UserModel>(x.users.Select(u => new UserModel
                    {
                        status = u.status,
                        address = u.address,
                        description = u.description,
                        email = u.email,
                        fullName = u.fullName,
                        Id = u.Id,
                        pathAvatar = pathImg + u.pathAvatar,
                        phoneNumber = u.phoneNumber,
                        roleId = u.roleId,
                    }).ToList()),
                }).ToList();
            }
            return models;
        }

        public RoleModel FindRoleById(int roleId)
        {
            RoleEntity entity = IAppRepository.RoleDao.FindRoleById(roleId);
            RoleModel model = new RoleModel();
            if(entity != null)
            {
                model.Id = entity.Id;
                model.roleName = entity.roleName;
            }
            return model;
        }
        public async Task<int> CreateRole(RoleModel model)
        {
            RoleEntity entity = new RoleEntity();
            entity.roleName = model.roleName;
            return await IAppRepository.RoleDao.CreateRole(entity);
        }
        public async Task<int> UpdataRole(RoleModel model)
        {
            RoleEntity entity = new RoleEntity();
            entity.Id = model.Id;
            entity.roleName = model.roleName;
            return await IAppRepository.RoleDao.CreateRole(entity);
        }

        public async Task<int> RemoveRole(RoleModel model)
        {
            RoleEntity entity = new RoleEntity();
            entity.Id = model.Id;
            entity.roleName = model.roleName;
            return await IAppRepository.RoleDao.RemoveRole(entity);
        }
    }
}
