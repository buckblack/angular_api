﻿using API_BlogServices.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public interface INotifyBlo
    {
        Task<int> CreateNotify(NotifyModel model);
        Task<NotifyModel> FindNotifyByID(long id);
        Task<int> DeleteNotify(NotifyModel model);
        Task<List<NotifyModel>> GetNotify(int records = 0);
    }
}
