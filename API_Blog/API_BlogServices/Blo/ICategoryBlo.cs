﻿using API_BlogServices.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public interface ICategoryBlo
    {
        List<CategoryModel> getAllCategory();
        CategoryModel FindCategoryById(int cateId);
        Task<List<CategoryModel>> GetAllCategorySync();
        Task<int> UpdateCategory(CategoryModel model);
        Task<int> CreateCategory(CategoryModel model);
        Task<int> DeleteCategory(CategoryModel model);
        bool CheckExistsCategoryByCateName(string cateName);
    }
}
