﻿using API_BlogCore.Common;
using API_BlogCore.Requests;
using API_BlogDao.Dao;
using API_BlogDao.Models;
using API_BlogServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public class CommentBlo:ICommentBlo
    {
        private readonly IAppRepository IAppRepository;
        private readonly ICommon _common;
        public CommentBlo(IAppRepository appRepository, ICommon common)
        {
            this.IAppRepository = appRepository;
            _common = common;
        }
        public List<CommentModel> GetCommentByPostId(long postId)
        {
            List<CommentEntity> commentEntities = IAppRepository.CommentDao.GetCommentByPostId(postId);
            List<CommentModel> commentModels = new List<CommentModel>();
            if(commentEntities != null && commentEntities.Count > 0)
            {
                commentModels = commentEntities.Select(x => new CommentModel
                {
                    contentComment = x.contentComment,
                    dateCreate = x.dateCreate,
                    guestId = x.guestId,
                    Id = x.Id,
                    postId = x.postId,
                    guest = x.guest == null ? null : new GuestModel()
                    {
                        Id = x.guest.Id,
                        email = x.guest.email,
                        fullName = x.guest.fullName,
                    },
                }).ToList();
            }
            return commentModels;
        }

        public void CreateComment(CommentRequest request, int guestId)
        {
            IAppRepository.CommentDao.CreateComment(request, guestId);
        }

        public async Task<List<CommentModel>> GetNewCommentDashboard(int records)
        {
            List<CommentEntity> commentEntities = await IAppRepository.CommentDao.GetNewCommentDashboard(records);
            List<CommentModel> commentModels = new List<CommentModel>();
            if (commentEntities != null && commentEntities.Count > 0)
            {
                commentModels = commentEntities.Select(x => new CommentModel
                {
                    contentComment = x.contentComment,
                    dateCreate = x.dateCreate,
                    guestId = x.guestId,
                    Id = x.Id,
                    postId = x.postId,
                    guest = x.guest == null ? null : new GuestModel()
                    {
                        Id = x.guest.Id,
                        email = x.guest.email,
                        fullName = x.guest.fullName,
                    },
                }).ToList();
            }
            return commentModels;
        }

        public async Task<List<CommentModel>> FindCommentByPostId(long postId)
        {
            List<CommentEntity> commentEntities = await IAppRepository.CommentDao.FindCommentByPostId(postId);
            List<CommentModel> commentModels = new List<CommentModel>();
            if (commentEntities != null && commentEntities.Count > 0)
            {
                commentModels = commentEntities.Select(x => new CommentModel
                {
                    contentComment = x.contentComment,
                    dateCreate = x.dateCreate,
                    guestId = x.guestId,
                    Id = x.Id,
                    postId = x.postId,
                    guest = x.guest == null ? null : new GuestModel()
                    {
                        Id = x.guest.Id,
                        email = x.guest.email,
                        fullName = x.guest.fullName,
                    },
                }).ToList();
            }
            return commentModels;
        }

        public CommentModel FindCommentById(long commentId)
        {
            CommentEntity entity = IAppRepository.CommentDao.FindCommentById(commentId);
            CommentModel model = new CommentModel();
            if (entity != null)
            {
                model.postId = entity.postId;
                model.Id = entity.Id;
                model.contentComment = entity.contentComment;
                model.dateCreate = entity.dateCreate;
                model.guestId = entity.guestId;
            }
            return model;
        }

        public async Task<int> DeleteComment(CommentModel model)
        {
            CommentEntity entity = new CommentEntity();
            entity.Id = model.Id;
            return await IAppRepository.CommentDao.DeleteComment(entity);
        }
    }
}
