﻿using API_BlogCore.Requests;
using API_BlogServices.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public interface IGuestBlo
    {
        GuestModel FindGuestByEmail(string email);
        void CreateGuest(CommentRequest request);
        void UpdateGuest(CommentRequest request, int guestId);
        Task<List<GuestModel>> GetAllGuest();
    }
}
