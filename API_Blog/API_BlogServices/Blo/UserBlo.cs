﻿using API_BlogCore.Common;
using API_BlogDao.Dao;
using API_BlogDao.Models;
using API_BlogServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public class UserBlo:IUserBlo
    {
        private readonly IAppRepository IAppRepository;
        private readonly ICommon _common;
        public UserBlo(IAppRepository appRepository, ICommon common)
        {
            this.IAppRepository = appRepository;
            _common = common;
        }

        public async Task<List<UserModel>> GetAllUser()
        {
            List<UserEntity> entities = await IAppRepository.UserDao.GetAllUser();
            List<UserModel> models = new List<UserModel>();
            if(entities != null && entities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                models = entities.Select(x => new UserModel
                {
                    address = x.address,
                    description = x.description,
                    email = x.email,
                    fullName = x.fullName,
                    Id = x.Id,
                    pathAvatar = pathImg + x.pathAvatar,
                    phoneNumber = x.phoneNumber,
                    roleId = x.roleId,
                    status = x.status,
                    role = new RoleModel() { Id = x.role.Id, roleName = x.role.roleName }
                }).ToList();
            }
            return models;
        }

        public UserModel FindUserByEmail(string email, bool isPW)
        {
            UserModel model = new UserModel();
            UserEntity entity = IAppRepository.UserDao.FindUserByEmail(email);
            if(entity != null)
            {
                model.address = entity.address;
                model.description = entity.description;
                model.email = entity.email;
                model.fullName = entity.fullName;
                model.Id = entity.Id;
                model.password = isPW ? entity.password : string.Empty;
                model.pathAvatar = _common.GetPathImg() + entity.pathAvatar;
                model.phoneNumber = entity.phoneNumber;
                model.roleId = entity.roleId;
                model.role = new RoleModel() { Id = entity.role.Id, roleName = entity.role.roleName };
            }
            return model;
        }

        public UserModel FindUserById(int userId)
        {
            UserModel model = new UserModel();
            UserEntity entity = IAppRepository.UserDao.FindUserById(userId);
            if (entity != null)
            {
                model.address = entity.address;
                model.description = entity.description;
                model.email = entity.email;
                model.fullName = entity.fullName;
                model.Id = entity.Id;
                model.pathAvatar = _common.GetPathImg() + entity.pathAvatar;
                model.phoneNumber = entity.phoneNumber;
                model.roleId = entity.roleId;
                model.role = new RoleModel() { Id = entity.role.Id, roleName = entity.role.roleName };
            }
            return model;
        }

        public void UpdateUser(UserModel user, bool isPw)
        {
            UserEntity entity = new UserEntity();
            entity.address = user.address;
            entity.description = user.description;
            entity.email = user.email;
            entity.fullName = user.fullName;
            entity.Id = user.Id;
            entity.phoneNumber = user.phoneNumber;
            entity.pathAvatar = user.pathAvatar;
            entity.roleId = user.roleId;
            entity.status = user.status;
            IAppRepository.UserDao.UpdateUser(entity);
        }

        public async Task<int> CreateUser(UserModel user)
        {
            UserEntity entity = new UserEntity();
            entity.address = user.address;
            entity.description = user.description;
            entity.email = user.email;
            entity.fullName = user.fullName;
            entity.Id = user.Id;
            entity.phoneNumber = user.phoneNumber;
            entity.pathAvatar = user.pathAvatar;
            entity.roleId = user.roleId;
            entity.status = user.status;
            entity.password = user.password;
            return await IAppRepository.UserDao.CreateUser(entity);
        }
        public async Task<int> UpdateUserSync(UserModel user, bool isPw)
        {
            UserEntity entity = new UserEntity();
            entity.address = user.address;
            entity.description = user.description;
            entity.email = user.email;
            entity.fullName = user.fullName;
            entity.Id = user.Id;
            entity.phoneNumber = user.phoneNumber;
            entity.pathAvatar = user.pathAvatar;
            entity.roleId = user.roleId;
            entity.status = user.status;
            entity.password = user.password;
            if (isPw)
            {
                entity.password = user.password;
            }
            return await IAppRepository.UserDao.UpdateUserSync(entity);
        }

        public int CountUserRoleAdmin()
        {
            return IAppRepository.UserDao.CountUserRoleAdmin();
        }
    }
}
