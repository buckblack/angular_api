﻿using API_BlogCore.Common;
using API_BlogDao.Dao;
using API_BlogDao.Models;
using API_BlogServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public class SubcategoryBlo:ISubCategoryBlo
    {
        private readonly IAppRepository IAppRepository;
        private readonly ICommon _common;
        public SubcategoryBlo(IAppRepository appRepository, ICommon common)
        {
            this.IAppRepository = appRepository;
            _common = common;
        }

        public List<SubCategoryModel> GetAllSubCategory()
        {
            //get data
            List<SubCategoryModel> result = new List<SubCategoryModel>();
            List<SubCategoryEntity> model = IAppRepository.SubcategoryDao.GetAllSubCategory();
            if (model != null)
            {
                string pathImg = _common.GetPathImg();
                result = model.Select(x => new SubCategoryModel
                {
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    subCateName = x.subCateName,
                    countPost= x.posts != null ? x.posts.LongCount() : 0,
                }).ToList();
            }
            return result;
        }

        public List<SubCategoryModel> GetSubCategoryByCateId(int cateId)
        {
            //get data
            List<SubCategoryModel> result = new List<SubCategoryModel>();
            List<SubCategoryEntity> model = IAppRepository.SubcategoryDao.GetSubCategoryByCateId(cateId);
            if (model != null)
            {
                string pathImg = _common.GetPathImg();
                result = model.Select(x => new SubCategoryModel
                {
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    subCateName = x.subCateName,
                }).ToList();
            }
            return result;
        }

        public async Task<List<SubCategoryModel>> GetSubCategoriesByCateIdSync(int cateId)
        {
            List<SubCategoryModel> result = new List<SubCategoryModel>();
            List<SubCategoryEntity> model = await IAppRepository.SubcategoryDao.GetSubCategoriesByCateIdSync(cateId);
            if (model != null)
            {
                string pathImg = _common.GetPathImg();
                result = model.Select(x => new SubCategoryModel
                {
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    subCateName = x.subCateName,
                }).ToList();
            }
            return result;
        }

        public long CountTotalPostBySubCateId(int cateId)
        {
            return IAppRepository.SubcategoryDao.CountTotalPostBySubCateId(cateId);
        }

        public List<PostModel> GetPostBySubCateIdPaging(int cateId, int sumDisplay, int currentPage)
        {
            List<PostModel> result = new List<PostModel>();
            List<PostEntity> model = IAppRepository.SubcategoryDao.GetPostBySubCateIdPaging(cateId, sumDisplay, currentPage);
            if (model != null)
            {
                string pathImg = _common.GetPathImg();
                result = model.Select(x => new PostModel
                {
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    postTitle = x.postTitle,
                    summaryContent = x.summaryContent,
                    user = new UserModel()
                    {
                        fullName = x.user.fullName,
                        Id = x.user.Id,
                        pathAvatar = x.user.pathAvatar,
                    },
                    subCategories = new SubCategoryModel()
                    {
                        Id = x.subCategories.Id,
                        subCateName = x.subCategories.subCateName,
                        pathThumbnail = x.subCategories.pathThumbnail,
                    },
                }).ToList();
            }
            return result;
        }

        public async Task<List<PostModel>> GetPostBySubCateId(int cateId)
        {
            List<PostModel> result = new List<PostModel>();
            List<PostEntity> model = await IAppRepository.SubcategoryDao.GetPostBySubCateId(cateId);
            if (model != null)
            {
                string pathImg = _common.GetPathImg();
                result = model.Select(x => new PostModel
                {
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    postTitle = x.postTitle,
                    status = x.status,
                    detailContent = x.detailContent,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    summaryContent = x.summaryContent,
                    user = new UserModel()
                    {
                        fullName = x.user.fullName,
                        Id = x.user.Id,
                        pathAvatar = x.user.pathAvatar,
                    },
                    subCategories = new SubCategoryModel()
                    {
                        Id = x.subCategories.Id,
                        subCateName = x.subCategories.subCateName,
                        pathThumbnail = x.subCategories.pathThumbnail,
                        categoryId = x.subCategories.categoryId,
                    },
                    comments = x.comments == null ? new List<CommentModel>() : x.comments.Select(c => new CommentModel {
                        Id = c.Id
                    }).ToList(),
                }).ToList();
            }
            return result;
        }

        public async Task<int> CreateSubCategory(SubCategoryModel model)
        {
            SubCategoryEntity entity = new SubCategoryEntity();
            entity.categoryId = model.categoryId;
            entity.pathThumbnail = model.pathThumbnail;
            entity.subCateName = model.subCateName;
            return await IAppRepository.SubcategoryDao.CreateSubCategory(entity);
        }
        public async Task<int> UpdateSubCategory(SubCategoryModel model)
        {
            SubCategoryEntity entity = new SubCategoryEntity();
            entity.categoryId = model.categoryId;
            entity.pathThumbnail = model.pathThumbnail;
            entity.subCateName = model.subCateName;
            entity.Id = model.Id;
            return await IAppRepository.SubcategoryDao.UpdateSubCategory(entity);
        }
        public async Task<int> DeleteSubCategory(SubCategoryModel model)
        {
            SubCategoryEntity entity = new SubCategoryEntity();
            entity.categoryId = model.categoryId;
            entity.pathThumbnail = model.pathThumbnail;
            entity.subCateName = model.subCateName;
            entity.Id = model.Id;
            return await IAppRepository.SubcategoryDao.DeleteSubCategory(entity);
        }

        public SubCategoryModel FindSubCategoryById(int subCateId)
        {
            SubCategoryEntity entity = IAppRepository.SubcategoryDao.FindSubCategoryById(subCateId);
            SubCategoryModel model = new SubCategoryModel();
            if(entity != null)
            {
                string pathImg = _common.GetPathImg();
                model.categoryId = entity.categoryId;
                model.Id = entity.Id;
                model.pathThumbnail = pathImg + entity.pathThumbnail;
                model.subCateName = entity.subCateName;
                model.category = new CategoryModel() {
                    cateName = entity.category.cateName,
                    Id = entity.category.Id,
                };
                model.posts = entity.posts == null ? null : entity.posts.Select(x => new PostModel {
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    postTitle = x.postTitle,
                    status = x.status,
                    detailContent = x.detailContent,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    summaryContent = x.summaryContent,
                }).ToList();
            }
            return model;
        }

        public bool CheckExistsSubCategory(int subCateId)
        {
            return IAppRepository.SubcategoryDao.CheckExistsSubCategory(subCateId);
        }

        public bool CheckExistsSubCateNameInCategory(string subCateName, int cateId)
        {
            return IAppRepository.SubcategoryDao.CheckExistsSubCateNameInCategory(subCateName, cateId);
        }

        public bool CheckExistsSubCateNameInCategoryUpdate(string subCateName, int cateId)
        {
            return IAppRepository.SubcategoryDao.CheckExistsSubCateNameInCategoryUpdate(subCateName, cateId);
        }
    }
}
