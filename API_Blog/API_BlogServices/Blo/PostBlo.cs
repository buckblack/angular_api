﻿using API_BlogCore.Common;
using API_BlogCore.Requests;
using API_BlogDao.Dao;
using API_BlogDao.Models;
using API_BlogDao.View;
using API_BlogServices.Models;
using API_BlogServices.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public class PostBlo : IPostBlo
    {
        private readonly IAppRepository IAppRepository;
        private readonly ICommon _common;
        public PostBlo(IAppRepository appRepository, ICommon common)
        {
            this.IAppRepository = appRepository;
            _common = common;
        }

        public List<PostsHotView> GetPostsHot(int records)
        {
            List<PostsHotViewEntity> model = IAppRepository.PostDao.GetPostsHot(records);
            List<PostsHotView> result = new List<PostsHotView>();
            if (model != null && model.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                result = model.Select(x => new PostsHotView
                {
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    postTitle = x.postTitle,
                    summaryContent = x.summaryContent,
                    fullName = x.fullName,
                    subCateName = x.subCateName,
                }).ToList();
            }
            return result;
        }

        public PostModel FindPostById(long postId)
        {
            PostModel post = new PostModel();
            PostEntity postEntity = IAppRepository.PostDao.FindPostById(postId);
            if (postEntity != null)
            {
                post.dateCreate = postEntity.dateCreate;
                post.detailContent = postEntity.detailContent;
                post.Id = postEntity.Id;
                post.pathThumbnail = postEntity.pathThumbnail;
                post.postTitle = postEntity.postTitle;
                post.status = postEntity.status;
                post.summaryContent = postEntity.summaryContent;
                post.userId = postEntity.userId;
                post.subCategoriesId = postEntity.subCategoriesId;
            }
            return post;
        }

        public long CountPostsRelatedRecord(int subCateId, long postId)
        {
            return IAppRepository.PostDao.CountPostsRelatedRecord(subCateId, postId);
        }

        public List<PostModel> GetPostsRelatedPaging(int subCateId, long postId, int currentPage, int sumDisplay)
        {
            List<PostModel> postModels = new List<PostModel>();
            List<PostEntity> postEntities = IAppRepository.PostDao.GetPostsRelatedPaging(subCateId, postId, currentPage, sumDisplay);
            if (postEntities != null && postEntities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                postModels = postEntities.Select(x => new PostModel
                {
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    postTitle = x.postTitle,
                    summaryContent = x.summaryContent,
                    user = x.user == null ? null : new UserModel()
                    {
                        address = x.user.address,
                        description = x.user.description,
                        email = x.user.email,
                        fullName = x.user.fullName,
                        Id = x.user.Id,
                        pathAvatar = pathImg + x.user.pathAvatar,
                        phoneNumber = x.user.phoneNumber,
                        roleId = x.user.roleId
                    },
                    subCategories = x.subCategories == null ? null : new SubCategoryModel()
                    {
                        Id = x.subCategories.Id,
                        categoryId = x.subCategories.categoryId,
                        pathThumbnail = x.subCategories.pathThumbnail,
                        subCateName = x.subCategories.subCateName,
                    },
                }).ToList();
            }
            return postModels;
        }

        public async Task<List<PostModel>> SearchPostById(long postId)
        {
            List<PostModel> postModels = new List<PostModel>();
            List<PostEntity> postEntities = await IAppRepository.PostDao.SearchPostById(postId);
            if (postEntities != null && postEntities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                postModels = postEntities.Select(x => new PostModel
                {
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    postTitle = x.postTitle,
                    summaryContent = x.summaryContent,
                    
                }).ToList();
            }
            return postModels;
        }

        public long CountAllPostEnable()
        {
            return IAppRepository.PostDao.CountAllPostEnable();
        }

        public long CountAllPost()
        {
            return IAppRepository.PostDao.CountAllPost();
        }

        public List<PostModel> GetPostsRecentPaging(int currentPage, int sumDisplay)
        {
            List<PostModel> postModels = new List<PostModel>();
            List<PostEntity> postEntities = IAppRepository.PostDao.GetPostsRecentPaging(currentPage, sumDisplay);
            if (postEntities != null && postEntities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                postModels = postEntities.Select(x => new PostModel
                {
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    postTitle = x.postTitle,
                    summaryContent = x.summaryContent,
                    user = x.user == null ? null : new UserModel()
                    {
                        address = x.user.address,
                        description = x.user.description,
                        email = x.user.email,
                        fullName = x.user.fullName,
                        Id = x.user.Id,
                        pathAvatar = pathImg + x.user.pathAvatar,
                        phoneNumber = x.user.phoneNumber,
                        roleId = x.user.roleId
                    },
                    subCategories = x.subCategories == null ? null : new SubCategoryModel()
                    {
                        Id = x.subCategories.Id,
                        categoryId = x.subCategories.categoryId,
                        pathThumbnail = pathImg + x.subCategories.pathThumbnail,
                        subCateName = x.subCategories.subCateName,
                    },
                }).ToList();
            }
            return postModels;
        }

        public List<PostModel> GetPostsRecentComment(int recordDisplay)
        {
            List<PostModel> postModels = new List<PostModel>();
            List<PostEntity> postEntities = IAppRepository.PostDao.GetPostsRecentComment(recordDisplay);
            if (postEntities != null && postEntities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                postModels = postEntities.Select(x => new PostModel
                {
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    postTitle = x.postTitle,
                    summaryContent = x.summaryContent,
                    user = x.user == null ? null : new UserModel()
                    {
                        address = x.user.address,
                        description = x.user.description,
                        email = x.user.email,
                        fullName = x.user.fullName,
                        Id = x.user.Id,
                        pathAvatar = pathImg + x.user.pathAvatar,
                        phoneNumber = x.user.phoneNumber,
                        roleId = x.user.roleId
                    },
                    subCategories = x.subCategories == null ? null : new SubCategoryModel()
                    {
                        Id = x.subCategories.Id,
                        categoryId = x.subCategories.categoryId,
                        pathThumbnail = x.subCategories.pathThumbnail,
                        subCateName = x.subCategories.subCateName,
                    },
                }).ToList();
            }
            return postModels;
        }

        public PostModel GetPostDetail(long postId)
        {
            PostModel postModels = new PostModel();
            PostEntity postEntities = IAppRepository.PostDao.GetPostDetail(postId);
            if (postEntities != null)
            {
                string pathImg = _common.GetPathImg();
                postModels.dateCreate = postEntities.dateCreate;
                postModels.Id = postEntities.Id;
                postModels.pathThumbnail = pathImg + postEntities.pathThumbnail;
                postModels.postTitle = postEntities.postTitle;
                postModels.summaryContent = postEntities.summaryContent;
                postModels.user = postEntities.user == null ? null : new UserModel()
                {
                    address = postEntities.user.address,
                    description = postEntities.user.description,
                    email = postEntities.user.email,
                    fullName = postEntities.user.fullName,
                    Id = postEntities.user.Id,
                    pathAvatar = pathImg + postEntities.user.pathAvatar,
                    phoneNumber = postEntities.user.phoneNumber,
                    roleId = postEntities.user.roleId
                };
                postModels.subCategories = postEntities.subCategories == null ? null : new SubCategoryModel()
                {
                    Id = postEntities.subCategories.Id,
                    categoryId = postEntities.subCategories.categoryId,
                    pathThumbnail = postEntities.subCategories.pathThumbnail,
                    subCateName = postEntities.subCategories.subCateName,
                };
                postModels.comments = postEntities.comments == null ? null : postEntities.comments.Select(x => new CommentModel
                {
                    contentComment = x.contentComment,
                    dateCreate = x.dateCreate,
                    guestId = x.guestId,
                    Id = x.Id,
                    postId = x.postId,
                }).ToList();
            }
            return postModels;
        }

        public long CountSearch(string searchKey)
        {
            return IAppRepository.PostDao.CountSearch(searchKey);
        }
        public List<PostModel> Search(string searchKey, int currentPage, int sumDisplay)
        {
            List<PostModel> postModels = new List<PostModel>();
            List<PostEntity> postEntities = IAppRepository.PostDao.Search(searchKey,currentPage, sumDisplay);
            if (postEntities != null && postEntities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                postModels = postEntities.Select(x => new PostModel
                {
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    postTitle = x.postTitle,
                    summaryContent = x.summaryContent,
                    user = x.user == null ? null : new UserModel()
                    {
                        address = x.user.address,
                        description = x.user.description,
                        email = x.user.email,
                        fullName = x.user.fullName,
                        Id = x.user.Id,
                        pathAvatar = pathImg + x.user.pathAvatar,
                        phoneNumber = x.user.phoneNumber,
                        roleId = x.user.roleId
                    },
                    subCategories = x.subCategories == null ? null : new SubCategoryModel()
                    {
                        Id = x.subCategories.Id,
                        categoryId = x.subCategories.categoryId,
                        pathThumbnail = x.subCategories.pathThumbnail,
                        subCateName = x.subCategories.subCateName,
                    },
                }).ToList();
            }
            return postModels;
        }

        public List<PostModel> SortSearch(string key, bool author, bool title, bool content, int sort)
        {
            List<PostModel> postModels = new List<PostModel>();
            List<PostEntity> postEntities = new List<PostEntity>();
            if (!author && !title && !content)
            {
                postEntities = IAppRepository.PostDao.SortSearch(key);
            }
            else
            {
                if (author)
                {
                    postEntities.AddRange(IAppRepository.PostDao.SortSearchByAuthor(key));
                }
                if (title)
                {
                    postEntities.AddRange(IAppRepository.PostDao.SortSearchByTitle(key));
                }
                if (content)
                {
                    postEntities.AddRange(IAppRepository.PostDao.SortSearchByContent(key));
                }
            }
            postEntities.GroupBy(x => x.Id).Select(x => x.FirstOrDefault());
            if(postEntities != null && postEntities.Count > 0)
            {
                //sort
                if (sort == 2)
                {
                    postEntities.OrderBy(x => x.dateCreate);
                }
                else if (sort == 3)
                {
                    postEntities.OrderByDescending(x => x.subCategories.subCateName);
                }
                else if (sort == 4)
                {
                    postEntities.OrderBy(x => x.subCategories.subCateName);
                }
                else
                {
                    postEntities.OrderByDescending(x => x.dateCreate);
                }
                string pathImg = _common.GetPathImg();
                postModels = postEntities.Select(x => new PostModel
                {
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    postTitle = x.postTitle,
                    summaryContent = x.summaryContent,
                    user = x.user == null ? null : new UserModel()
                    {
                        address = x.user.address,
                        description = x.user.description,
                        email = x.user.email,
                        fullName = x.user.fullName,
                        Id = x.user.Id,
                        pathAvatar = pathImg + x.user.pathAvatar,
                        phoneNumber = x.user.phoneNumber,
                        roleId = x.user.roleId
                    },
                    subCategories = x.subCategories == null ? null : new SubCategoryModel()
                    {
                        Id = x.subCategories.Id,
                        categoryId = x.subCategories.categoryId,
                        pathThumbnail = x.subCategories.pathThumbnail,
                        subCateName = x.subCategories.subCateName,
                    },
                }).ToList();
            }
            return postModels;
        }

        public List<PostModel> SortSearchPaging(List<PostModel> posts,int currentPage, int sumDisplay)
        {
            return posts.Skip((currentPage - 1) * sumDisplay).Take(sumDisplay).ToList();
        }

        public async Task<List<PostModel>> GetPostsNewDashBoard(int records)
        {
            List<PostModel> postModels = new List<PostModel>();
            List<PostEntity> postEntities = await IAppRepository.PostDao.GetPostsNewDashBoard(records);
            if (postEntities != null && postEntities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                postModels = postEntities.Select(x => new PostModel
                {
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    postTitle = x.postTitle,
                    summaryContent = x.summaryContent,
                    user = x.user == null ? null : new UserModel()
                    {
                        address = x.user.address,
                        description = x.user.description,
                        email = x.user.email,
                        fullName = x.user.fullName,
                        Id = x.user.Id,
                        pathAvatar = pathImg + x.user.pathAvatar,
                        phoneNumber = x.user.phoneNumber,
                        roleId = x.user.roleId
                    },
                    subCategories = x.subCategories == null ? null : new SubCategoryModel()
                    {
                        Id = x.subCategories.Id,
                        categoryId = x.subCategories.categoryId,
                        pathThumbnail = x.subCategories.pathThumbnail,
                        subCateName = x.subCategories.subCateName,
                    },
                }).ToList();
            }
            return postModels;
        }

        public async Task<List<PostsHotView>> GetPostsHotDashBoard(int records)
        {
            List<PostsHotView> postModels = new List<PostsHotView>();
            List<PostsHotViewEntity> postEntities = await IAppRepository.PostDao.GetPostsHotDashBoard(records);
            if (postEntities != null && postEntities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                postModels = postEntities.Select(x => new PostsHotView
                {
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    postTitle = x.postTitle,
                    fullName = x.fullName,
                    subCateName = x.subCateName,
                }).ToList();
            }
            return postModels;
        }

        public async Task<int> CreatePost(PostModel model)
        {
            PostEntity entity = new PostEntity();
            entity.dateCreate = model.dateCreate;
            entity.detailContent = model.detailContent;
            entity.pathThumbnail = model.pathThumbnail;
            entity.postTitle = model.postTitle;
            entity.status = model.status;
            entity.subCategoriesId = model.subCategoriesId;
            entity.summaryContent = model.summaryContent;
            entity.userId = model.userId;
            return await IAppRepository.PostDao.CreatePost(entity);
        }
        public async Task<int> UpdatePost(PostModel model)
        {
            PostEntity entity = new PostEntity();
            entity.Id = model.Id;
            entity.dateCreate = model.dateCreate;
            entity.detailContent = model.detailContent;
            entity.pathThumbnail = model.pathThumbnail;
            entity.postTitle = model.postTitle;
            entity.status = model.status;
            entity.subCategoriesId = model.subCategoriesId;
            entity.summaryContent = model.summaryContent;
            //entity.userId = model.userId;
            return await IAppRepository.PostDao.UpdatePost(entity);
        }

        public async Task<List<PostModel>> SearchPostsComment(string key)
        {
            List<PostModel> postModels = new List<PostModel>();
            List<PostEntity> postEntities = await IAppRepository.PostDao.SearchPostsComment(key);
            if (postEntities != null && postEntities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                postModels = postEntities.Select(x => new PostModel
                {
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    postTitle = x.postTitle,
                    summaryContent = x.summaryContent,
                    comments = x.comments == null ? null : x.comments.Select(c => new CommentModel
                    {
                        contentComment = c.contentComment,
                        dateCreate = c.dateCreate,
                        guestId = c.guestId,
                        Id = c.Id,
                        postId = c.postId,
                    }).ToList(),
                }).ToList();
            }
            return postModels;
        }

        public List<PostModel> GetPostBySubCategoryIdPaging(int subCateId, int currentPage, int sumDisplay)
        {
            List<PostModel> postModels = new List<PostModel>();
            List<PostEntity> postEntities = IAppRepository.PostDao.GetPostBySubCategoryIdPaging(subCateId, currentPage, sumDisplay);
            if (postEntities != null && postEntities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                postModels = postEntities.Select(x => new PostModel
                {
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    postTitle = x.postTitle,
                    summaryContent = x.summaryContent,
                    user = x.user == null ? null : new UserModel()
                    {
                        address = x.user.address,
                        description = x.user.description,
                        email = x.user.email,
                        fullName = x.user.fullName,
                        Id = x.user.Id,
                        pathAvatar = pathImg + x.user.pathAvatar,
                        phoneNumber = x.user.phoneNumber,
                        roleId = x.user.roleId
                    },
                    subCategories = x.subCategories == null ? null : new SubCategoryModel()
                    {
                        Id = x.subCategories.Id,
                        categoryId = x.subCategories.categoryId,
                        pathThumbnail = x.subCategories.pathThumbnail,
                        subCateName = x.subCategories.subCateName,
                    },
                }).ToList();
            }
            return postModels;
        }

        public long CoutPostBySubCateID(int subCateId)
        {
            return IAppRepository.PostDao.CoutPostBySubCateID(subCateId);
        }
    }
}
