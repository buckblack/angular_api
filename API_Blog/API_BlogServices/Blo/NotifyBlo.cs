﻿using API_BlogCore.Common;
using API_BlogDao.Dao;
using API_BlogDao.Models;
using API_BlogServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public class NotifyBlo : INotifyBlo
    {
        private readonly IAppRepository IAppRepository;
        private readonly ICommon _common;
        public NotifyBlo(IAppRepository appRepository, ICommon common)
        {
            this.IAppRepository = appRepository;
            _common = common;
        }

        public async Task<int> CreateNotify(NotifyModel model)
        {
            NotifyEntity entity = new NotifyEntity();
            entity.contentNotify = model.contentNotify;
            entity.dateCreate = model.dateCreate;
            entity.notifyTitle = model.notifyTitle;
            entity.userId = model.userId;
            return await IAppRepository.NotifyDao.CreateNotify(entity);
        }
        public async Task<NotifyModel> FindNotifyByID(long id)
        {
            NotifyModel model = new NotifyModel();
            NotifyEntity entity = await IAppRepository.NotifyDao.FindNotifyByID(id);
            if(entity != null)
            {
                string pathImg = _common.GetPathImg();
                model.contentNotify = entity.contentNotify;
                model.dateCreate = entity.dateCreate;
                model.Id = entity.Id;
                model.notifyTitle = entity.notifyTitle;
                model.userId = entity.userId;
                model.user = new UserModel()
                {
                    Id = entity.user.Id,
                    address = entity.user.address,
                    description = entity.user.description,
                    email = entity.user.email,
                    fullName = entity.user.fullName,
                    pathAvatar = pathImg + entity.user.pathAvatar,
                    roleId = entity.user.roleId,
                    phoneNumber = entity.user.phoneNumber,
                };
            }
            return model;
        }
        public async Task<int> DeleteNotify(NotifyModel model)
        {
            NotifyEntity entity = new NotifyEntity();
            entity.Id = model.Id;
            entity.contentNotify = model.contentNotify;
            entity.dateCreate = model.dateCreate;
            entity.notifyTitle = model.notifyTitle;
            entity.userId = model.userId;
            return await IAppRepository.NotifyDao.DeleteNotify(entity);
        }
        public async Task<List<NotifyModel>> GetNotify(int records = 0)
        {
            List<NotifyModel> models = new List<NotifyModel>();
            List<NotifyEntity> entities = await IAppRepository.NotifyDao.GetNotify(records);
            if(entities != null && entities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                models = entities.Select(x => new NotifyModel
                {
                    contentNotify = x.contentNotify,
                    dateCreate = x.dateCreate,
                    Id = x.Id,
                    notifyTitle = x.notifyTitle,
                    userId = x.userId,
                    user = new UserModel()
                    {
                        Id = x.user.Id,
                        address = x.user.address,
                        description = x.user.description,
                        email = x.user.email,
                        fullName = x.user.fullName,
                        pathAvatar = pathImg + x.user.pathAvatar,
                        roleId = x.user.roleId,
                        phoneNumber = x.user.phoneNumber,
                    }
                }).ToList();
            }
            return models;
        }
    }
}
