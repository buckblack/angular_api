﻿using API_BlogServices.Models;
using API_BlogServices.Models.View;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public interface IPostBlo
    {
        List<PostsHotView> GetPostsHot(int records);
        PostModel FindPostById(long postId);
        long CountPostsRelatedRecord(int subCateId, long postId);
        List<PostModel> GetPostsRelatedPaging(int subCateId, long postId, int currentPage, int sumDisplay);
        long CountAllPostEnable();
        long CountAllPost();
        List<PostModel> GetPostsRecentPaging(int currentPage, int sumDisplay);
        List<PostModel> GetPostsRecentComment(int recordDisplay);
        PostModel GetPostDetail(long postId);
        long CountSearch(string searchKey);
        List<PostModel> Search(string searchKey, int currentPage, int sumDisplay);
        List<PostModel> SortSearch(string key, bool author, bool title, bool content, int sort);
        List<PostModel> SortSearchPaging(List<PostModel> posts, int currentPage, int sumDisplay);
        Task<List<PostModel>> GetPostsNewDashBoard(int records);
        Task<List<PostsHotView>> GetPostsHotDashBoard(int records);
        Task<List<PostModel>> SearchPostById(long postId);
        Task<int> CreatePost(PostModel model);
        Task<int> UpdatePost(PostModel model);
        Task<List<PostModel>> SearchPostsComment(string key);
        List<PostModel> GetPostBySubCategoryIdPaging(int subCateId, int currentPage, int sumDisplay);
        long CoutPostBySubCateID(int subCateId);
    }
}
