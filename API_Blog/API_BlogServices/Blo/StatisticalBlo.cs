﻿using API_BlogCore.Common;
using API_BlogDao.Dao;
using API_BlogDao.View;
using API_BlogServices.Models.View;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace API_BlogServices.Blo
{
    public class StatisticalBlo:IStatisticalBlo
    {
        private readonly IAppRepository IAppRepository;
        private readonly ICommon _common;
        public StatisticalBlo(IAppRepository appRepository, ICommon common)
        {
            this.IAppRepository = appRepository;
            _common = common;
        }
        public List<StatisticalPostView> GetPostChartByTime(string dateFrom, string dateTo)
        {
            List<StatisticalPostView> postViews = new List<StatisticalPostView>();
            List <StatisticalPostViewEntity> entities = IAppRepository.StatisticalDao.GetPostChartByTime(dateFrom, dateTo);
            if(entities != null && entities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                postViews = entities.Select(x => new StatisticalPostView
                {
                    categoryId = x.categoryId,
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    subCateName = x.subCateName,
                    sumPost = x.sumPost
                }).ToList();
            }
            return postViews;
        }
        public List<StatisticalPostView> GetPostChartAllTime()
        {
            List<StatisticalPostView> postViews = new List<StatisticalPostView>();
            List<StatisticalPostViewEntity> entities = IAppRepository.StatisticalDao.GetPostChartAllTime();
            if (entities != null && entities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                postViews = entities.Select(x => new StatisticalPostView
                {
                    categoryId = x.categoryId,
                    Id = x.Id,
                    pathThumbnail = pathImg + x.pathThumbnail,
                    subCateName = x.subCateName,
                    sumPost = x.sumPost
                }).ToList();
            }
            return postViews;
        }
        public List<StatisticalCommentView> GetCommentChartAllTime(int records)
        {
            List<StatisticalCommentView> commentViews = new List<StatisticalCommentView>();
            List<StatisticalCommentViewEntity> entities = IAppRepository.StatisticalDao.GetCommentChartAllTime(records);
            if (entities != null && entities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                commentViews = entities.Select(x => new StatisticalCommentView
                {
                    Id = x.Id,
                    postTitle = x.postTitle,
                    sumComment = x.sumComment,
                }).ToList();
            }
            return commentViews;
        }
        public List<StatisticalCommentView> GetCommentChartByTime(string dateFrom, string dateTo)
        {
            List<StatisticalCommentView> commentViews = new List<StatisticalCommentView>();
            List<StatisticalCommentViewEntity> entities = IAppRepository.StatisticalDao.GetCommentChartByTime(dateFrom, dateTo);
            if (entities != null && entities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                commentViews = entities.Select(x => new StatisticalCommentView
                {
                    Id = x.Id,
                    postTitle = x.postTitle,
                    sumComment = x.sumComment,
                }).ToList();
            }
            return commentViews;
        }
        public List<StatisticalUserView> GetUserChartAllTime(int records)
        {
            List<StatisticalUserView> userViews = new List<StatisticalUserView>();
            List<StatisticalUserViewEntity> entities = IAppRepository.StatisticalDao.GetUserChartAllTime(records);
            if (entities != null && entities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                userViews = entities.Select(x => new StatisticalUserView
                {
                    Id = x.Id,
                    fullName = x.fullName,
                    sumPost = x.sumPost,
                    pathAvatar = pathImg + x.pathAvatar,
                }).ToList();
            }
            return userViews;
        }
        public List<StatisticalUserView> GetUserChartByTime(string dateFrom, string dateTo)
        {
            List<StatisticalUserView> userViews = new List<StatisticalUserView>();
            List<StatisticalUserViewEntity> entities = IAppRepository.StatisticalDao.GetUserChartByTime(dateFrom, dateTo);
            if (entities != null && entities.Count > 0)
            {
                string pathImg = _common.GetPathImg();
                userViews = entities.Select(x => new StatisticalUserView
                {
                    Id = x.Id,
                    fullName = x.fullName,
                    sumPost = x.sumPost,
                    pathAvatar = pathImg + x.pathAvatar,
                }).ToList();
            }
            return userViews;
        }
    }
}
