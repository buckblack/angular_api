﻿using API_BlogServices.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public interface IRoleBlo
    {
        Task<List<RoleModel>> GetAllRole();
        bool CheckRoleExists(string roleName);
        RoleModel FindRoleById(int roleId);
        Task<int> CreateRole(RoleModel model);
        Task<int> UpdataRole(RoleModel model);
        Task<int> RemoveRole(RoleModel model);
    }
}
