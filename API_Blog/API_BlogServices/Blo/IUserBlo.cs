﻿using API_BlogServices.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public interface IUserBlo
    {
        UserModel FindUserByEmail(string email, bool isPW);
        UserModel FindUserById(int userId);
        void UpdateUser(UserModel user, bool isPw);
        Task<List<UserModel>> GetAllUser();
        Task<int> CreateUser(UserModel model);
        Task<int> UpdateUserSync(UserModel model, bool isPw);
        int CountUserRoleAdmin();
    }
}
