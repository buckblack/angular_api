﻿using API_BlogCore.Common;
using API_BlogCore.Requests;
using API_BlogDao.Dao;
using API_BlogDao.Models;
using API_BlogServices.Models;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public class GuestBlo:IGuestBlo
    {
        private readonly IAppRepository IAppRepository;
        private readonly ICommon _common;
        public GuestBlo(IAppRepository appRepository, ICommon common)
        {
            this.IAppRepository = appRepository;
            _common = common;
        }
        public GuestModel FindGuestByEmail(string email)
        {
            GuestModel model = new GuestModel();
            GuestEntity guestEntity = IAppRepository.GuestDao.FindGuestByEmail(email);
            if(guestEntity != null)
            {
                model.email = guestEntity.email;
                model.fullName = guestEntity.fullName;
                model.Id = guestEntity.Id;
            }
            return model;
        }

        public void CreateGuest(CommentRequest request)
        {
            IAppRepository.GuestDao.CreateGuest(request);
        }

        public void UpdateGuest(CommentRequest request, int guestId)
        {
            IAppRepository.GuestDao.UpdateGuest(request, guestId);
        }

        public async Task<List<GuestModel>> GetAllGuest()
        {
            List<GuestModel> models = new List<GuestModel>();
            List<GuestEntity> entities= await IAppRepository.GuestDao.GetAllGuest();
            if(entities != null && entities.Count > 0)
            {
                models = entities.Select(x => new GuestModel
                {
                    email = x.email,
                    fullName = x.fullName,
                    Id = x.Id,
                    comment = x.comment == null ? null : new Collection<CommentModel>(x.comment.Select(c => new CommentModel
                    {
                        contentComment = c.contentComment,
                        dateCreate = c.dateCreate,
                        guestId = c.guestId,
                        Id = c.Id,
                        postId = c.postId
                    }).ToList())
                }).ToList();
            }
            return models; 
        }
    }
}
