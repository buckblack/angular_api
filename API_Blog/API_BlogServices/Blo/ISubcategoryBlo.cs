﻿using API_BlogServices.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public interface ISubCategoryBlo
    {
        List<SubCategoryModel> GetAllSubCategory();
        List<SubCategoryModel> GetSubCategoryByCateId(int cateId);
        long CountTotalPostBySubCateId(int cateId);
        List<PostModel> GetPostBySubCateIdPaging(int cateId, int sumDisplay, int currentPage);
        Task<List<PostModel>> GetPostBySubCateId(int cateId);
        Task<List<SubCategoryModel>> GetSubCategoriesByCateIdSync(int cateId);
        Task<int> CreateSubCategory(SubCategoryModel model);
        Task<int> UpdateSubCategory(SubCategoryModel model);
        Task<int> DeleteSubCategory(SubCategoryModel model);
        SubCategoryModel FindSubCategoryById(int subCateId);
        bool CheckExistsSubCategory(int subCateId);
        bool CheckExistsSubCateNameInCategory(string subCateName, int cateId);
        bool CheckExistsSubCateNameInCategoryUpdate(string subCateName, int cateId);
    }
}
