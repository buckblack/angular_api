﻿using API_BlogCore.Requests;
using API_BlogServices.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogServices.Blo
{
    public interface ICommentBlo
    {
        List<CommentModel> GetCommentByPostId(long postId);
        void CreateComment(CommentRequest request, int guestId);
        Task<List<CommentModel>> GetNewCommentDashboard(int records);
        Task<List<CommentModel>> FindCommentByPostId(long postId);
        CommentModel FindCommentById(long commentId);
        Task<int> DeleteComment(CommentModel model);
    }
}
