﻿using API_BlogServices.Models.View;
using System;
using System.Collections.Generic;
using System.Text;

namespace API_BlogServices.Blo
{
    public interface IStatisticalBlo
    {
        List<StatisticalPostView> GetPostChartByTime(string dateFrom, string dateTo);
        List<StatisticalPostView> GetPostChartAllTime();
        List<StatisticalCommentView> GetCommentChartAllTime(int records);
        List<StatisticalCommentView> GetCommentChartByTime(string dateFrom, string dateTo);
        List<StatisticalUserView> GetUserChartAllTime(int records);
        List<StatisticalUserView> GetUserChartByTime(string dateFrom, string dateTo);
    }
}
