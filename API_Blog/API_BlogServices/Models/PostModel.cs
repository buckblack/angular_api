﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_BlogServices.Models
{
    public class PostModel
    {
        [Key]
        public long Id { get; set; }
        public string postTitle { get; set; }
        public string summaryContent { get; set; }
        public string detailContent { get; set; }
        public DateTime dateCreate { get; set; }
        public string pathThumbnail { get; set; }
        public int status { get; set; }
        public ICollection<CommentModel> comments { get; set; }
        public int subCategoriesId { get; set; }
        public virtual SubCategoryModel subCategories { get; set; }
        public int userId { get; set; }
        public virtual UserModel user { get; set; }
    }
}
