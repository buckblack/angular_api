﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_BlogServices.Models
{
    public class UserModel
    {
        [Key]
        public int Id { get; set; }
        public string fullName { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; }
        public string pathAvatar { get; set; }
        public string description { get; set; }
        public string address { get; set; }
        public string password { get; set; }
        public int status { get; set; }
        public ICollection<PostModel> posts { get; set; }
        public ICollection<NotifyModel> notifies { get; set; }
        public int roleId { get; set; }
        public virtual RoleModel role { get; set; }
    }
}
