﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_BlogServices.Models
{
    public class CommentModel
    {
        [Key]
        public long Id { get; set; }
        public string contentComment { get; set; }
        public DateTime dateCreate { get; set; }
        public long postId { get; set; }
        public virtual PostModel post { get; set; }
        public int guestId { get; set; }
        public virtual GuestModel guest { get; set; }
    }
}
