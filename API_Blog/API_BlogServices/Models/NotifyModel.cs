﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_BlogServices.Models
{
    public class NotifyModel
    {
        [Key]
        public long Id { get; set; }
        public string notifyTitle { get; set; }
        public string contentNotify { get; set; }
        public DateTime dateCreate { get; set; }
        public int userId { get; set; }
        public virtual UserModel user { get; set; }
    }
}
