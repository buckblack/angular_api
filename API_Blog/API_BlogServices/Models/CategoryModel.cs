﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_BlogServices.Models
{
    public class CategoryModel
    {
        [Key]
        public int Id { get; set; }
        public string cateName { get; set; }
        public ICollection<SubCategoryModel> subCategories { get; set; }
    }
}
