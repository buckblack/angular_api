﻿
namespace API_BlogServices.Models.View
{
    public class StatisticalUserView
    {
        public int Id { get; set; }
        public string pathAvatar { get; set; }
        public string fullName { get; set; }
        public int sumPost { get; set; }
    }
}
