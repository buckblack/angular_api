﻿using System;

namespace API_BlogServices.Models.View
{
    public class PostsHotView
    {
        public long Id { get; set; }
        public string postTitle { get; set; }
        public string summaryContent { get; set; }
        public DateTime dateCreate { get; set; }
        public string pathThumbnail { get; set; }
        public int status { get; set; }
        public string subCateName { get; set; }
        public string fullName { get; set; }
    }
}
