﻿
namespace API_BlogServices.Models.View
{
    public class StatisticalCommentView
    {
        public long Id { get; set; }
        public string postTitle { get; set; }
        public int sumComment { get; set; }
    }
}
