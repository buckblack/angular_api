﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_BlogServices.Models.View
{
    public class StatisticalPostView
    {
        public int Id { get; set; }
        public string subCateName { get; set; }
        public string pathThumbnail { get; set; }
        public int categoryId { get; set; }
        public int sumPost { get; set; }
    }
}
