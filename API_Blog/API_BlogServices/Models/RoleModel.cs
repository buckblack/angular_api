﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_BlogServices.Models
{
    public class RoleModel
    {
        [Key]
        public int Id { get; set; }
        public string roleName { get; set; }
        public ICollection<UserModel> users { get; set; }
    }
}
