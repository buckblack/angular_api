﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_BlogServices.Models
{
    public class GuestModel
    {
        [Key]
        public int Id { get; set; }
        public string email { get; set; }
        public string fullName { get; set; }
        public ICollection<CommentModel> comment { get; set; }
    }
}
