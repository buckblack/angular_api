﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API_BlogDao.Migrations
{
    public partial class API_Blog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    cateName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Guests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    email = table.Column<string>(nullable: true),
                    fullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Guests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    roleName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SubCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    subCateName = table.Column<string>(nullable: true),
                    pathThumbnail = table.Column<string>(nullable: true),
                    categoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubCategories_Categories_categoryId",
                        column: x => x.categoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    fullName = table.Column<string>(nullable: true),
                    phoneNumber = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    pathAvatar = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    address = table.Column<string>(nullable: true),
                    password = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    roleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Roles_roleId",
                        column: x => x.roleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Notifies",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    notifyTitle = table.Column<string>(nullable: true),
                    contentNotify = table.Column<string>(nullable: true),
                    dateCreate = table.Column<DateTime>(nullable: false),
                    userId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Notifies_Users_userId",
                        column: x => x.userId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    postTitle = table.Column<string>(nullable: true),
                    summaryContent = table.Column<string>(nullable: true),
                    detailContent = table.Column<string>(nullable: true),
                    dateCreate = table.Column<DateTime>(nullable: false),
                    pathThumbnail = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    subCategoriesId = table.Column<int>(nullable: false),
                    userId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_SubCategories_subCategoriesId",
                        column: x => x.subCategoriesId,
                        principalTable: "SubCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Posts_Users_userId",
                        column: x => x.userId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    contentComment = table.Column<string>(nullable: true),
                    dateCreate = table.Column<DateTime>(nullable: false),
                    postId = table.Column<long>(nullable: false),
                    guestId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Guests_guestId",
                        column: x => x.guestId,
                        principalTable: "Guests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Comments_Posts_postId",
                        column: x => x.postId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "roleName" },
                values: new object[] { 1, "Admin" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "address", "description", "email", "fullName", "password", "pathAvatar", "phoneNumber", "roleId", "status" },
                values: new object[] { 1, null, "no description", "tienhm4@gmail.com", "Hồ Minh Tiến", "AQAAAAEAACcQAAAAEIoWzhl+6HqGqLenf45bXCcmbZUFI7wgakcSTN5iM5hKd+m946of9XUE/2CjUoCCkQ==", "/users/person_4.jpg", "0969638980", 1, 0 });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_guestId",
                table: "Comments",
                column: "guestId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_postId",
                table: "Comments",
                column: "postId");

            migrationBuilder.CreateIndex(
                name: "IX_Guests_email",
                table: "Guests",
                column: "email",
                unique: true,
                filter: "[email] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Notifies_userId",
                table: "Notifies",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_subCategoriesId",
                table: "Posts",
                column: "subCategoriesId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_userId",
                table: "Posts",
                column: "userId");

            migrationBuilder.CreateIndex(
                name: "IX_SubCategories_categoryId",
                table: "SubCategories",
                column: "categoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_email",
                table: "Users",
                column: "email",
                unique: true,
                filter: "[email] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Users_roleId",
                table: "Users",
                column: "roleId");

            migrationBuilder.Sql(@"create view PostsHotViews as
                select hp.Id, hp.postTitle,hp.summaryContent,hp.dateCreate,hp.status,hp.pathThumbnail,u.fullName,s.subCateName from Posts hp
                inner join
                (select top 20 p.Id,count(c.postId) as sumcomments from dbo.Posts p
                left join dbo.Comments c
                on c.postId = p.Id
                where p.status=0
                group by c.postId,p.Id
                order by sumcomments desc) v1
                on v1.Id=hp.Id
                inner join dbo.SubCategories s
                on s.Id = hp.subCategoriesId
                inner join dbo.Users u
                on u.id = hp.userId");
            //add statistical post
            migrationBuilder.Sql(@"CREATE PROCEDURE StatisticalPost @dateFrom nvarchar(10), @dateTo nvarchar(10)
                as
                select * from SubCategories s,
                (select p.subCategoriesId,count(*) as sumPost from Posts p
                where p.dateCreate>=CONVERT(datetime,@dateFrom)
                AND p.dateCreate<CONVERT(datetime,@dateTo)
                group by p.subCategoriesId) as tmp
                where tmp.subCategoriesId=s.Id");
            //add statistical comment
            migrationBuilder.Sql(@"CREATE PROCEDURE StatisticalComment @dateFrom nvarchar(10), @dateTo nvarchar(10)
                as
                select hp.Id, hp.postTitle, sumComment from Posts hp
                inner join
                (select top 20 p.Id,count(c.postId) as sumComment from dbo.Posts p
                left join dbo.Comments c
                on c.postId = p.Id
                where p.status=0
                AND c.dateCreate>=CONVERT(datetime,@dateFrom)
                AND c.dateCreate<CONVERT(datetime,@dateTo)
                group by c.postId,p.Id
                order by sumComment desc) v1
                on v1.Id=hp.Id");
            //add statistical comment
            migrationBuilder.Sql(@"CREATE PROCEDURE StatisticalUser @dateFrom nvarchar(10), @dateTo nvarchar(10)
                as
                select top 20 u.Id,u.pathAvatar,u.fullName,
	                (case 
		                when tmp.sumPost is NULL then 0
		                else tmp.sumPost
	                end) as sumPost
                from Users u
                left join
                (select top 20 p.userId, count(*) as sumPost from Posts p
                where p.status=0
                AND p.dateCreate>=CONVERT(datetime,@dateFrom)
                AND p.dateCreate<CONVERT(datetime,@dateTo)
                group by p.userId
                order by sumPost desc) as tmp
                on u.Id = tmp.userId
                order by tmp.sumPost desc");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Notifies");

            migrationBuilder.DropTable(
                name: "Guests");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "SubCategories");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.Sql(@"DROP VIEW PostsHotViews");
            migrationBuilder.Sql(@"DROP PROCEDURE StatisticalPost");
            migrationBuilder.Sql(@"DROP PROCEDURE StatisticalComment");
            migrationBuilder.Sql(@"DROP PROCEDURE StatisticalUser");

        }
    }
}
