﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API_BlogDao.Models
{
    public class PostEntity
    {
        [Key]
        public long Id { get; set; }
        public string postTitle { get; set; }
        public string summaryContent { get; set; }
        public string detailContent { get; set; }
        public DateTime dateCreate { get; set; }
        public string pathThumbnail { get; set; }
        public int status { get; set; }
        public ICollection<CommentEntity> comments { get; set; }
        public int subCategoriesId { get; set; }
        public virtual SubCategoryEntity subCategories { get; set; }
        public int userId { get; set; }
        public virtual UserEntity user { get; set; }
    }
}
