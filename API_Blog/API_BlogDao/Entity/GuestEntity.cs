﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API_BlogDao.Models
{
    public class GuestEntity
    {
        [Key]
        public int Id { get; set; }
        public string email { get; set; }
        public string fullName { get; set; }
        public ICollection<CommentEntity> comment { get; set; }
    }
}
