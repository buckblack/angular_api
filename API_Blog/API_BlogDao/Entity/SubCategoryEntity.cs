﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API_BlogDao.Models
{
    public class SubCategoryEntity
    {
        [Key]
        public int Id { get; set; }
        public string subCateName { get; set; }
        public string pathThumbnail { get; set; }
        public int categoryId { get; set; }
        public virtual CategoryEntity category { get; set; } // virtual keyword is lazy loading
        public ICollection<PostEntity> posts { get; set; }
        [NotMapped]
        public long countPost { get; set; }
    }
}
