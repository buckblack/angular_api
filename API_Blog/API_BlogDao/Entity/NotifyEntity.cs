﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API_BlogDao.Models
{
    public class NotifyEntity
    {
        [Key]
        public long Id { get; set; }
        public string notifyTitle { get; set; }
        public string contentNotify { get; set; }
        public DateTime dateCreate { get; set; }
        public int userId { get; set; }
        public virtual UserEntity user { get; set; }
    }
}
