﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API_BlogDao.Models
{
    public class CategoryEntity
    {
        [Key]
        public int Id { get; set; }
        public string cateName { get; set; }
        public ICollection<SubCategoryEntity> subCategories { get; set; }
    }
}
