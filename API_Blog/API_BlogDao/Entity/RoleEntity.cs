﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API_BlogDao.Models
{
    public class RoleEntity
    {
        [Key]
        public int Id { get; set; }
        public string roleName { get; set; }
        public ICollection<UserEntity> users { get; set; }
    }
}
