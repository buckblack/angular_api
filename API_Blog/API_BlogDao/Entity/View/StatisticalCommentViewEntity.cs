﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_BlogDao.View
{
    public class StatisticalCommentViewEntity
    {
        public long Id { get; set; }
        public string postTitle { get; set; }
        public int sumComment { get; set; }
    }
}
