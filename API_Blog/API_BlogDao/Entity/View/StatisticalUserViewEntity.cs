﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_BlogDao.View
{
    public class StatisticalUserViewEntity
    {
        public int Id { get; set; }
        public string pathAvatar { get; set; }
        public string fullName { get; set; }
        public int sumPost { get; set; }
    }
}
