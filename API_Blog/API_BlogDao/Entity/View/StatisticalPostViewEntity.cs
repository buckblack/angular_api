﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_BlogDao.View
{
    public class StatisticalPostViewEntity
    {
        public int Id { get; set; }
        public string subCateName { get; set; }
        public string pathThumbnail { get; set; }
        public int categoryId { get; set; }
        public int sumPost { get; set; }
    }
}
