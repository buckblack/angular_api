﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_BlogDao.View
{
    public class PostsHotViewEntity
    {
        [Key]
        public long Id { get; set; }
        public string postTitle { get; set; }
        public string summaryContent { get; set; }
        public DateTime dateCreate { get; set; }
        public string pathThumbnail { get; set; }
        public int status { get; set; }
        public string subCateName { get; set; }
        public string fullName { get; set; }
    }
}
