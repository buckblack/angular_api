﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API_BlogDao.Models
{
    public class UserEntity
    {
        [Key]
        public int Id { get; set; }
        public string fullName { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; }
        public string pathAvatar { get; set; }
        public string description { get; set; }
        public string address { get; set; }
        public string password { get; set; }
        public int status { get; set; }
        public ICollection<PostEntity> posts { get; set; }
        public ICollection<NotifyEntity> notifies { get; set; }
        public int roleId { get; set; }
        public virtual RoleEntity role { get; set; }
    }
}
