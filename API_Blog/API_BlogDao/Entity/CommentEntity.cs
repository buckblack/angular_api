﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API_BlogDao.Models
{
    public class CommentEntity
    {
        [Key]
        public long Id { get; set; }
        public string contentComment { get; set; }
        public DateTime dateCreate { get; set; }
        public long postId { get; set; }
        public virtual PostEntity post { get; set; }
        public int guestId { get; set; }
        public virtual GuestEntity guest { get; set; }
    }
}
