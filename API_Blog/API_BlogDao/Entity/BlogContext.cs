﻿using API_BlogDao.View;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace API_BlogDao.Models
{
    public class BlogContext: DbContext
    {
        public static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder =>
        {
            builder
                .AddFilter((category, level) =>
                    category == DbLoggerCategory.Database.Command.Name
                    && level == LogLevel.Information)
                .AddFile("Logs/Info/LogInfo-{Date}.txt", minimumLevel: LogLevel.Information)
            .AddFile("Logs/Error/LogError-{Date}.txt", minimumLevel: LogLevel.Error);
        });
        public IConfiguration configuration;
        public BlogContext(DbContextOptions<BlogContext> options) : base(options)
        {
        }
        public BlogContext(DbContextOptions<BlogContext> options, IConfiguration config) : base(options)
        {
            configuration = config;
        }
        public DbSet<CategoryEntity> Categories { get; set; }
        public DbSet<CommentEntity> Comments { get; set; }
        public DbSet<GuestEntity> Guests { get; set; }
        public DbSet<NotifyEntity> Notifies { get; set; }
        public DbSet<PostEntity> Posts { get; set; }
        public DbSet<SubCategoryEntity> SubCategories { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<RoleEntity> Roles { get; set; }
        public DbSet<PostsHotViewEntity> PostsHotViews { get; set; }
        public DbSet<StatisticalPostViewEntity> StatisticalPostViews { get; set; }
        public DbSet<StatisticalCommentViewEntity> StatisticalCommentViews { get; set; }
        public DbSet<StatisticalUserViewEntity> StatisticalUserViews { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string server = configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            optionsBuilder.UseLoggerFactory(MyLoggerFactory).EnableSensitiveDataLogging()
                .UseSqlServer(server).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserEntity>().HasIndex(x => x.email).IsUnique();
            modelBuilder.Entity<GuestEntity>().HasIndex(x => x.email).IsUnique();
            modelBuilder.Entity<PostsHotViewEntity>().ToView("PostsHotViews");
            modelBuilder.Entity<StatisticalPostViewEntity>().ToView("StatisticalPostViews");
            modelBuilder.Entity<StatisticalCommentViewEntity>().ToView("StatisticalCommentViews");
            modelBuilder.Entity<StatisticalUserViewEntity>().ToView("StatisticalUserViews");
            modelBuilder.Entity<RoleEntity>()
                .HasData(new RoleEntity() { Id = 1, roleName = "Admin" });
            modelBuilder.Entity<UserEntity>()
                .HasData(
                    new UserEntity()
                    {
                        Id = 1,
                        fullName = "Hồ Minh Tiến",
                        email = "tienhm4@gmail.com",
                        pathAvatar = "/users/person_4.jpg",
                        phoneNumber = "0969638980",
                        description = "no description",
                        roleId = 1,
                        status = 0,
                        password = "AQAAAAEAACcQAAAAEIoWzhl+6HqGqLenf45bXCcmbZUFI7wgakcSTN5iM5hKd+m946of9XUE/2CjUoCCkQ=="
                    }
                );
        }

    }
}
