﻿using API_BlogDao.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public class RoleDao:IRoleDao
    {
        private readonly BlogContext _context;
        public RoleDao(BlogContext context)
        {
            _context = context;
        }

        public async Task<List<RoleEntity>> GetAllRole()
        {
            return await _context.Roles.Include(x => x.users).AsNoTracking().ToListAsync();
        }

        public bool CheckRoleExists(string roleName)
        {
            RoleEntity role = _context.Roles.Where(x => x.roleName == roleName.Trim()).FirstOrDefault();
            if (role != null)
                return true;
            return false;
        }

        public RoleEntity FindRoleById(int roleId)
        {
            return _context.Roles.Find(roleId);
        }

        public async Task<int> CreateRole(RoleEntity entity)
        {
            _context.Roles.Add(entity);
            await _context.SaveChangesAsync();
            return 0;
        }

        public async Task<int> UpdataRole(RoleEntity entity)
        {
            _context.Roles.Update(entity);
            await _context.SaveChangesAsync();
            return 0;
        }

        public async Task<int> RemoveRole(RoleEntity entity)
        {
            _context.Roles.Remove(entity);
            await _context.SaveChangesAsync();
            return 0;
        }
    }
}
