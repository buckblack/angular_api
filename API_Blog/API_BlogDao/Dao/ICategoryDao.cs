﻿using API_BlogDao.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public interface ICategoryDao
    {
        List<CategoryEntity> getAllCategory();
        CategoryEntity FindCategoryById(int cateId);
        Task<List<CategoryEntity>> getAllCategorySync();
        bool CheckExistsCategoryByCateName(string cateName);
        Task<int> UpdateCategory(CategoryEntity entity);
        Task<int> CreateCategory(CategoryEntity entity);
        Task<int> DeleteCategory(CategoryEntity entity);
    }
}
