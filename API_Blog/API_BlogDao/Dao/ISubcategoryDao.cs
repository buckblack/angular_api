﻿using API_BlogDao.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public interface ISubcategoryDao
    {
        List<SubCategoryEntity> GetSubCategoryByCateId(int cateId);
        List<SubCategoryEntity> GetAllSubCategory();
        long CountTotalPostBySubCateId(int cateId);
        List<PostEntity> GetPostBySubCateIdPaging(int cateId, int sumDisplay, int currentPage);
        Task<List<PostEntity>> GetPostBySubCateId(int cateId);
        bool CheckExistsSubCategory(int subCateId);
        Task<List<SubCategoryEntity>> GetSubCategoriesByCateIdSync(int cateId);
        bool CheckExistsSubCateNameInCategory(string subCateName, int cateId);
        bool CheckExistsSubCateNameInCategoryUpdate(string subCateName, int cateId);
        Task<int> CreateSubCategory(SubCategoryEntity entity);
        Task<int> UpdateSubCategory(SubCategoryEntity entity);
        Task<int> DeleteSubCategory(SubCategoryEntity entity);
        SubCategoryEntity FindSubCategoryById(int subCateId);
    }
}
