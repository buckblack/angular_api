﻿using API_BlogDao.Models;
using API_BlogDao.View;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public interface IPostDao
    {
        List<PostsHotViewEntity> GetPostsHot(int records);
        PostEntity FindPostById(long postId);
        long CountPostsRelatedRecord(int subCateId, long postId);
        List<PostEntity> GetPostsRelatedPaging(int subCateId, long postId, int currentPage, int sumDisplay);
        long CountAllPostEnable();
        long CountAllPost();
        List<PostEntity> GetPostsRecentPaging(int currentPage, int sumDisplay);
        List<PostEntity> GetPostsRecentComment(int recordDisplay);
        PostEntity GetPostDetail(long postId);
        long CountSearch(string searchKey);
        List<PostEntity> Search(string searchKey, int currentPage, int sumDisplay);
        List<PostEntity> SortSearch(string key);
        List<PostEntity> SortSearchByAuthor(string key);
        List<PostEntity> SortSearchByTitle(string key);
        List<PostEntity> SortSearchByContent(string key);
        Task<List<PostsHotViewEntity>> GetPostsHotDashBoard(int records);
        Task<List<PostEntity>> GetPostsNewDashBoard(int records);
        Task<List<PostEntity>> SearchPostById(long postId);
        Task<int> CreatePost(PostEntity entity);
        Task<int> UpdatePost(PostEntity entity);
        Task<List<PostEntity>> SearchPostsComment(string key);
        long CoutPostBySubCateID(int subCateId);
        List<PostEntity> GetPostBySubCategoryIdPaging(int subCateId, int currentPage, int sumDisplay);

    }
}
