﻿using API_BlogDao.Models;
using API_BlogDao.View;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace API_BlogDao.Dao
{
    public class StatisticalDao:IStatisticalDao
    {
        private readonly BlogContext _context;
        public StatisticalDao(BlogContext context)
        {
            _context = context;
        }

        public List<StatisticalPostViewEntity> GetPostChartByTime(string dateFrom, string dateTo)
        {
            return _context.StatisticalPostViews.FromSqlRaw("EXEC StatisticalPost @dateFrom, @dateTo",
                new SqlParameter("@dateFrom", dateFrom), new SqlParameter("@dateTo", dateTo))
                .ToList();
        }

        public List<StatisticalPostViewEntity> GetPostChartAllTime()
        {
            return _context.SubCategories
                .Include(x => x.posts)
                .Select(x => new StatisticalPostViewEntity
                {
                    Id = x.Id,
                    subCateName = x.subCateName,
                    sumPost = x.posts != null ? x.posts.Count() : 0,
                }).AsNoTracking().ToList();
        }

        public List<StatisticalCommentViewEntity> GetCommentChartAllTime(int records)
        {
            return _context.Posts
                .Include(x => x.comments).OrderByDescending(x => x.comments.Count).Take(records)
                .Select(x => new StatisticalCommentViewEntity
                {
                    Id = x.Id,
                    postTitle = x.postTitle,
                    sumComment = x.comments != null ? x.comments.Count() : 0,
                }).AsNoTracking().ToList();
        }

        public List<StatisticalCommentViewEntity> GetCommentChartByTime(string dateFrom, string dateTo)
        {
            return _context.StatisticalCommentViews.FromSqlRaw("EXEC StatisticalComment @dateFrom, @dateTo",
                new SqlParameter("@dateFrom", dateFrom), new SqlParameter("@dateTo", dateTo))
                .Select(x => new StatisticalCommentViewEntity
                {
                    Id = x.Id,
                    postTitle = x.postTitle,
                    sumComment = x.sumComment,
                }).ToList();
        }

        public List<StatisticalUserViewEntity> GetUserChartAllTime(int records)
        {
            return _context.Users
                .Include(x => x.posts).OrderByDescending(x => x.posts.Count).Take(records)
                .Select(x => new StatisticalUserViewEntity
                {
                    Id = x.Id,
                    fullName = x.fullName,
                    sumPost = x.posts != null ? x.posts.Count() : 0,
                    pathAvatar = x.pathAvatar,
                }).AsNoTracking().ToList();
        }

        public List<StatisticalUserViewEntity> GetUserChartByTime(string dateFrom, string dateTo)
        {
            return _context.StatisticalUserViews.FromSqlRaw("EXEC StatisticalUser @dateFrom, @dateTo",
                    new SqlParameter("@dateFrom", dateFrom), new SqlParameter("@dateTo", dateTo))
                    .Select(x => new StatisticalUserViewEntity
                    {
                        Id = x.Id,
                        fullName = x.fullName,
                        sumPost = x.sumPost,
                        pathAvatar = x.pathAvatar,
                    }).ToList();
        }
    }
}
