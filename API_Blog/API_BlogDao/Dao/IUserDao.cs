﻿using API_BlogDao.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public interface IUserDao
    {
        UserEntity FindUserByEmail(string email);
        UserEntity FindUserById(int userId);
        void UpdateUser(UserEntity user);
        Task<List<UserEntity>> GetAllUser();
        Task<int> CreateUser(UserEntity entity);
        Task<int> UpdateUserSync(UserEntity entity);
        int CountUserRoleAdmin();
    }
}
