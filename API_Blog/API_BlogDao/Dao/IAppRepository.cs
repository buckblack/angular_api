﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API_BlogDao.Dao
{
    public interface IAppRepository
    {
        ICategoryDao CategoryDao { get; set; }
        ISubcategoryDao SubcategoryDao { get; set; }
        IPostDao PostDao { get; set; }
        ICommentDao CommentDao { get; set; }
        IGuestDao GuestDao { get; set; }
        IUserDao UserDao { get; set; }
        IRoleDao RoleDao { get; set; }
        IStatisticalDao StatisticalDao { get; set; }
        INotifyDao NotifyDao { get; set; }
    }
}
