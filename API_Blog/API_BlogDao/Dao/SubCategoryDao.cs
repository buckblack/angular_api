﻿using API_BlogDao.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public class SubCategoryDao:ISubcategoryDao
    {
        private readonly BlogContext _context;
        public SubCategoryDao(BlogContext context)
        {
            _context = context;
        }

        public List<SubCategoryEntity> GetSubCategoryByCateId(int cateId)
        {
            List<SubCategoryEntity> result = _context.SubCategories.Where(x => x.categoryId == cateId)
            .Select(x => new SubCategoryEntity
            {
                Id = x.Id,
                pathThumbnail = x.pathThumbnail,
                subCateName = x.subCateName,
            }).AsNoTracking().ToList();
            return result;
        }

        public List<SubCategoryEntity> GetAllSubCategory()
        {
            return _context.SubCategories.Include(x => x.posts).AsNoTracking().ToList();
        }

        public long CountTotalPostBySubCateId(int cateId)
        {
            return _context.Posts.Where(x => x.subCategories.categoryId == cateId && x.status == 0).AsNoTracking().Count();
        }

        public List<PostEntity> GetPostBySubCateIdPaging(int cateId, int sumDisplay, int currentPage)
        {
            List<PostEntity> result = _context.Posts.Where(x => x.subCategories.categoryId == cateId && x.status == 0)
                    .Include(x => x.user).Include(x => x.subCategories).OrderByDescending(x => x.dateCreate)
                    .AsNoTracking().Skip((currentPage - 1) * sumDisplay).Take(sumDisplay).ToList();
            return result;
        }

        public async Task<List<PostEntity>> GetPostBySubCateId(int cateId)
        {
            List<PostEntity> result = await _context.Posts.Where(x => x.subCategoriesId == cateId).OrderByDescending(x => x.dateCreate)
                .Include(x => x.user).Include(x => x.subCategories).AsNoTracking().ToListAsync();
            return result;
        }

        public bool CheckExistsSubCategory(int subCateId)
        {
            SubCategoryEntity entity = _context.SubCategories.Find(subCateId);
            if (entity != null)
                return true;
            return false;
        }

        public async Task<List<SubCategoryEntity>> GetSubCategoriesByCateIdSync(int cateId)
        {
            List<SubCategoryEntity> result = await _context.SubCategories.Where(x => x.categoryId == cateId)
                .AsNoTracking().ToListAsync();
            return result;
        }

        public bool CheckExistsSubCateNameInCategory(string subCateName, int cateId)
        {
            SubCategoryEntity entity = _context.SubCategories.Where(x => x.subCateName == subCateName && x.categoryId == cateId).FirstOrDefault();
            if (entity != null)
                return true;
            return false;
        }

        public bool CheckExistsSubCateNameInCategoryUpdate(string subCateName, int cateId)
        {
            SubCategoryEntity entity = _context.SubCategories.Where(x => x.subCateName == subCateName && x.categoryId == cateId && x.categoryId != cateId).FirstOrDefault();
            if(entity != null)
                return true;
            return false;
        }

        public async Task<int> CreateSubCategory(SubCategoryEntity entity)
        {
            _context.SubCategories.Add(entity);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> UpdateSubCategory(SubCategoryEntity entity)
        {
            _context.SubCategories.Update(entity);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteSubCategory(SubCategoryEntity entity)
        {
            _context.SubCategories.Remove(entity);
            return await _context.SaveChangesAsync();
        }

        public SubCategoryEntity FindSubCategoryById(int subCateId)
        {
            return _context.SubCategories.Include(x => x.posts).Include(x => x.category).Where(x => x.Id == subCateId).FirstOrDefault();
        }
    }
}
