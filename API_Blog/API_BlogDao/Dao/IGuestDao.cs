﻿using API_BlogCore.Requests;
using API_BlogDao.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public interface IGuestDao
    {
        GuestEntity FindGuestByEmail(string email);
        void CreateGuest(CommentRequest request);
        void UpdateGuest(CommentRequest request, int guestId);
        Task<List<GuestEntity>> GetAllGuest(); 
    }
}
