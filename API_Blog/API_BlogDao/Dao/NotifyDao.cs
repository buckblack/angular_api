﻿using API_BlogDao.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public class NotifyDao: INotifyDao
    {
        private readonly BlogContext _context;
        public NotifyDao(BlogContext context)
        {
            _context = context;
        }

        public async Task<int> CreateNotify(NotifyEntity entity)
        {
            _context.Notifies.Add(entity);
            return await _context.SaveChangesAsync();
        }

        public async Task<NotifyEntity> FindNotifyByID(long id)
        {
            return await _context.Notifies.Include(x => x.user).Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<int> DeleteNotify(NotifyEntity entity)
        {
            _context.Notifies.Remove(entity);
            return await _context.SaveChangesAsync();
        }

        public async Task<List<NotifyEntity>> GetNotify(int records=0)
        {
            if(records == 0)
            {
                return await _context.Notifies.Include(x => x.user)
                    .OrderByDescending(x => x.dateCreate).AsNoTracking().ToListAsync();
            }
            //notifu nav
            return await _context.Notifies.Include(x => x.user)
                .OrderByDescending(x => x.dateCreate).Take(records).AsNoTracking().ToListAsync();
        }
    }
}
