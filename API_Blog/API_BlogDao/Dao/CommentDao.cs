﻿using API_BlogCore.Requests;
using API_BlogDao.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public class CommentDao:ICommentDao
    {
        private readonly BlogContext _context;
        public CommentDao(BlogContext context)
        {
            _context = context;
        }

        public List<CommentEntity> GetCommentByPostId(long postId)
        {
            return _context.Comments.Where(x => x.postId == postId).OrderByDescending(x => x.dateCreate)
                .Include(x => x.guest).AsNoTracking().ToList();
        }

        public void CreateComment(CommentRequest request, int guestId)
        {
            CommentEntity entity = new CommentEntity() { contentComment = request.contentComment, dateCreate = DateTime.Now, guestId = guestId, postId = request.postId };
            _context.Comments.Add(entity);
            _context.SaveChanges();
        }

        public async Task<List<CommentEntity>> GetNewCommentDashboard(int records)
        {
            return await _context.Comments.Take(records).OrderByDescending(x => x.dateCreate).Include(x => x.guest).ToListAsync();
        }

        public async Task<List<CommentEntity>> FindCommentByPostId(long postId)
        {
            return await _context.Comments.Where(x => x.postId == postId)
                    .Include(x => x.guest).AsNoTracking().ToListAsync();
        }

        public CommentEntity FindCommentById(long commentId)
        {
            return _context.Comments.Find(commentId);
        }

        public async Task<int> DeleteComment(CommentEntity entity)
        {
            _context.Comments.Remove(entity);
            return await _context.SaveChangesAsync();
        }
    }
}
