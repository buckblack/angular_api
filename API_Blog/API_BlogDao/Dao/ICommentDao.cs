﻿using API_BlogCore.Requests;
using API_BlogDao.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public interface ICommentDao
    {
        List<CommentEntity> GetCommentByPostId(long postId);
        void CreateComment(CommentRequest request, int guestId);
        Task<List<CommentEntity>> GetNewCommentDashboard(int records);
        Task<List<CommentEntity>> FindCommentByPostId(long postId);
        CommentEntity FindCommentById(long commentId);
        Task<int> DeleteComment(CommentEntity entity);
    }
}
