﻿using API_BlogDao.Dao;
using API_BlogDao.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public class CategoryDao: ICategoryDao
    {
        private readonly BlogContext _context;
        public CategoryDao(BlogContext context)
        {
            _context = context;
        }

        public List<CategoryEntity> getAllCategory()
        {
            List<CategoryEntity> result = _context.Categories.AsNoTracking().ToList();
            return result;
        }

        public async Task<List<CategoryEntity>> getAllCategorySync()
        {
            List<CategoryEntity> result = await _context.Categories.Include(x => x.subCategories).AsNoTracking().ToListAsync();
            return result;
        }

        public CategoryEntity FindCategoryById(int cateId)
        {
            return _context.Categories.Include(x => x.subCategories).Where(x => x.Id == cateId).FirstOrDefault();
        }

        public bool CheckExistsCategoryByCateName(string cateName)
        {
            CategoryEntity entity = _context.Categories.Where(x => x.cateName == cateName).FirstOrDefault();
            if (entity != null)
                return true;
            return false;
        }

        public async Task<int> CreateCategory(CategoryEntity entity)
        {
            _context.Categories.Add(entity);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> UpdateCategory(CategoryEntity entity)
        {
            //_context.Entry<CategoryEntity>(entity).State = EntityState.Modified;
            _context.Categories.Update(entity);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteCategory(CategoryEntity entity)
        {
            _context.Categories.Remove(entity);
            return await _context.SaveChangesAsync();
        }

        public bool CheckExistsCategoryByName(string cateName)
        {
            CategoryEntity entity = _context.Categories.Where(x => x.cateName == cateName).FirstOrDefault();
            if (entity != null)
                return true;
            return false;
        }

    }
}
