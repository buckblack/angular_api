﻿using API_BlogDao.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public interface IRoleDao
    {
        Task<List<RoleEntity>> GetAllRole();
        bool CheckRoleExists(string roleName);
        RoleEntity FindRoleById(int roleId);
        Task<int> CreateRole(RoleEntity entity);
        Task<int> UpdataRole(RoleEntity entity);
        Task<int> RemoveRole(RoleEntity entity);
    }
}
