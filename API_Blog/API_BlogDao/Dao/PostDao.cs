﻿using API_BlogDao.Models;
using API_BlogDao.View;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public class PostDao:IPostDao
    {
        private readonly BlogContext _context;
        public PostDao(BlogContext context)
        {
            _context = context;
        }

        public List<PostsHotViewEntity> GetPostsHot(int records)
        {
            return _context.PostsHotViews.Where(x => x.status == 0).Take(records).AsNoTracking().ToList();
        }

        public PostEntity FindPostById(long postId)
        {
            return _context.Posts.Find(postId);
        }

        public long CountPostsRelatedRecord(int subCateId, long postId)
        {
            return _context.Posts.Where(x => x.subCategoriesId == subCateId && x.Id != postId && x.status == 0).AsNoTracking().Count();
        }

        public List<PostEntity> GetPostsRelatedPaging(int subCateId, long postId, int currentPage, int sumDisplay)
        {
            return _context.Posts.Where(x => x.subCategoriesId == subCateId && x.Id != postId && x.status == 0)
                .OrderByDescending(x => x.dateCreate).Skip((currentPage - 1) * sumDisplay).Take(sumDisplay)
                .Include(x => x.subCategories).Include(x => x.user).AsNoTracking().ToList();
        }

        public long CountAllPostEnable()
        {
            return _context.Posts.Where(x => x.status == 0).Count();
        }

        public long CountAllPost()
        {
            return _context.Posts.Count();
        }

        public List<PostEntity> GetPostsRecentPaging(int currentPage, int sumDisplay)
        {
            return _context.Posts.Where(x => x.status == 0).Include(x => x.subCategories).Include(x => x.user)
                .OrderByDescending(x => x.dateCreate).Skip((currentPage - 1) * sumDisplay).Take(sumDisplay)
                .AsNoTracking().ToList();
        }

        public List<PostEntity> GetPostsRecentComment(int recordDisplay)
        {
            return _context.Posts.OrderByDescending(x => x.comments.OrderByDescending(o => o.dateCreate).FirstOrDefault())
                .Take(recordDisplay).Where(x => x.status == 0).Include(x => x.subCategories).Include(x => x.user)
                .AsNoTracking().ToList();
        }
        public PostEntity GetPostDetail(long postId)
        {
            return _context.Posts.Where(x => x.Id == postId && x.status == 0)
                .Include(x => x.user).Include(x => x.subCategories).Include(x => x.comments)
                .AsNoTracking().FirstOrDefault();
        }

        public long CountSearch(string searchKey)
        {
            return _context.Posts.Where(x => x.postTitle.Contains(searchKey) || x.summaryContent.Contains(searchKey) || x.detailContent.Contains(searchKey))
                .AsNoTracking().Count();
        }
         
        public List<PostEntity> Search(string searchKey, int currentPage, int sumDisplay)
        {
            return _context.Posts.Where(x => x.postTitle.Contains(searchKey) || x.summaryContent.Contains(searchKey) || x.detailContent.Contains(searchKey))
                .OrderByDescending(x => x.dateCreate).Skip((currentPage - 1) * sumDisplay).Take(sumDisplay)
                .Include(x => x.user).Include(x => x.subCategories)
                .AsNoTracking().ToList();
        }

        public List<PostEntity> SortSearch(string key)
        {
            return _context.Posts.Include(x => x.user).Include(x => x.subCategories)
                .Where(x => x.postTitle.ToUpper().Contains(key.ToUpper())
                    || x.summaryContent.ToUpper().Contains(key.ToUpper())
                    || x.detailContent.ToUpper().Contains(key.ToUpper())).ToList();
        }

        public List<PostEntity> SortSearchByAuthor(string key)
        {
            return _context.Posts.Include(x => x.user).Include(x => x.subCategories)
               .Where(x => x.user.fullName.ToUpper().Contains(key.ToUpper())).ToList();
        }

        public List<PostEntity> SortSearchByTitle(string key)
        {
            return _context.Posts.Include(x => x.user).Include(x => x.subCategories)
               .Where(x => x.postTitle.ToUpper().Contains(key.ToUpper())).ToList();
        }

        public List<PostEntity> SortSearchByContent(string key)
        {
            return _context.Posts.Include(x => x.user).Include(x => x.subCategories)
               .Where(x => x.detailContent.ToUpper().Contains(key.ToUpper())).ToList();
        }

        public async Task<List<PostEntity>> GetPostsNewDashBoard(int records)
        {
            return await _context.Posts.Where(x => x.status == 0).OrderByDescending(x => x.dateCreate).Take(records)
                .Include(x => x.user).Include(x => x.subCategories).AsNoTracking().ToListAsync();
        }

        public async Task<List<PostsHotViewEntity>> GetPostsHotDashBoard(int records)
        {
            return await _context.PostsHotViews.Where(x => x.status == 0).Take(records).Select(x => new PostsHotViewEntity
            {
                dateCreate = x.dateCreate,
                Id = x.Id,
                postTitle = x.postTitle,
                fullName = x.fullName,
                subCateName = x.subCateName,
            }).AsNoTracking().ToListAsync();
        }

        public async Task<List<PostEntity>> SearchPostById(long postId)
        {
            return await _context.Posts.Where(x => x.Id.ToString().Contains(postId.ToString()))
                .OrderByDescending(x => x.dateCreate).AsNoTracking().ToListAsync();
        }

        public async Task<int> CreatePost(PostEntity entity)
        {
            _context.Posts.Add(entity);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> UpdatePost(PostEntity entity)
        {
            _context.Posts.Update(entity);
            return await _context.SaveChangesAsync();
        }

        public async Task<List<PostEntity>> SearchPostsComment(string key)
        {
            return await _context.Posts.Where(x => x.Id.ToString().Contains(key) || x.postTitle.Contains(key))
                    .Include(x => x.comments).AsNoTracking().ToListAsync();
        }

        public long CoutPostBySubCateID(int subCateId)
        {
            return _context.Posts.Where(x => x.subCategoriesId == subCateId && x.status == 0).AsNoTracking().Count();
        }

        public List<PostEntity> GetPostBySubCategoryIdPaging(int subCateId, int currentPage, int sumDisplay)
        {
            return _context.Posts.Where(x => x.subCategoriesId == subCateId && x.status == 0)
                .Include(x => x.user).Include(x => x.subCategories)
                .OrderByDescending(x => x.dateCreate).AsNoTracking().Skip((currentPage - 1) * sumDisplay).Take(sumDisplay).ToList();
        }
    }
}
