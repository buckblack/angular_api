﻿using API_BlogCore.Requests;
using API_BlogDao.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public class GuestDao:IGuestDao
    {
        private readonly BlogContext _context;
        public GuestDao(BlogContext context)
        {
            _context = context;
        }

        public async Task<List<GuestEntity>> GetAllGuest()
        {
            return await _context.Guests.Include(x => x.comment).AsNoTracking().ToListAsync();
        }

        public GuestEntity FindGuestByEmail(string email)
        {
            return _context.Guests.Where(x => x.email == email).FirstOrDefault();
        }

        public void CreateGuest(CommentRequest request)
        {
            _context.Guests.Add(new GuestEntity() { email = request.guestEmail.Trim(), fullName = request.guestName });
            _context.SaveChanges();
        }

        public void UpdateGuest(CommentRequest request, int guestId)
        {
            GuestEntity entity = new GuestEntity() { Id = guestId, email = request.guestEmail.Trim(), fullName = request.guestName };
            _context.Guests.Update(entity);
            _context.SaveChanges();
        }
    }
}
