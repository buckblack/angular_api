﻿using API_BlogDao.Models;

namespace API_BlogDao.Dao
{
    public class AppRepository: IAppRepository
    {
        public readonly BlogContext _context;
        public ICategoryDao CategoryDao { get; set; }
        public ISubcategoryDao SubcategoryDao { get; set; }
        public IPostDao PostDao { get; set; }
        public ICommentDao CommentDao { get; set; }
        public IGuestDao GuestDao { get; set; }
        public IUserDao UserDao { get; set; }
        public IRoleDao RoleDao { get; set; }
        public IStatisticalDao StatisticalDao { get; set; }
        public INotifyDao NotifyDao { get; set; }
        public AppRepository(BlogContext context, 
            ICategoryDao category,
            ISubcategoryDao subcategory,
            IPostDao post,
            ICommentDao comment,
            IGuestDao guest,
            IUserDao user,
            IRoleDao role,
            IStatisticalDao statistical,
            INotifyDao notify)
        {
            _context = context;
            CategoryDao = category;
            SubcategoryDao = subcategory;
            PostDao = post;
            CommentDao = comment;
            GuestDao = guest;
            UserDao = user;
            RoleDao = role;
            StatisticalDao = statistical;
            NotifyDao = notify;
        }
       
    }
}
