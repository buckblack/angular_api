﻿using API_BlogDao.View;
using System;
using System.Collections.Generic;
using System.Text;

namespace API_BlogDao.Dao
{
    public interface IStatisticalDao
    {
        List<StatisticalPostViewEntity> GetPostChartByTime(string dateFrom, string dateTo);
        List<StatisticalPostViewEntity> GetPostChartAllTime();
        List<StatisticalCommentViewEntity> GetCommentChartAllTime(int records);
        List<StatisticalCommentViewEntity> GetCommentChartByTime(string dateFrom, string dateTo);
        List<StatisticalUserViewEntity> GetUserChartAllTime(int records);
        List<StatisticalUserViewEntity> GetUserChartByTime(string dateFrom, string dateTo);

    }
}
