﻿using API_BlogDao.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public interface INotifyDao
    {
        Task<int> CreateNotify(NotifyEntity entity);
        Task<NotifyEntity> FindNotifyByID(long id);
        Task<int> DeleteNotify(NotifyEntity entity);
        Task<List<NotifyEntity>> GetNotify(int records = 0);
    }
}
