﻿using API_BlogDao.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_BlogDao.Dao
{
    public class UserDao:IUserDao
    {
        private readonly BlogContext _context;
        public UserDao(BlogContext context)
        {
            _context = context;
        }

        public async Task<List<UserEntity>> GetAllUser()
        {
            return await _context.Users.Include(x => x.role).ToListAsync();
        }
        public UserEntity FindUserByEmail(string email)
        {
            return _context.Users.Where(x => x.email == email).Include(x => x.role).FirstOrDefault();
        }

        public UserEntity FindUserById(int userId)
        {
            return _context.Users.Find(userId);
        }

        public void UpdateUser(UserEntity user)
        {
            _context.Users.Update(user);
            _context.SaveChanges();
        }

        public async Task<int> CreateUser(UserEntity entity)
        {
            _context.Users.Add(entity);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> UpdateUserSync(UserEntity entity)
        {
            _context.Users.Update(entity);
            return await _context.SaveChangesAsync();
        }

        public int CountUserRoleAdmin()
        {
            return _context.Users.Include(x => x.role).Where(x => x.role.roleName == "Admin" && x.status == 0).Count();
        }
    }
}
