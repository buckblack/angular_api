﻿using API_Blog.Controllers;
using API_Blog.Models;
using API_Blog.Services.ultils;
using Moq;
using MyBlog_SD3892.Models.Requests;
using NUnit.Framework;

namespace API_BlogTest
{
    class PostControllerTest
    {
        private readonly BlogContext _dbContext;
        private readonly PostController _post;
        private IMyLogger _logger;

        public PostControllerTest()
        {
            _dbContext = new DbContextSetUp().GetBlogContext();
            _logger = new Mock<IMyLogger>().Object;
            _post = new PostController(_dbContext, _logger);
        }

        [Test]
        public void getPostsHot()
        {
            int result = _post.getPostsHot().errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void getPostsRelated()
        {
            //incorrect post id, pagr number
            int result = _post.getPostsRelated(postId: "a", page: "a").errorCode;
            Assert.AreNotEqual(0, result);
            //incorrect post id,correct pagr number
            result = _post.getPostsRelated(postId: "a", page: "1").errorCode;
            Assert.AreNotEqual(0, result);
            //correct post id, incorrect pagr number
            result = _post.getPostsRelated(postId: "2", page: "a").errorCode;
            Assert.AreEqual(0, result);
            //correct post id, pagr number
            result = _post.getPostsRelated(postId: "2", page: "1").errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void getPostsRecent()
        {
            //incorrect page number
            int result = _post.getPostsRecent(page: "a").errorCode;
            Assert.AreEqual(0, result);
            //correct page number
            result = _post.getPostsRecent(page: "1").errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void getPostsRecentComment()
        {
            int result = _post.getPostsRecentComment().errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void getDetail()
        {
            //incorrect post id
            int reult = _post.getDetail(postId: "a").errorCode;
            Assert.AreEqual(0, reult);
            //correct post id
            reult = _post.getDetail(postId: "2").errorCode;
            Assert.AreEqual(0, reult);
        }

        [Test]
        public void getComment()
        {
            //incorrect post id
            int result = _post.getComment(postId: "a").errorCode;
            Assert.AreEqual(0, result);
            //correct post id
            result = _post.getComment(postId: "2").errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void submitComment()
        {
            //null request
            CommentRequest cmt1 = new CommentRequest();
            int result = _post.submitComment(cmt1).errorCode;
            Assert.AreNotEqual(0, result);
            //invalid email
            CommentRequest cmt2 = new CommentRequest() { guestEmail="fdhhf"};
            result = _post.submitComment(cmt2).errorCode;
            Assert.AreNotEqual(0, result);
            //invalid name
            CommentRequest cmt3 = new CommentRequest() { guestEmail = "a@mail.com" };
            result = _post.submitComment(cmt3).errorCode;
            Assert.AreNotEqual(0, result);
            //invalid post id
            CommentRequest cmt4 = new CommentRequest() { guestEmail = "a@mail.com",guestName="user1" };
            result = _post.submitComment(cmt4).errorCode;
            Assert.AreNotEqual(0, result);
            //invalid comment content
            CommentRequest cmt5 = new CommentRequest() { guestEmail = "a@mail.com", guestName = "user1",postId=1 };
            result = _post.submitComment(cmt5).errorCode;
            Assert.AreNotEqual(0, result);
            //valid request
            CommentRequest cmt6 = new CommentRequest() { guestEmail = "a@mail.com", guestName = "user1", postId = 2,contentComment= "1234567890 1234567890 1234567890 1234567890 1234567890 " };
            result = _post.submitComment(cmt6).errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void AddNewSubscriber()
        {
            //invalid email
            CommentRequest cmt1 = new CommentRequest() { guestEmail = "fdhhf" };
            int result = _post.AddNewSubscriber(cmt1).errorCode;
            Assert.AreNotEqual(0, result);
            //valid email
            CommentRequest cmt2 = new CommentRequest() { guestEmail = "a@gmail.com" };
            result = _post.AddNewSubscriber(cmt2).errorCode;
            Assert.AreEqual(0, result);
        }
    }
}
