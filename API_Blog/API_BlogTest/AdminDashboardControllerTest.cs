﻿using API_Blog.Controllers;
using API_Blog.Models;
using API_Blog.Services.ultils;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace API_BlogTest
{
     public class AdminDashboardControllerTest
    {
        private readonly BlogContext _dbContext;
        private readonly AdminDashboardController _adminDashboard;
        private IMyLogger _logger;

        public AdminDashboardControllerTest()
        {
            _dbContext = new DbContextSetUp().GetBlogContext();
            _logger = new Mock<IMyLogger>().Object;
            _adminDashboard = new AdminDashboardController(_dbContext, _logger);
        }

        [Test]
        public void DashBoardChart()
        {
            int result = _adminDashboard.DashBoardChart().Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void DashBoard()
        {
            int result = _adminDashboard.DashBoard().Result.errorCode;
            Assert.AreEqual(0, result);
        }
    }
}
