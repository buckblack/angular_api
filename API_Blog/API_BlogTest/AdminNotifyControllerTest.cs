﻿using API_Blog.Controllers;
using API_Blog.Models;
using API_Blog.Models.Requests;
using API_Blog.Services.ultils;
using Microsoft.AspNetCore.Http;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace API_BlogTest
{
    public class AdminNotifyControllerTest
    {
        private readonly BlogContext _dbContext;
        private readonly AdminNotifyController _adminNotify;
        private IMyLogger _logger;

        public AdminNotifyControllerTest()
        {
            var contextMock = new Mock<HttpContext>();
            contextMock.Setup(x => x.User).Returns(
                new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Role, "Admin"),
                    new Claim("UserId", "1"),
                }))
                );
            _dbContext = new DbContextSetUp().GetBlogContext();
            _logger = new Mock<IMyLogger>().Object;
            _adminNotify = new AdminNotifyController(_dbContext, _logger);
            _adminNotify.ControllerContext.HttpContext = contextMock.Object;
        }

        [Test]
        public void SubmitNotify()
        {
            //invalid request
            NotifyRequest request = new NotifyRequest();
            int result = _adminNotify.SubmitNotify(request).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //valid request
            request.title = "hdf ahguwye u aei i";
            request.content = "wjefh hkuq uqh uw w q qhiug fiqwgf wugfiw fgu wfui fuiw iug";
            result = _adminNotify.SubmitNotify(request).Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetNotifyNav()
        {
            int result = _adminNotify.GetNotifyNav().Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetNotify()
        {
            int result = _adminNotify.GetNotify().Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetNotifyDetail()
        {
            //invalid notify id
            int result = _adminNotify.GetNotifyDetail("a").Result.errorCode;
            Assert.AreNotEqual(0, result);
            //valid notify id
            result = _adminNotify.GetNotifyDetail("1").Result.errorCode;
            Assert.AreEqual(0, result);
        }

        //please add and enter notify id
        //[Test]
        //public void DeleteNotify()
        //{
        //    //invalid notify id
        //    int result = _adminNotify.DeleteNotify("a").Result.errorCode;
        //    Assert.AreNotEqual(0, result);
        //    //valid notify id
        //    result = _adminNotify.DeleteNotify("3").Result.errorCode;
        //    Assert.AreEqual(0, result);
        //}
    }
}
