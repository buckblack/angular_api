﻿using API_Blog.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace API_BlogTest
{
    public class DbContextSetUp
    {
        public static IConfiguration InitConfiguration()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();
            return config;
        }
        public BlogContext GetBlogContext()
        {
            var config = InitConfiguration();
            var options = new DbContextOptionsBuilder<BlogContext>()
                            .UseSqlServer(config["ConnectionStrings:TestingConnection"])
                            .Options;
            var dbContext = new BlogContext(options);

            return dbContext;
        }
    }
}
