using API_Blog.Controllers;
using API_Blog.Models;
using API_Blog.Services.ultils;
using Moq;
using NUnit.Framework;

namespace API_BlogTest
{
    public class CategoryControllerTest
    {
        private readonly BlogContext _dbContext;
        private readonly CategoryController _category;
        private IMyLogger _logger;

        public CategoryControllerTest()
        {
            _dbContext = new DbContextSetUp().GetBlogContext();
            _logger = new Mock<IMyLogger>().Object;
            _category = new CategoryController(_dbContext,_logger);
        }

        [Test]
        public void GetAll()
        {
            int result = _category.getAll().errorCode;
            Assert.AreEqual(0, result);
        }

        //true category id
        [Test]
        public void getAllSubCategoryTrue()
        {
            int result = _category.getAllSubCategory("a").errorCode;
            Assert.AreEqual(0, result);
        }

        //false category id
        [Test]
        public void getAllSubCategoryFalse()
        {
            int result = _category.getAllSubCategory("1").errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void getPostsByCateId()
        {
            //incorrect category id, page number
            int result = _category.getPostsByCateId(cateId: "a", page: "a").errorCode;
            Assert.AreEqual(0, result);
            //correct category id, page number
            result =_category.getPostsByCateId(cateId: "1", page: "1").errorCode;
            Assert.AreEqual(0, result);
            //incorrect category id, correct page number
            result = _category.getPostsByCateId(cateId: "a", page: "1").errorCode;
            Assert.AreEqual(0, result);
            //correct category id,incorrect page number
            result = _category.getPostsByCateId(cateId: "1", page: "a").errorCode;
            Assert.AreEqual(0, result);
        }
    }
}