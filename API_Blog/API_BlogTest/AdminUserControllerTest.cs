﻿using API_Blog.Controllers;
using API_Blog.Models;
using API_Blog.Models.Requests;
using API_Blog.Services.ultils;
using Microsoft.AspNetCore.Http;
using Moq;
using MyBlog_SD3892.Models.Requests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace API_BlogTest
{
    public class AdminUserControllerTest
    {
        private readonly BlogContext _dbContext;
        private readonly AdminUserController _adminUser;
        private IMyLogger _logger;

        public AdminUserControllerTest()
        {
            _dbContext = new DbContextSetUp().GetBlogContext();
            _logger = new Mock<IMyLogger>().Object;
            _adminUser = new AdminUserController(_dbContext, _logger);
        }

        [Test]
        public void GetAllRole()
        {
            int result = _adminUser.GetAllRole().Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void AddNewRole()
        {
            //invalid role
            RoleRequest role1 = new RoleRequest();
            int result = _adminUser.AddNewRole(role1).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //duplicate role name
            RoleRequest role2 = new RoleRequest() { roleName="Admin"};
            result = _adminUser.AddNewRole(role2).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //valid role
            RoleRequest role3 = new RoleRequest() { roleName = "Test" + DateTime.Now.ToString() };
            result = _adminUser.AddNewRole(role3).Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void EditRole()
        {
            //invalid role
            RoleRequest role1 = new RoleRequest();
            int result = _adminUser.EditRole(role1).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //invalid role id
            RoleRequest role2 = new RoleRequest() { roleId = "a" };
            result = _adminUser.EditRole(role2).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //cannot rename role admin
            RoleRequest role3 = new RoleRequest() { roleId = "1" };
            result = _adminUser.EditRole(role3).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //valid role
            RoleRequest role4 = new RoleRequest() { roleId = "3", roleName = "testEdit" + DateTime.Now.ToString() };
            result = _adminUser.EditRole(role4).Result.errorCode;
            Assert.AreEqual(0, result);
        }

        //please add role 4 before remove
        //[Test]
        //public void RemoveRole()
        //{
        //    //invalid role id
        //    int result = _adminUser.RemoveRole("a").Result.errorCode;
        //    Assert.AreNotEqual(0, result);
        //    //cannot delete role admin
        //    result = _adminUser.RemoveRole("1").Result.errorCode;
        //    Assert.AreNotEqual(0, result);
        //    //valid role id
        //    result = _adminUser.RemoveRole("4").Result.errorCode;
        //    Assert.AreEqual(0, result);
        //}

        [Test]
        public void getAllUser()
        {
            int result = _adminUser.getAllUser().Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void AddNewUser()
        {
            //invalid user
            UserRequest user1 = new UserRequest();
            int result = _adminUser.AddNewUser(user1).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //user exists
            UserRequest user2 = new UserRequest()
            {
                userEmail = "tienhm4@gmail.com",
                userAddress = "asdfg",
                userFullName = "asdfg",
                userDescription = "asdfg",
                userPassword = "123456",
                userPhone = "1234567890",
                userRoleId = "0"
            };
            result = _adminUser.AddNewUser(user2).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //invalid role id
            user2.userRoleId = "0";
            result = _adminUser.AddNewUser(user2).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //avatar null
            user2.userRoleId = "1";
            result = _adminUser.AddNewUser(user2).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //valid role
            user2.userEmail = DateTime.Now.ToString() + "@gmail.com";
            user2.userPathAvatar=new FormFile(new MemoryStream(Encoding.UTF8.GetBytes("dummy image")), 0, 0, "Data", "image.png");
            result = _adminUser.AddNewUser(user2).Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void UpdateUser()
        {
            //invalid user
            UserRequest user1 = new UserRequest();
            int result = _adminUser.UpdateUser(user1).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //user exists
            UserRequest user2 = new UserRequest()
            {
                userEmail = "tienhm4@gmail.com",
                userAddress = "asdfg",
                userFullName = "asdfg",
                userDescription = "asdfg",
                userPassword = "123456",
                userPhone = "1234567890",
                userRoleId = "0",
                userId="2",
            };
            result = _adminUser.UpdateUser(user2).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //invalid role id
            user2.userRoleId = "0";
            result = _adminUser.UpdateUser(user2).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //valid role, avatar null
            user2.userRoleId = "1";
            user2.userEmail = DateTime.Now.ToString() + "@gmail.com";
            result = _adminUser.UpdateUser(user2).Result.errorCode;
            Assert.AreEqual(0, result);
            //valid role
            user2.userPathAvatar = new FormFile(new MemoryStream(Encoding.UTF8.GetBytes("dummy image")), 0, 0, "Data", "image.png");
            result = _adminUser.UpdateUser(user2).Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void UpdateStatusUser()
        {
            //invalid user
            UserRequest user1 = new UserRequest();
            int result = _adminUser.UpdateStatusUser(user1).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //status 0
            UserRequest user2 = new UserRequest()
            {
                status = "0",
                userId = "2",
            };
            result = _adminUser.UpdateStatusUser(user2).Result.errorCode;
            Assert.AreEqual(0, result);
            //status 1
            user2.userRoleId = "1";
            result = _adminUser.UpdateStatusUser(user2).Result.errorCode;
            Assert.AreEqual(0, result);
            //exists user
            user2.userId = "0";
            result = _adminUser.UpdateStatusUser(user2).Result.errorCode;
            Assert.AreNotEqual(0, result);
        }

        [Test]
        public void getAllGuest()
        {
            int result = _adminUser.getAllGuest().Result.errorCode;
            Assert.AreEqual(0, result);
        }
    }
}
