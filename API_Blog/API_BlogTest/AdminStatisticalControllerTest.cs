﻿using API_Blog.Controllers;
using API_Blog.Models;
using API_Blog.Models.Requests;
using API_Blog.Services.ultils;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace API_BlogTest
{
    public class AdminStatisticalControllerTest
    {
        private readonly BlogContext _dbContext;
        private readonly AdminStatisticalController _adminStatistical;
        private IMyLogger _logger;

        public AdminStatisticalControllerTest()
        {
            _dbContext = new DbContextSetUp().GetBlogContext();
            _logger = new Mock<IMyLogger>().Object;
            _adminStatistical = new AdminStatisticalController(_dbContext, _logger);
        }

        [Test]
        public void GetPostChartAllTime()
        {
            int result = _adminStatistical.GetPostChartAllTime().errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPostChartByTime()
        {
            //invalid request
            StatisticalRequest request = new StatisticalRequest();
            int result = _adminStatistical.GetPostChartByTime(request).errorCode;
            Assert.AreNotEqual(0, result);
            //valid request
            request.dateFrom = DateTime.Now;
            request.dateTo = DateTime.Now;
            result = _adminStatistical.GetPostChartByTime(request).errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetCommentChartAllTime()
        {
            int result = _adminStatistical.GetCommentChartAllTime().errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetCommentChartByTime()
        {
            //invalid request
            StatisticalRequest request = new StatisticalRequest();
            int result = _adminStatistical.GetCommentChartByTime(request).errorCode;
            Assert.AreNotEqual(0, result);
            //valid request
            request.dateFrom = DateTime.Now;
            request.dateTo = DateTime.Now;
            result = _adminStatistical.GetCommentChartByTime(request).errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetUserChartAllTime()
        {
            int result = _adminStatistical.GetUserChartAllTime().errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetUserChartByTime()
        {
            //invalid request
            StatisticalRequest request = new StatisticalRequest();
            int result = _adminStatistical.GetCommentChartByTime(request).errorCode;
            Assert.AreNotEqual(0, result);
            //valid request
            request.dateFrom = DateTime.Now;
            request.dateTo = DateTime.Now;
            result = _adminStatistical.GetCommentChartByTime(request).errorCode;
            Assert.AreEqual(0, result);
        }
    }
}
