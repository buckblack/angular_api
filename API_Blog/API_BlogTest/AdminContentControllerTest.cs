﻿using API_Blog.Controllers;
using API_Blog.Models;
using API_Blog.Services.ultils;
using Microsoft.AspNetCore.Http;
using Moq;
using MyBlog_SD3892.Models.Requests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Text;

namespace API_BlogTest
{
    public class AdminContentControllerTest
    {
        private readonly BlogContext _dbContext;
        private readonly AdminContentController _adminContent;
        private IMyLogger _logger;

        public AdminContentControllerTest()
        {
            _dbContext = new DbContextSetUp().GetBlogContext();
            _logger = new Mock<IMyLogger>().Object;
            _adminContent = new AdminContentController(_dbContext, _logger);
            var contextMock = new Mock<HttpContext>();
            contextMock.Setup(x => x.User).Returns(
                new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Role, "Admin"),
                    new Claim("UserId", "1"),
                }))
                );
            _adminContent.ControllerContext.HttpContext = contextMock.Object;
        }

        [Test]
        public void GetAllCategoryForPost()
        {
            int result = _adminContent.GetAllCategoryForPost().Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetSubCategoryForPost()
        {
            //incorrect category id
            int result = _adminContent.GetSubCategoryForPost(cateid: "a").Result.errorCode;
            Assert.AreEqual(0, result);
            //correct category id
            result = _adminContent.GetSubCategoryForPost(cateid: "1").Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetPostBySubCateId()
        {
            //incorrect subcategory
            int result = _adminContent.GetPostBySubCateId(subCateid: "a").Result.errorCode;
            Assert.AreEqual(0, result);
            //correct subcategory
            result = _adminContent.GetPostBySubCateId(subCateid: "1").Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void SearchPostById()
        {
            int result = _adminContent.SearchPostById(new PostRequest() { postId = "" }).Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void getDetail()
        {
            //invalid post id
            int result = _adminContent.getDetail(postId: "a").errorCode;
            Assert.AreNotEqual(0, result);
            //valid post id
            result = _adminContent.getDetail(postId: "2").errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void addNewPost()
        {
            //invalid post
            PostRequest post = new PostRequest();
            int result = _adminContent.addNewPost(post).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //thumbnail empty
            post.detailContent = "qwert dft df ";
            post.postTitle = "hgf g";
            post.summaryContent = "jhfg fg";
            post.subCateId = "1";
            //post.pathThumbnail= new FormFile(new MemoryStream(Encoding.UTF8.GetBytes("dummy image")), 0, 0, "Data", "image.png");
            result = _adminContent.addNewPost(post).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //valid post
            post.pathThumbnail= new FormFile(new MemoryStream(Encoding.UTF8.GetBytes("dummy image")), 0, 0, "Data", "image.png");
            result = _adminContent.addNewPost(post).Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void AddMoreImageUpdate()
        {
            //invalid post
            PostRequest post = new PostRequest();
            int result = _adminContent.AddMoreImageUpdate(post).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //image content null
            post.postId = "2";
            result = _adminContent.AddMoreImageUpdate(post).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //image content null
            post.pathThumbnailMore = new FormFile(new MemoryStream(Encoding.UTF8.GetBytes("dummy image")), 0, 0, "Data", "image.png");
            result = _adminContent.AddMoreImageUpdate(post).Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void UpdatePost()
        {
            //invalid post
            PostRequest post = new PostRequest();
            int result = _adminContent.UpdatePost(post).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //incorrect post id
            post.postId = "a";
            result = _adminContent.UpdatePost(post).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //thumbnail empty
            post.postId = "2";
            post.detailContent = "qwert dft df ";
            post.postTitle = "hgf g";
            post.summaryContent = "jhfg fg";
            post.subCateId = "1";
            result = _adminContent.UpdatePost(post).Result.errorCode;
            Assert.AreEqual(0, result);
            //valid post
            post.pathThumbnail = new FormFile(new MemoryStream(Encoding.UTF8.GetBytes("dummy image")), 0, 0, "Data", "image.png");
            result = _adminContent.UpdatePost(post).Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void UpdateStatusPost()
        {
            //invalid post
            PostRequest post = new PostRequest();
            int result = _adminContent.UpdateStatusPost(post).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //incorrect post id
            post.postId = "a";
            result = _adminContent.UpdateStatusPost(post).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //valid post
            post.postId = "2";
            post.status = "1";
            result = _adminContent.UpdateStatusPost(post).Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void RemoveTrashImageContent()
        {
            //incorrect post id
            int result = _adminContent.RemoveTrashImageContent(postId: "a").errorCode;
            Assert.AreNotEqual(0, result);
            //correct post id
            result = _adminContent.RemoveTrashImageContent(postId: "2").errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetCategoryOptionSort()
        {
            int result = _adminContent.GetCategoryOptionSort().Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetSubCategoryOptionSort()
        {
            //incorrect category id
            int result = _adminContent.GetSubCategoryOptionSort(cateid: "a").Result.errorCode;
            Assert.AreEqual(0, result);
            //correct category id
            result = _adminContent.GetSubCategoryOptionSort(cateid: "1").Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void GetSortResultCategoryByCateId()
        {
            //incorrect category id
            int result = _adminContent.GetSortResultCategoryByCateId(cateid: "a").Result.errorCode;
            Assert.AreEqual(0, result);
            //correct category id
            result = _adminContent.GetSortResultCategoryByCateId(cateid: "1").Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void CreateCategory()
        {
            //invalid request
            CategoryRequest request = new CategoryRequest();
            int result = _adminContent.CreateCategory(request).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //name exists
            request.cateName = "Sports";
            result = _adminContent.CreateCategory(request).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //valid request
            request.cateName = DateTime.Now.ToString(); ;
            result = _adminContent.CreateCategory(request).Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void UpdateCategory()
        {
            //invalid request
            CategoryRequest request = new CategoryRequest();
            int result = _adminContent.UpdateCategory(request).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //incorrect category id
            request.cateId = "a";
            request.cateName = "asdg";
            result = _adminContent.UpdateCategory(request).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //name exists
            request.cateId = "2";
            request.cateName = "Sports";
            result = _adminContent.UpdateCategory(request).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //valid request
            request.cateName = DateTime.Now.ToString();
            result = _adminContent.UpdateCategory(request).Result.errorCode;
            Assert.AreEqual(0, result);
        }

        //please add category and enter id
        //[Test]
        //public void DeleteCategory()
        //{
        //    //incorrect category id
        //    int result = _adminContent.DeleteCategory(cateId: "a").Result.errorCode;
        //    Assert.AreNotEqual(0, result);
        //    //correct category id
        //    result = _adminContent.DeleteCategory(cateId: "3").Result.errorCode;
        //    Assert.AreEqual(0, result);
        //}

        [Test]
        public void CreateSubCategory()
        {
            //invalid request
            SubCategoryRequest request = new SubCategoryRequest();
            int result = _adminContent.CreateSubCategory(request).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //exists request
            request.cateName = "Football";
            request.cateId = "1";
            result = _adminContent.CreateSubCategory(request).Result.errorCode;
            Assert.AreNotEqual(0, result);
            ///thumbnail empty
            request.cateName = DateTime.Now.ToString();
            request.cateId = "1";
            result = _adminContent.CreateSubCategory(request).Result.errorCode;
            Assert.AreNotEqual(0, result);
            //valid request
            request.thumbnailSubCate= new FormFile(new MemoryStream(Encoding.UTF8.GetBytes("dummy image")), 0, 0, "Data", "image.png"); ;
            result = _adminContent.CreateSubCategory(request).Result.errorCode;
            Assert.AreNotEqual(0, result);
        }

        [Test]
        public void UpdateSubCategory()
        {
            //invalid request
            SubCategoryRequest request = new SubCategoryRequest();
            int result = _adminContent.UpdateSubCategory(request).Result.errorCode;
            Assert.AreNotEqual(0, result);
            ///thumbnail empty
            request.subCateName = DateTime.Now.ToString();
            request.subCateId = "2";
            request.cateId = "1";
            result = _adminContent.UpdateSubCategory(request).Result.errorCode;
            Assert.AreEqual(0, result);
            //valid request
            request.thumbnailSubCate = new FormFile(new MemoryStream(Encoding.UTF8.GetBytes("dummy image")), 0, 0, "Data", "image.png"); ;
            result = _adminContent.UpdateSubCategory(request).Result.errorCode;
            Assert.AreEqual(0, result);
        }

        //please add subcategory and enter id
        //[Test]
        //public void DeleteSubCategory()
        //{
        //    //incorrect subcategory id
        //    int result = _adminContent.DeleteSubCategory(subCateId: "a").Result.errorCode;
        //    Assert.AreNotEqual(0, result);
        //    //correct subcategory id
        //    result = _adminContent.DeleteSubCategory(subCateId: "3").Result.errorCode;
        //    Assert.AreEqual(0, result);
        //}

        [Test]
        public void SearchPostsComment()
        {
            int result = _adminContent.SearchPostsComment("a").Result.errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void getComments()
        {
            //incorrect post id
            int result = _adminContent.getComments(postId: "a").Result.errorCode;
            Assert.AreNotEqual(0, result);
            //correct post id
            result = _adminContent.getComments(postId: "1").Result.errorCode;
            Assert.AreEqual(0, result);
        }

        //please add comment and enter comment id
        //[Test]
        //public void RemoveComment()
        //{
        //    //incaorrect comment id
        //    int result = _adminContent.RemoveComment(commentId: "a").Result.errorCode;
        //    Assert.AreNotEqual(0, result);
        //    //correct comment id
        //    result = _adminContent.RemoveComment(commentId: "1").Result.errorCode;
        //    Assert.AreEqual(0, result);
        //}
    }
}
