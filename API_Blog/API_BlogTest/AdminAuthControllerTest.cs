﻿using API_Blog.Controllers;
using API_Blog.Models;
using API_Blog.Models.Requests;
using API_Blog.Services.ultils;
using Microsoft.AspNetCore.Http;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace API_BlogTest
{
    public class AdminAuthControllerTest
    {
        private readonly BlogContext _dbContext;
        private readonly AdminAuthController _adminAuth;
        private IMyLogger _logger;

        public AdminAuthControllerTest()
        {
            _dbContext = new DbContextSetUp().GetBlogContext();
            _logger = new Mock<IMyLogger>().Object;
            _adminAuth = new AdminAuthController(_dbContext, _logger);
            var contextMock = new Mock<HttpContext>();
            contextMock.Setup(x => x.User).Returns(
                new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Role, "Admin"),
                    new Claim("UserId", "1"),
                }))
                );
            _adminAuth.ControllerContext.HttpContext = contextMock.Object;
        }

        [Test]
        public void login()
        {
            //invalid request
            LoginRequest request = new LoginRequest();
            int result = _adminAuth.login(request).errorCode;
            Assert.AreNotEqual(0, result);
            //incorrect password
            request.password = "12345678";
            request.email = "tienhm4@gmail.com";
            result = _adminAuth.login(request).errorCode;
            Assert.AreNotEqual(0, result);
            //valid request
            request.password = "123456";
            result = _adminAuth.login(request).errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void ChangePassword()
        {
            //invalid request
            LoginRequest request = new LoginRequest();
            int result = _adminAuth.ChangePassword(request).errorCode;
            Assert.AreNotEqual(0, result);
            //incorrect current password
            request.password = "12345678";
            result = _adminAuth.ChangePassword(request).errorCode;
            Assert.AreNotEqual(0, result);
            //password not match
            request.password = "yAuIu5q65V";
            request.newPassword = "123456";
            request.confirmPassword = "1234567";
            result = _adminAuth.ChangePassword(request).errorCode;
            Assert.AreNotEqual(0, result);
            //valid request
            request.password = "yAuIu5q65V";
            request.confirmPassword = "123456";
            result = _adminAuth.ChangePassword(request).errorCode;
            Assert.AreEqual(0, result);
        }


        [Test]
        public void ResetPassword()
        {
            //invalid request
            LoginRequest request = new LoginRequest();
            int result = _adminAuth.ResetPassword(request).errorCode;
            Assert.AreNotEqual(0, result);
            //valid request
            request.email = "tienhm4@gmail.com";
            result = _adminAuth.ResetPassword(request).errorCode;
            Assert.AreEqual(0, result);
        }
    }
}
