﻿using API_Blog.Controllers;
using API_Blog.Models;
using API_Blog.Services.ultils;
using Moq;
using NUnit.Framework;

namespace API_BlogTest
{
    public class SubCategoryControllerTest
    {
        private readonly BlogContext _dbContext;
        private readonly SubCategoryController _subCategory;
        private IMyLogger _logger;

        public SubCategoryControllerTest()
        {
            _dbContext = new DbContextSetUp().GetBlogContext();
            _logger = new Mock<IMyLogger>().Object;
            _subCategory = new SubCategoryController(_dbContext, _logger);
        }

        [Test]
        public void getallSubCategory()
        {
            int result = _subCategory.getallSubCategory().errorCode;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void getAllPostsBySubCateid()
        {
            //incorrect subcategory id, page number
            int result = _subCategory.getAllPostsBySubCateid(subCateId: "a", page: "a").errorCode;
            Assert.AreNotEqual(0, result);
            //incorrect subcategory id, correct page number
            result = _subCategory.getAllPostsBySubCateid(subCateId: "a", page: "1").errorCode;
            Assert.AreNotEqual(0, result);
            //correct subcategory id,incorrect page number
            result = _subCategory.getAllPostsBySubCateid(subCateId: "1", page: "a").errorCode;
            Assert.AreEqual(0, result);
            //correct subcategory id, page number
            result = _subCategory.getAllPostsBySubCateid(subCateId: "1", page: "1").errorCode;
            Assert.AreEqual(0, result);
        }
    }
}
